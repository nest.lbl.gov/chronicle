package gov.lbl.nest.chronicle;

import java.util.List;

/**
 * {@link EndorsementLifecycleFactory} implementation for testing.
 * 
 */
public class MockEndorsementLifecycleFactory extends
                                             EndorsementLifecycleFactory {

    final String[] PLACEMENTS_ARRAY = new String[] { "USDC Placement",
                                                     "UKDC Placement",
                                                     "HPSS Placement" };

    @Override
    public EndorsementLifecycle createEndorsementLifecycle() {
        return new EndorsementLifecycle() {

            @Override
            public void failedEndorsement(Chronicle chronicle,
                                          Narrative narrative,
                                          Endorsement endorsement) {
            }

            @Override
            public boolean preEndorsement(Chronicle chronicle,
                                          Narrative narrative,
                                          Endorsement endorsement) {
                return true;
            }

            @Override
            public void successfulEndorsement(Chronicle chronicle,
                                              Narrative narrative,
                                              Endorsement endorsement) {
                final String topic = (narrative.getTopic()).getName();
                switch (topic) {
                case "Data Centers":
                    final List<? extends Endorsement> endorsements = narrative.getEndorsements();
                    int found = 0;
                    for (String placement : PLACEMENTS_ARRAY) {
                        if (null != findEndorsement(placement,
                                                    endorsements)) {
                            found++;
                        }
                    }
                    if (2 <= found) {
                        try {
                            chronicle.load(topic,
                                           new EndorsementLifecycle.InputEndorsement((narrative.getItem()).getName(),
                                                                                     "Ready for DAQ Deletion",
                                                                                     endorsement.getWhenEndorsed()));
                        } catch (IllegalArgumentException
                                 | IllegalStateException
                                 | NoSuchActionException
                                 | NoSuchTopicException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                    break;
                default:
                    break;
                }
            }
        };
    }

    private Endorsement findEndorsement(String name,
                                        List<? extends Endorsement> endorsements) {
        for (Endorsement endorsement : endorsements) {
            if (name.equals(endorsement.getEndorser()
                                       .getAction())) {
                return endorsement;
            }
        }
        return null;
    }
}
