package gov.lbl.nest.chronicle;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.chronicle.Chronicle.MultipleEndorsements;

/**
 * This class defines the tests that any {@link Chronicle} class must pass.
 *
 * @author patton
 */
public abstract class AbstractChronicleTests {

    private class MockEndorsement implements
                                  Endorsement {

        private final Endorser endorser;
        private final String item;
        private final Date whenEndorsed;

        private MockEndorsement(String item,
                                String action,
                                Date whenEndorsed) {
            this.endorser = new Endorser() {

                @Override
                public String getAction() {
                    return action;
                }
            };
            this.item = item;
            this.whenEndorsed = whenEndorsed;
        }

        @Override
        public Endorser getEndorser() {
            return endorser;
        }

        @Override
        public String getItem() {
            return item;
        }

        @Override
        public Date getWhenEndorsed() {
            return whenEndorsed;
        }
    }

    /**
     * The earliest date used for endorsing.
     */
    private static final Date EARLIEST_DATE = new Date(TimeUnit.MILLISECONDS.convert(2L,
                                                                                     TimeUnit.DAYS));

    /**
     * The earliest date used for endorsing.
     */
    private static final Date MIDDLE_DATE = new Date(TimeUnit.MILLISECONDS.convert(4L,
                                                                                   TimeUnit.DAYS));

    /**
     * The latest date used for endorsing.
     */
    private static final Date LATEST_DATE = new Date(TimeUnit.MILLISECONDS.convert(6L,
                                                                                   TimeUnit.DAYS));

    /**
     * The item used for testing.
     */
    private static final String TEST_ITEM = "item";

    /**
     * The endorsers used for testing.
     */
    private static final String[] TEST_ACTION = new String[] { "action.0",
                                                               "action.1" };

    /**
     * The endorsers used for testing.
     */
    private static final String[] TEST_ADDITIONAL_ACTIONS = new String[] { "action.2",
                                                                           "action.3" };

    /**
     * The topic used for testing.
     */
    private static final String TEST_TOPIC = "topic";

    /**
     * The instance being tested.
     */
    private Chronicle testObject;

    /**
     * Test the creation of a new topic works correctly.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Action Creation")
    public void actionCreation() throws IllegalArgumentException,
                                 NarrativeExistsException,
                                 NoSuchActionException,
                                 NoSuchTopicException {
        testObject.setAutomaticActions(false);
        for (String action : TEST_ACTION) {
            testObject.createAction(action);
        }
        final Narrative narrative = testObject.createNarrative(TEST_ITEM,
                                                               null,
                                                               Arrays.asList(TEST_ACTION));
        final Collection<? extends Endorser> endorers = narrative.getEndorsers();
        assertEquals(TEST_ACTION.length,
                     endorers.size(),
                     "Wrong number of endorser when created");
    }

    /**
     * Test that duplication of multiple endorsements works correctly.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     * @throws NoSuchNarrativeException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Allow duplicate multiple Endorsements")
    public void allowDuplicateMultipleEndorsements() throws IllegalArgumentException,
                                                     IllegalStateException,
                                                     NarrativeExistsException,
                                                     NoSuchNarrativeException,
                                                     NoSuchActionException,
                                                     NoSuchTopicException {
        testObject.setMultipleEndorsements(MultipleEndorsements.DUPLICATES);
        testObject.createNarrative(TEST_ITEM,
                                   TEST_TOPIC,
                                   Arrays.asList(TEST_ACTION));
        final List<Endorsement> endorsements = new ArrayList<>(1);
        endorsements.add(new MockEndorsement(TEST_ITEM,
                                             TEST_ACTION[0],
                                             MIDDLE_DATE));
        testObject.load(TEST_TOPIC,
                        endorsements);

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         endorsements.clear();
                         endorsements.add(new MockEndorsement(TEST_ITEM,
                                                              TEST_ACTION[0],
                                                              EARLIEST_DATE));
                         testObject.load(TEST_TOPIC,
                                         endorsements);
                     });

        {
            endorsements.clear();
            endorsements.add(new MockEndorsement(TEST_ITEM,
                                                 TEST_ACTION[0],
                                                 MIDDLE_DATE));
            testObject.load(TEST_TOPIC,
                            endorsements);
            Narrative narrative = testObject.getNarrative(TEST_ITEM,
                                                          TEST_TOPIC);
            final List<? extends Endorsement> currentEndorsements = narrative.getEndorsements();
            assertEquals(1,
                         currentEndorsements.size(),
                         "Wrong number of endorsements");
        }

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         endorsements.clear();
                         endorsements.add(new MockEndorsement(TEST_ITEM,
                                                              TEST_ACTION[0],
                                                              LATEST_DATE));
                         testObject.load(TEST_TOPIC,
                                         endorsements);
                     });
    }

    /**
     * Test that allowing multiple endorsements works correctly.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     * @throws NoSuchNarrativeException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Allow multiple Endorsements")
    public void allowMultipleEndorsements() throws IllegalArgumentException,
                                            IllegalStateException,
                                            NarrativeExistsException,
                                            NoSuchNarrativeException,
                                            NoSuchActionException,
                                            NoSuchTopicException {
        testObject.setMultipleEndorsements(MultipleEndorsements.ALLOWED);
        testObject.createNarrative(TEST_ITEM,
                                   TEST_TOPIC,
                                   Arrays.asList(TEST_ACTION));
        final List<Endorsement> endorsements = new ArrayList<>(1);
        endorsements.add(new MockEndorsement(TEST_ITEM,
                                             TEST_ACTION[0],
                                             MIDDLE_DATE));
        testObject.load(TEST_TOPIC,
                        endorsements);

        {
            endorsements.clear();
            endorsements.add(new MockEndorsement(TEST_ITEM,
                                                 TEST_ACTION[0],
                                                 EARLIEST_DATE));
            testObject.load(TEST_TOPIC,
                            endorsements);
            Narrative narrative = testObject.getNarrative(TEST_ITEM,
                                                          TEST_TOPIC);
            final List<? extends Endorsement> currentEndorsements = narrative.getEndorsements();
            assertEquals(2,
                         currentEndorsements.size(),
                         "Wrong number of endorsements");
            assertEquals(MIDDLE_DATE,
                         (currentEndorsements.get(0)).getWhenEndorsed(),
                         "Wrong Endorsement exists");
            assertEquals(EARLIEST_DATE,
                         (currentEndorsements.get(1)).getWhenEndorsed(),
                         "Wrong Endorsement exists");
        }

        {
            endorsements.clear();
            endorsements.add(new MockEndorsement(TEST_ITEM,
                                                 TEST_ACTION[0],
                                                 MIDDLE_DATE));
            testObject.load(TEST_TOPIC,
                            endorsements);
            Narrative narrative = testObject.getNarrative(TEST_ITEM,
                                                          TEST_TOPIC);
            final List<? extends Endorsement> currentEndorsements = narrative.getEndorsements();
            assertEquals(2,
                         currentEndorsements.size(),
                         "Wrong number of endorsements");
        }

        {
            endorsements.clear();
            endorsements.add(new MockEndorsement(TEST_ITEM,
                                                 TEST_ACTION[0],
                                                 LATEST_DATE));
            testObject.load(TEST_TOPIC,
                            endorsements);
            Narrative narrative = testObject.getNarrative(TEST_ITEM,
                                                          TEST_TOPIC);
            final List<? extends Endorsement> currentEndorsements = narrative.getEndorsements();
            assertEquals(3,
                         currentEndorsements.size(),
                         "Wrong number of endorsements");
            assertEquals(LATEST_DATE,
                         (currentEndorsements.get(0)).getWhenEndorsed(),
                         "Wrong Endorsement exists");
            assertEquals(MIDDLE_DATE,
                         (currentEndorsements.get(1)).getWhenEndorsed(),
                         "Wrong Endorsement exists");
            assertEquals(EARLIEST_DATE,
                         (currentEndorsements.get(2)).getWhenEndorsed(),
                         "Wrong Endorsement exists");
        }
    }

    /**
     * Test the assignment of additional actions to existing narratives.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     * @throws NoSuchNarrativeException
     *             when there is a problem.
     */
    @Test
    @DisplayName(value = "Assigning Actions")
    public void assigningActions() throws IllegalArgumentException,
                                   IllegalStateException,
                                   NarrativeExistsException,
                                   NoSuchNarrativeException,
                                   NoSuchActionException,
                                   NoSuchTopicException {
        createNarrative();
        final Narrative narrative = testObject.endorse(TEST_ITEM,
                                                       TEST_TOPIC,
                                                       TEST_ACTION[0]);
        final List<String> actions = new ArrayList<>(Arrays.asList(TEST_ACTION));
        actions.addAll(Arrays.asList(TEST_ADDITIONAL_ACTIONS));
        testObject.assignActions(TEST_ITEM,
                                 TEST_TOPIC,
                                 actions);

        final Collection<? extends Endorser> endorers = narrative.getEndorsers();
        assertEquals((TEST_ACTION.length + TEST_ADDITIONAL_ACTIONS.length) - 1,
                     endorers.size(),
                     "Wrong number of endorser when created");
    }

    /**
     * Test the creation of a new narrative with automatic creation of actions and
     * topics.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     */
    @Test
    @DisplayName(value = "Create Narrative")
    public void createNarrative() throws IllegalArgumentException,
                                  NarrativeExistsException,
                                  NoSuchActionException,
                                  NoSuchTopicException {
        final Narrative narrative = testObject.createNarrative(TEST_ITEM,
                                                               TEST_TOPIC,
                                                               Arrays.asList(TEST_ACTION));
        assertEquals(TEST_ITEM,
                     (narrative.getItem()).getName());
        assertEquals(TEST_TOPIC,
                     (narrative.getTopic()).getName());
        final Collection<? extends Endorser> endorers = narrative.getEndorsers();
        assertEquals(TEST_ACTION.length,
                     endorers.size(),
                     "Wrong number of endorser when created");
    }

    /**
     * Test the return of a Digest from a narrative.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     * @throws NoSuchNarrativeException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Digest creation")
    public void digestCreation() throws IllegalArgumentException,
                                 NarrativeExistsException,
                                 NoSuchActionException,
                                 NoSuchTopicException,
                                 IllegalStateException,
                                 NoSuchNarrativeException {
        testObject.createNarrative(TEST_ITEM,
                                   TEST_TOPIC,
                                   Arrays.asList(TEST_ACTION));
        final Narrative narrative = testObject.getNarrative(TEST_ITEM,
                                                            TEST_TOPIC);
        final Collection<? extends Endorser> endorers = narrative.getEndorsers();
        assertEquals(TEST_ACTION.length,
                     endorers.size(),
                     "Wrong number of endorsers");
        final List<? extends Endorsement> endorsements = narrative.getEndorsements();
        assertTrue(null == endorsements || endorsements.isEmpty(),
                   "Wrong number of endorsements");
    }

    /**
     * Test that keeping the earliest multiple endorsements works correctly.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     * @throws NoSuchNarrativeException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Earliest multiple Endorsement")
    public void earliestMultipleEndorsement() throws IllegalArgumentException,
                                              IllegalStateException,
                                              NarrativeExistsException,
                                              NoSuchNarrativeException,
                                              NoSuchActionException,
                                              NoSuchTopicException {
        testObject.setMultipleEndorsements(MultipleEndorsements.EARLIEST);
        testObject.createNarrative(TEST_ITEM,
                                   TEST_TOPIC,
                                   Arrays.asList(TEST_ACTION));
        final List<Endorsement> endorsements = new ArrayList<>(1);
        endorsements.add(new MockEndorsement(TEST_ITEM,
                                             TEST_ACTION[0],
                                             MIDDLE_DATE));
        testObject.load(TEST_TOPIC,
                        endorsements);

        {
            endorsements.clear();
            endorsements.add(new MockEndorsement(TEST_ITEM,
                                                 TEST_ACTION[0],
                                                 EARLIEST_DATE));
            testObject.load(TEST_TOPIC,
                            endorsements);
            Narrative narrative = testObject.getNarrative(TEST_ITEM,
                                                          TEST_TOPIC);
            final List<? extends Endorsement> currentEndorsements = narrative.getEndorsements();
            assertEquals(1,
                         currentEndorsements.size(),
                         "Wrong number of endorsements");
            assertEquals(EARLIEST_DATE,
                         (currentEndorsements.get(0)).getWhenEndorsed(),
                         "Wrong Endorsement exists");
        }

        {
            endorsements.clear();
            endorsements.add(new MockEndorsement(TEST_ITEM,
                                                 TEST_ACTION[0],
                                                 LATEST_DATE));
            testObject.load(TEST_TOPIC,
                            endorsements);
            Narrative narrative = testObject.getNarrative(TEST_ITEM,
                                                          TEST_TOPIC);
            final List<? extends Endorsement> currentEndorsements = narrative.getEndorsements();
            assertEquals(1,
                         currentEndorsements.size(),
                         "Wrong number of endorsements");
            assertEquals(EARLIEST_DATE,
                         (currentEndorsements.get(0)).getWhenEndorsed(),
                         "Wrong Endorsement exists");
        }
    }

    /**
     * Test that endorsing an narrative works correctly.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     * @throws NoSuchNarrativeException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Endorse existing Endorsers")
    public void endorseExistingEndorsers() throws IllegalArgumentException,
                                           IllegalStateException,
                                           NarrativeExistsException,
                                           NoSuchNarrativeException,
                                           NoSuchActionException,
                                           NoSuchTopicException {
        testObject.createNarrative(TEST_ITEM,
                                   TEST_TOPIC,
                                   Arrays.asList(TEST_ACTION));
        int count = 0;
        for (String action : TEST_ACTION) {
            final Narrative narrative = testObject.endorse(TEST_ITEM,
                                                           TEST_TOPIC,
                                                           action);
            count += 1;
            final Collection<? extends Endorser> endorers = narrative.getEndorsers();
            assertEquals(TEST_ACTION.length - count,
                         endorers.size(),
                         "Wrong number of endorsers");
            final List<? extends Endorsement> endorsements = narrative.getEndorsements();
            assertEquals(count,
                         endorsements.size(),
                         "Wrong number of endorsements");
        }
    }

    /**
     * Test that loading endorsements from an external source works correctly.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     * @throws NoSuchNarrativeException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Endorsement loading")
    public void endorsementLoading() throws IllegalArgumentException,
                                     IllegalStateException,
                                     NarrativeExistsException,
                                     NoSuchNarrativeException,
                                     NoSuchActionException,
                                     NoSuchTopicException {
        testObject.setMultipleEndorsements(MultipleEndorsements.ALLOWED);
        testObject.createNarrative(TEST_ITEM,
                                   TEST_TOPIC,
                                   Arrays.asList(TEST_ACTION));
        final List<Endorsement> endorsements = new ArrayList<>(1);
        endorsements.add(new MockEndorsement(TEST_ITEM,
                                             TEST_ACTION[0],
                                             MIDDLE_DATE));
        endorsements.add(new MockEndorsement(TEST_ITEM,
                                             TEST_ACTION[1],
                                             MIDDLE_DATE));
        final List<? extends Narrative> narratives = testObject.load(TEST_TOPIC,
                                                                     endorsements);
        for (Narrative narrative : narratives) {
            final Collection<? extends Endorser> endorers = narrative.getEndorsers();
            assertTrue(endorers.isEmpty(),
                       "Wrong number of endorsers");
            final List<? extends Endorsement> currentEndorsements = narrative.getEndorsements();
            assertEquals(2,
                         currentEndorsements.size(),
                         "Wrong number of endorsements");
        }
    }

    /**
     * Test that forbidding multiple endorsements works correctly.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     * @throws NoSuchNarrativeException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Forbidden multiple Endorsement")
    public void forbiddenMultipleEndorsements() throws IllegalArgumentException,
                                                IllegalStateException,
                                                NarrativeExistsException,
                                                NoSuchNarrativeException,
                                                NoSuchActionException,
                                                NoSuchTopicException {
        testObject.setMultipleEndorsements(MultipleEndorsements.FORBIDDEN);
        testObject.createNarrative(TEST_ITEM,
                                   TEST_TOPIC,
                                   Arrays.asList(TEST_ACTION));
        final List<Endorsement> endorsements = new ArrayList<>(1);
        endorsements.add(new MockEndorsement(TEST_ITEM,
                                             TEST_ACTION[0],
                                             MIDDLE_DATE));
        testObject.load(TEST_TOPIC,
                        endorsements);

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         endorsements.clear();
                         endorsements.add(new MockEndorsement(TEST_ITEM,
                                                              TEST_ACTION[0],
                                                              EARLIEST_DATE));
                         testObject.load(TEST_TOPIC,
                                         endorsements);
                     });

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         endorsements.clear();
                         endorsements.add(new MockEndorsement(TEST_ITEM,
                                                              TEST_ACTION[0],
                                                              MIDDLE_DATE));
                         testObject.load(TEST_TOPIC,
                                         endorsements);
                     });

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         endorsements.clear();
                         endorsements.add(new MockEndorsement(TEST_ITEM,
                                                              TEST_ACTION[0],
                                                              LATEST_DATE));
                         testObject.load(TEST_TOPIC,
                                         endorsements);
                     });
    }

    /**
     * Test that ignoring multiple endorsements works correctly.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     * @throws NoSuchNarrativeException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Ignore multiple Endorsement")
    public void ignoreMultipleEndorsements() throws IllegalArgumentException,
                                             IllegalStateException,
                                             NarrativeExistsException,
                                             NoSuchNarrativeException,
                                             NoSuchActionException,
                                             NoSuchTopicException {
        testObject.setMultipleEndorsements(MultipleEndorsements.IGNORE);
        testObject.createNarrative(TEST_ITEM,
                                   TEST_TOPIC,
                                   Arrays.asList(TEST_ACTION));
        final List<Endorsement> endorsements = new ArrayList<>(1);
        endorsements.add(new MockEndorsement(TEST_ITEM,
                                             TEST_ACTION[0],
                                             MIDDLE_DATE));
        testObject.load(TEST_TOPIC,
                        endorsements);
        {
            endorsements.clear();
            endorsements.add(new MockEndorsement(TEST_ITEM,
                                                 TEST_ACTION[0],
                                                 EARLIEST_DATE));
            testObject.load(TEST_TOPIC,
                            endorsements);
            Narrative narrative = testObject.getNarrative(TEST_ITEM,
                                                          TEST_TOPIC);
            final List<? extends Endorsement> currentEndorsements = narrative.getEndorsements();
            assertEquals(1,
                         currentEndorsements.size(),
                         "Wrong number of endorsements");
        }

        {
            endorsements.clear();
            endorsements.add(new MockEndorsement(TEST_ITEM,
                                                 TEST_ACTION[0],
                                                 MIDDLE_DATE));
            testObject.load(TEST_TOPIC,
                            endorsements);
            Narrative narrative = testObject.getNarrative(TEST_ITEM,
                                                          TEST_TOPIC);
            final List<? extends Endorsement> currentEndorsements = narrative.getEndorsements();
            assertEquals(1,
                         currentEndorsements.size(),
                         "Wrong number of endorsements");
        }

        {
            endorsements.clear();
            endorsements.add(new MockEndorsement(TEST_ITEM,
                                                 TEST_ACTION[0],
                                                 LATEST_DATE));
            testObject.load(TEST_TOPIC,
                            endorsements);
            Narrative narrative = testObject.getNarrative(TEST_ITEM,
                                                          TEST_TOPIC);
            final List<? extends Endorsement> currentEndorsements = narrative.getEndorsements();
            assertEquals(1,
                         currentEndorsements.size(),
                         "Wrong number of endorsements");
        }
    }

    /**
     * Test that keeping the latest multiple endorsements works correctly.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     * @throws NoSuchNarrativeException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Latest multiple Endorsement")
    public void latestMultipleEndorsement() throws IllegalArgumentException,
                                            IllegalStateException,
                                            NarrativeExistsException,
                                            NoSuchNarrativeException,
                                            NoSuchActionException,
                                            NoSuchTopicException {
        testObject.setMultipleEndorsements(MultipleEndorsements.LATEST);
        testObject.createNarrative(TEST_ITEM,
                                   TEST_TOPIC,
                                   Arrays.asList(TEST_ACTION));
        final List<Endorsement> endorsements = new ArrayList<>(1);
        endorsements.add(new MockEndorsement(TEST_ITEM,
                                             TEST_ACTION[0],
                                             MIDDLE_DATE));
        testObject.load(TEST_TOPIC,
                        endorsements);

        {
            endorsements.clear();
            endorsements.add(new MockEndorsement(TEST_ITEM,
                                                 TEST_ACTION[0],
                                                 EARLIEST_DATE));
            testObject.load(TEST_TOPIC,
                            endorsements);
            Narrative narrative = testObject.getNarrative(TEST_ITEM,
                                                          TEST_TOPIC);
            final List<? extends Endorsement> currentEndorsements = narrative.getEndorsements();
            assertEquals(1,
                         currentEndorsements.size(),
                         "Wrong number of endorsements");
            assertEquals(MIDDLE_DATE,
                         (currentEndorsements.get(0)).getWhenEndorsed(),
                         "Wrong Endorsement exists");
        }

        {
            endorsements.clear();
            endorsements.add(new MockEndorsement(TEST_ITEM,
                                                 TEST_ACTION[0],
                                                 LATEST_DATE));
            testObject.load(TEST_TOPIC,
                            endorsements);
            Narrative narrative = testObject.getNarrative(TEST_ITEM,
                                                          TEST_TOPIC);
            final List<? extends Endorsement> currentEndorsements = narrative.getEndorsements();
            assertEquals(1,
                         currentEndorsements.size(),
                         "Wrong number of endorsements");
            assertEquals(LATEST_DATE,
                         (currentEndorsements.get(0)).getWhenEndorsed(),
                         "Wrong Endorsement exists");
        }
    }

    /**
     * Test the loading of an existing item works correctly.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Load existing Narrative")
    public void loadExistNarrative() throws IllegalArgumentException,
                                     IllegalStateException,
                                     NoSuchActionException,
                                     NoSuchTopicException {
        loadNewNarrative();
        final Endorsement lateEndorsement = new MockEndorsement(TEST_ITEM,
                                                                TEST_ACTION[0],
                                                                LATEST_DATE);
        final Endorsement earlyEndorsement = new MockEndorsement(TEST_ITEM,
                                                                 TEST_ACTION[0],
                                                                 EARLIEST_DATE);
        final List<Endorsement> endorsements = new ArrayList<>();
        endorsements.add(lateEndorsement);
        endorsements.add(earlyEndorsement);
        final List<? extends Narrative> narratives = testObject.load(TEST_TOPIC,
                                                                     endorsements);
        assertEquals(1,
                     narratives.size());
        final Narrative narrative = narratives.get(0);
        final List<? extends Endorsement> existingEndorsements = narrative.getEndorsements();
        assertEquals(3,
                     existingEndorsements.size());
        Date previousDate = null;
        for (Endorsement endorsement : existingEndorsements) {
            if (null == previousDate) {
                previousDate = endorsement.getWhenEndorsed();
            } else {
                final Date date = endorsement.getWhenEndorsed();
                assertTrue(previousDate.after(date));
                previousDate = date;
            }
        }
    }

    /**
     * Test the loading of a new item works correctly.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Load new Narrative")
    public void loadNewNarrative() throws IllegalArgumentException,
                                   IllegalStateException,
                                   NoSuchActionException,
                                   NoSuchTopicException {
        final Endorsement endorsement = new MockEndorsement(TEST_ITEM,
                                                            TEST_ACTION[0],
                                                            MIDDLE_DATE);
        final List<Endorsement> endorsements = new ArrayList<>();
        endorsements.add(endorsement);
        final List<? extends Narrative> narratives = testObject.load(TEST_TOPIC,
                                                                     endorsements);
        assertEquals(1,
                     narratives.size());
    }

    /**
     * Test the creation of a new narrative fails when a action is unknown and is
     * not automatically created.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Missing Action")
    public void missingActions() throws IllegalArgumentException,
                                 NarrativeExistsException,
                                 NoSuchActionException,
                                 NoSuchTopicException {
        testObject.setAutomaticActions(false);
        assertThrows(NoSuchActionException.class,
                     () -> {
                         testObject.createNarrative(TEST_ITEM,
                                                    TEST_TOPIC,
                                                    Arrays.asList(TEST_ACTION));
                     });
    }

    /**
     * Test the creation of a new narrative fails when a topic is unknown and is not
     * automatically created.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Missing Topic")
    public void missingTopic() throws IllegalArgumentException,
                               NarrativeExistsException,
                               NoSuchActionException,
                               NoSuchTopicException {
        testObject.setAutomaticTopics(false);
        assertThrows(NoSuchTopicException.class,
                     () -> {
                         testObject.createNarrative(TEST_ITEM,
                                                    TEST_TOPIC,
                                                    Arrays.asList(TEST_ACTION));
                     });
    }

    /**
     * Test that replacing multiple endorsements works correctly.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     * @throws NoSuchNarrativeException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Replacing multiple Endorsement")
    public void replacingMultipleEndorsement() throws IllegalArgumentException,
                                               IllegalStateException,
                                               NarrativeExistsException,
                                               NoSuchNarrativeException,
                                               NoSuchActionException,
                                               NoSuchTopicException {
        testObject.setMultipleEndorsements(MultipleEndorsements.REPLACE);
        testObject.createNarrative(TEST_ITEM,
                                   TEST_TOPIC,
                                   Arrays.asList(TEST_ACTION));
        final List<Endorsement> endorsements = new ArrayList<>(1);
        endorsements.add(new MockEndorsement(TEST_ITEM,
                                             TEST_ACTION[0],
                                             MIDDLE_DATE));
        testObject.load(TEST_TOPIC,
                        endorsements);

        {
            endorsements.clear();
            endorsements.add(new MockEndorsement(TEST_ITEM,
                                                 TEST_ACTION[0],
                                                 EARLIEST_DATE));
            testObject.load(TEST_TOPIC,
                            endorsements);
            Narrative narrative = testObject.getNarrative(TEST_ITEM,
                                                          TEST_TOPIC);
            final List<? extends Endorsement> currentEndorsements = narrative.getEndorsements();
            assertEquals(1,
                         currentEndorsements.size(),
                         "Wrong number of endorsements");
            assertEquals(EARLIEST_DATE,
                         (currentEndorsements.get(0)).getWhenEndorsed(),
                         "Wrong Endorsement exists");
        }

        {
            endorsements.clear();
            endorsements.add(new MockEndorsement(TEST_ITEM,
                                                 TEST_ACTION[0],
                                                 LATEST_DATE));
            testObject.load(TEST_TOPIC,
                            endorsements);
            Narrative narrative = testObject.getNarrative(TEST_ITEM,
                                                          TEST_TOPIC);
            final List<? extends Endorsement> currentEndorsements = narrative.getEndorsements();
            assertEquals(1,
                         currentEndorsements.size(),
                         "Wrong number of endorsements");
            assertEquals(LATEST_DATE,
                         (currentEndorsements.get(0)).getWhenEndorsed(),
                         "Wrong Endorsement exists");
        }
    }

    /**
     * Sets the object to be tested.
     *
     * @param chronicle
     *            the object to be tested.
     */
    protected void setTestObject(final Chronicle chronicle) {
        testObject = chronicle;
    }

    /**
     * Test the creation of a new topic work.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Topic Creation")
    public void topicCreation() throws IllegalArgumentException,
                                NarrativeExistsException,
                                NoSuchActionException,
                                NoSuchTopicException {
        testObject.setAutomaticTopics(false);
        testObject.createTopic(TEST_TOPIC);
        final Narrative narrative = testObject.createNarrative(TEST_ITEM,
                                                               null,
                                                               Arrays.asList(TEST_ACTION));
        final Collection<? extends Endorser> endorers = narrative.getEndorsers();
        assertEquals(TEST_ACTION.length,
                     endorers.size(),
                     "Wrong number of endorser when created");
    }

    /**
     * Test the unassignment of additional actions from existing narratives.
     *
     * @throws IllegalArgumentException
     *             when there is a problem.
     * @throws IllegalStateException
     *             when there is a problem.
     * @throws NarrativeExistsException
     *             when there is a problem.
     * @throws NoSuchActionException
     *             when there is a problem.
     * @throws NoSuchTopicException
     *             when there is a problem.
     * @throws NoSuchNarrativeException
     *             when there is a problem.
     */
    @Test
    @DisplayName("Unasigning Actions")
    public void unassigningActions() throws IllegalArgumentException,
                                     IllegalStateException,
                                     NarrativeExistsException,
                                     NoSuchNarrativeException,
                                     NoSuchActionException,
                                     NoSuchTopicException {
        createNarrative();
        final Narrative narrative = testObject.endorse(TEST_ITEM,
                                                       TEST_TOPIC,
                                                       TEST_ACTION[0]);
        testObject.assignActions(TEST_ITEM,
                                 TEST_TOPIC,
                                 Arrays.asList(TEST_ADDITIONAL_ACTIONS));
        testObject.unassignActions(TEST_ITEM,
                                   TEST_TOPIC,
                                   Arrays.asList(TEST_ACTION));

        final Collection<? extends Endorser> endorers = narrative.getEndorsers();
        assertEquals(TEST_ADDITIONAL_ACTIONS.length,
                     endorers.size(),
                     "Wrong number of endorser when created");
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}
}
