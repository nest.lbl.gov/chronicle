package gov.lbl.nest.chronicle.ejb;

import jakarta.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import gov.lbl.nest.chronicle.AbstractChronicleTests;

/**
 * Tests {@link ChronicleImpl} class.
 */
@DisplayName("ChronicleImpl Tests")
public class ChronicleImplTests extends
                                AbstractChronicleTests {

    /**
     * The instance being tested.
     */
    private ChronicleImpl testObject;

    /**
     * Sets up test environment.
     * 
     * @throws Exception
     *             when there is a problem.
     */
    @BeforeEach
    protected void setUp() throws Exception {
        final EntityManager entityManager = new MockEntityManager();
        testObject = new ChronicleImpl(entityManager,
                                       new ChronicleOptions());
        setTestObject(testObject);
    }

}
