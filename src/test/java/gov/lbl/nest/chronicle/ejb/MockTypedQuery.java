package gov.lbl.nest.chronicle.ejb;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jakarta.persistence.FlushModeType;
import jakarta.persistence.LockModeType;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.NoResultException;
import jakarta.persistence.NonUniqueResultException;
import jakarta.persistence.Parameter;
import jakarta.persistence.TemporalType;
import jakarta.persistence.TypedQuery;

/**
 * {@link MockTypedQuery} implementation for testing.
 * 
 * @param <X>
 *            the class returned by this query.
 */
public class MockTypedQuery<X> implements
                           TypedQuery<X> {

    /**
     * The NamedQuery to find all {@link TopicImpl} instances.
     */
    private static final String GET_ALL_TOPICS = "getAllTopics";

    /**
     * The NamedQuery to find all {@link NarrativeImpl} instances.
     */
    private static final String GET_ALL_NARRATIVES_BY_ITEM = "getAllNarrativesByItem";

    /**
     * The NamedQuery to find {@link EndorserImpl} instances specified by their
     * actions.
     */
    private static final String GET_ENDORSER_BY_ACTION = "getEndorserByAction";

    /**
     * The NamedQuery to find {@link EndorserImpl} instances specified by their
     * actions.
     */
    private static final String GET_ENDORSERS_BY_ACTIONS = "getEndorsersByActions";

    /**
     * The NamedQuery to find a {@link ItemImpl} instance specified by its name.
     */
    private static final String GET_ITEM_BY_NAME = "getItemByName";

    /**
     * The NamedQuery to find a {@link ItemImpl} instance specified by its name.
     */
    private static final String GET_ITEMS_BY_NAMES = "getItemsByNames";

    /**
     * The NamedQuery to find a {@link TopicImpl} instance specified by its name.
     */
    private static final String GET_TOPIC_BY_NAME = "getTopicByName";

    /**
     * The NamedQuery to find a {@link NarrativeImpl} instance specified by its item
     * and topic.
     */
    private static final String GET_NARRATIVE_BY_ITEM_AND_TOPIC = "getNarrativeByItemAndTopic";

    /**
     * The NamedQuery to find a set of {@link NarrativeImpl} instances specified by
     * its item and topic.
     */
    private static final String GET_NARRATIVES_BY_ITEM_NAMES_AND_TOPIC = "getNarrativesByItemNamesAndTopic";

    /**
     * The NamedQuery to find a set of {@link NarrativeImpl} instances specified by
     * its item and topic.
     */
    private static final String GET_NARRATIVES_BY_ITEMS_AND_TOPIC = "getNarrativesByItemsAndTopic";

    /**
     * The {@link MockEntityManager} instance used by this object.
     */
    private MockEntityManager entityManager;

    /**
     * The max number of results for this query {@link int} in a NamedQuery.
     */
    private int maxResults = 0;

    /**
     * The name of the {@link NamedQuery} instance with which this object was
     * created.
     */
    private String namedQuery;

    /**
     * The action to be used in the {@link NamedQuery} instance with which this
     * object was created.
     */
    private String action;

    /**
     * The actions to be used in the {@link NamedQuery} instance with which this
     * object was created.
     */
    private List<String> actions;

    /**
     * The item to be used in the {@link NamedQuery} instance with which this object
     * was created.
     */
    private ItemImpl item;

    /**
     * The item names to be used in the {@link NamedQuery} instance with which this
     * object was created.
     */
    private List<ItemImpl> items;

    /**
     * The name to be used in the {@link NamedQuery} instance with which this object
     * was created.
     */
    private String name;

    /**
     * The item names to be used in the {@link NamedQuery} instance with which this
     * object was created.
     */
    private List<String> names;

    /**
     * The topic to be used in the {@link NamedQuery} instance with which this
     * object was created.
     */
    private TopicImpl topic;

    /**
     * Creates an instance of this class.
     * 
     * @param name
     *            the name of the query.
     * @param mockEntityManager
     *            the {@link MockEntityManager} that will execute this object.
     */
    public MockTypedQuery(String name,
                          MockEntityManager mockEntityManager) {
        this.entityManager = mockEntityManager;
        this.namedQuery = name;
    }

    @Override
    public int executeUpdate() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getFirstResult() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public FlushModeType getFlushMode() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<String, Object> getHints() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public LockModeType getLockMode() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getMaxResults() {
        return maxResults;
    }

    @Override
    public Parameter<?> getParameter(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> Parameter<T> getParameter(int position,
                                         Class<T> type) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Parameter<?> getParameter(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> Parameter<T> getParameter(String name,
                                         Class<T> type) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Set<Parameter<?>> getParameters() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object getParameterValue(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T getParameterValue(Parameter<T> param) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object getParameterValue(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<X> getResultList() {
        return (List<X>) resolveResultsList(namedQuery);
    }

    @SuppressWarnings("unchecked")
    @Override
    public X getSingleResult() {
        return (X) resolveSingleResult(namedQuery);
    }

    @Override
    public boolean isBound(Parameter<?> param) {
        // TODO Auto-generated method stub
        return false;
    }

    private Object resolveResultsList(String namedQuery) {
        if (GET_ALL_TOPICS.equals(namedQuery)) {
            return entityManager.TOPICS;
        }
        if (GET_ITEMS_BY_NAMES.equals(namedQuery)) {
            final List<ItemImpl> results = new ArrayList<>();
            for (ItemImpl item : entityManager.ITEMS) {
                if (names.contains(item.getName())) {
                    results.add(item);
                }
            }
            return results;
        }
        if (GET_ALL_NARRATIVES_BY_ITEM.equals(namedQuery)) {
            final List<NarrativeImpl> results = new ArrayList<>();
            for (NarrativeImpl narrative : entityManager.NARRATIVES) {
                if (narrative.getItem() == item) {
                    results.add(narrative);
                }
            }
            return results;
        }
        if (GET_ENDORSERS_BY_ACTIONS.equals(namedQuery)) {
            final List<EndorserImpl> results = new ArrayList<>();
            for (EndorserImpl endorser : entityManager.ENDORSERS) {
                if (actions.contains(endorser.getAction())) {
                    results.add(endorser);
                }
            }
            return results;
        }
        if (GET_NARRATIVES_BY_ITEM_NAMES_AND_TOPIC.equals(namedQuery)) {
            final List<NarrativeImpl> results = new ArrayList<>();
            for (NarrativeImpl narrative : entityManager.NARRATIVES) {
                if (names.contains((narrative.getItem()).getName()) && topic == narrative.getTopic()) {
                    results.add(narrative);
                }
            }
            return results;
        }
        if (GET_NARRATIVES_BY_ITEMS_AND_TOPIC.equals(namedQuery)) {
            final List<NarrativeImpl> results = new ArrayList<>();
            for (NarrativeImpl narrative : entityManager.NARRATIVES) {
                if (items.contains(narrative.getItem()) && topic == narrative.getTopic()) {
                    results.add(narrative);
                }
            }
            return results;
        }
        return null;
    }

    private Object resolveSingleResult(String namedQuery) {
        if (GET_ENDORSER_BY_ACTION.equals(namedQuery)) {
            EndorserImpl result = null;
            synchronized (entityManager.ENDORSERS) {
                for (EndorserImpl endorser : entityManager.ENDORSERS) {
                    if (action.equals(endorser.getAction())) {
                        if (null == result) {
                            result = endorser;
                        } else {
                            throw new NonUniqueResultException();
                        }
                    }
                }
            }
            if (null == result) {
                throw new NoResultException();
            }
            return result;
        }
        if (GET_ITEM_BY_NAME.equals(namedQuery)) {
            ItemImpl result = null;
            synchronized (entityManager.ITEMS) {
                for (ItemImpl item : entityManager.ITEMS) {
                    if (name.equals(item.getName())) {
                        if (null == result) {
                            result = item;
                        } else {
                            throw new NonUniqueResultException();
                        }
                    }
                }
            }
            if (null == result) {
                throw new NoResultException();
            }
            return result;
        }
        if (GET_NARRATIVE_BY_ITEM_AND_TOPIC.equals(namedQuery)) {
            NarrativeImpl result = null;
            synchronized (entityManager.TOPICS) {
                for (NarrativeImpl narrative : entityManager.NARRATIVES) {
                    if (item == narrative.getItem() && topic == narrative.getTopic()) {
                        if (null == result) {
                            result = narrative;
                        } else {
                            throw new NonUniqueResultException();
                        }
                    }
                }
            }
            return result;
        }
        if (GET_TOPIC_BY_NAME.equals(namedQuery)) {
            TopicImpl result = null;
            synchronized (entityManager.TOPICS) {
                for (TopicImpl topic : entityManager.TOPICS) {
                    if (name.equals(topic.getName())) {
                        if (null == result) {
                            result = topic;
                        } else {
                            throw new NonUniqueResultException();
                        }
                    }
                }
            }
            if (null == result) {
                throw new NoResultException();
            }
            return result;
        }
        throw new NoResultException();
    }

    @Override
    public TypedQuery<X> setFirstResult(int startPosition) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TypedQuery<X> setFlushMode(FlushModeType flushMode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TypedQuery<X> setHint(String hintName,
                                 Object value) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TypedQuery<X> setLockMode(LockModeType lockMode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TypedQuery<X> setMaxResults(int maxResult) {
        this.maxResults = maxResult;
        return this;
    }

    @Override
    public TypedQuery<X> setParameter(int position,
                                      Calendar value,
                                      TemporalType temporalType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TypedQuery<X> setParameter(int position,
                                      Date value,
                                      TemporalType temporalType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TypedQuery<X> setParameter(int position,
                                      Object value) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TypedQuery<X> setParameter(Parameter<Calendar> param,
                                      Calendar value,
                                      TemporalType temporalType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TypedQuery<X> setParameter(Parameter<Date> param,
                                      Date value,
                                      TemporalType temporalType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> TypedQuery<X> setParameter(Parameter<T> param,
                                          T value) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TypedQuery<X> setParameter(String name,
                                      Calendar value,
                                      TemporalType temporalType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TypedQuery<X> setParameter(String name,
                                      Date value,
                                      TemporalType temporalType) {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public TypedQuery<X> setParameter(String name,
                                      Object value) {
        if ("action".equals(name)) {
            action = (String) value;
            return this;
        }
        if ("actions".equals(name)) {
            actions = (List<String>) value;
            return this;
        }
        if ("item".equals(name)) {
            this.item = (ItemImpl) value;
            return this;
        }
        if ("items".equals(name)) {
            this.items = (List<ItemImpl>) value;
            return this;
        }
        if ("name".equals(name)) {
            this.name = (String) value;
            return this;
        }
        if ("names".equals(name)) {
            this.names = (List<String>) value;
            return this;
        }
        if ("topic".equals(name)) {
            this.topic = (TopicImpl) value;
            return this;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public <T> T unwrap(Class<T> cls) {
        // TODO Auto-generated method stub
        return null;
    }

}
