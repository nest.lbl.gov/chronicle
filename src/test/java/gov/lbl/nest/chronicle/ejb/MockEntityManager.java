package gov.lbl.nest.chronicle.ejb;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jakarta.persistence.EntityGraph;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.FlushModeType;
import jakarta.persistence.LockModeType;
import jakarta.persistence.Query;
import jakarta.persistence.StoredProcedureQuery;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaDelete;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.CriteriaUpdate;
import jakarta.persistence.metamodel.Metamodel;

/**
 * {@link EntityManager} implementation for testing.
 */
public class MockEntityManager implements
                               EntityManager {

    /**
     * The array holding persistent {@link NarrativeImpl} instances.
     */
    final List<NarrativeImpl> NARRATIVES = new ArrayList<>();

    /**
     * The array holding persistent {@link EndorserImpl} instances.
     */
    final List<EndorserImpl> ENDORSERS = new ArrayList<>();

    /**
     * The array holding persistent {@link TopicImpl} instances.
     */
    final List<ItemImpl> ITEMS = new ArrayList<>();

    /**
     * The array holding persistent {@link TopicImpl} instances.
     */
    final List<TopicImpl> TOPICS = new ArrayList<>();

    @Override
    public void clear() {
        // TODO Auto-generated method stub

    }

    @Override
    public void close() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean contains(Object entity) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public <T> EntityGraph<T> createEntityGraph(Class<T> rootType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public EntityGraph<?> createEntityGraph(String graphName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Query createNamedQuery(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> TypedQuery<T> createNamedQuery(String name,
                                              Class<T> resultClass) {
        return new MockTypedQuery<>(name,
                                    this);
    }

    @Override
    public StoredProcedureQuery createNamedStoredProcedureQuery(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Query createNativeQuery(String sqlString) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Query createNativeQuery(String sqlString,
                                   @SuppressWarnings("rawtypes") Class resultClass) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Query createNativeQuery(String sqlString,
                                   String resultSetMapping) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Query createQuery(@SuppressWarnings("rawtypes") CriteriaDelete deleteQuery) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> TypedQuery<T> createQuery(CriteriaQuery<T> criteriaQuery) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Query createQuery(@SuppressWarnings("rawtypes") CriteriaUpdate updateQuery) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Query createQuery(String qlString) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> TypedQuery<T> createQuery(String qlString,
                                         Class<T> resultClass) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String procedureName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String procedureName,
                                                           @SuppressWarnings("rawtypes") Class... resultClasses) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String procedureName,
                                                           String... resultSetMappings) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void detach(Object entity) {
        // TODO Auto-generated method stub

    }

    @Override
    public <T> T find(Class<T> entityClass,
                      Object primaryKey) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T find(Class<T> entityClass,
                      Object primaryKey,
                      LockModeType lockMode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T find(Class<T> entityClass,
                      Object primaryKey,
                      LockModeType lockMode,
                      Map<String, Object> properties) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T find(Class<T> entityClass,
                      Object primaryKey,
                      Map<String, Object> properties) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void flush() {
        // TODO Auto-generated method stub

    }

    @Override
    public CriteriaBuilder getCriteriaBuilder() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object getDelegate() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public EntityGraph<?> getEntityGraph(String graphName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> List<EntityGraph<? super T>> getEntityGraphs(Class<T> entityClass) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public FlushModeType getFlushMode() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public LockModeType getLockMode(Object entity) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Metamodel getMetamodel() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<String, Object> getProperties() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T getReference(Class<T> entityClass,
                              Object primaryKey) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public EntityTransaction getTransaction() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isJoinedToTransaction() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isOpen() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void joinTransaction() {
        // TODO Auto-generated method stub

    }

    @Override
    public void lock(Object entity,
                     LockModeType lockMode) {
        // TODO Auto-generated method stub

    }

    @Override
    public void lock(Object entity,
                     LockModeType lockMode,
                     Map<String, Object> properties) {
        // TODO Auto-generated method stub

    }

    @Override
    public <T> T merge(T entity) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void persist(Object entity) {
        if (entity instanceof NarrativeImpl) {
            final NarrativeImpl narrative = (NarrativeImpl) entity;
            NARRATIVES.add(narrative);
            return;
        }
        if (entity instanceof EndorserImpl) {
            final EndorserImpl endorser = (EndorserImpl) entity;
            ENDORSERS.add(endorser);
            return;
        }
        if (entity instanceof ItemImpl) {
            final ItemImpl item = (ItemImpl) entity;
            ITEMS.add(item);
            return;
        }
        if (entity instanceof TopicImpl) {
            final TopicImpl topic = (TopicImpl) entity;
            TOPICS.add(topic);
            return;
        }
    }

    @Override
    public void refresh(Object entity) {
        // TODO Auto-generated method stub

    }

    @Override
    public void refresh(Object entity,
                        LockModeType lockMode) {
        // TODO Auto-generated method stub

    }

    @Override
    public void refresh(Object entity,
                        LockModeType lockMode,
                        Map<String, Object> properties) {
        // TODO Auto-generated method stub

    }

    @Override
    public void refresh(Object entity,
                        Map<String, Object> properties) {
        // TODO Auto-generated method stub

    }

    @Override
    public void remove(Object entity) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setFlushMode(FlushModeType flushMode) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setProperty(String propertyName,
                            Object value) {
        // TODO Auto-generated method stub

    }

    @Override
    public <T> T unwrap(Class<T> cls) {
        // TODO Auto-generated method stub
        return null;
    }

}
