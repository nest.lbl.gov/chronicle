package gov.lbl.nest.chronicle.ejb;

import jakarta.persistence.EntityManager;

import gov.lbl.nest.chronicle.Chronicle;

/**
 * This class exposes package only constructors to be only used in testing.
 *
 * @author patton
 */
public class ChronicleImplFactory {

    /**
     * Create an instance of this class for testing only.
     *
     * @param entityManager
     *            the {@link EntityManager} that this object is using.
     * @param options
     *            the {@link ChronicleOptions} used by this object.
     * 
     * @return the created {@link Chronicle} instance.
     */
    public static Chronicle createInstance(EntityManager entityManager,
                                           ChronicleOptions options) {
        return new ChronicleImpl(entityManager,
                                 options);
    }

}
