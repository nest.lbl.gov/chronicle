package gov.lbl.nest.chronicle.ejb;

import java.util.Date;

import gov.lbl.nest.chronicle.Endorsement;
import gov.lbl.nest.chronicle.Endorser;

/**
 * {@link Endorsement} implementation for testing.
 */
public class MockEndorsement implements
                             Endorsement {

    private final Endorser endorser;
    private final String item;
    private final Date whenEndorsed;

    private MockEndorsement(String item,
                            String action,
                            Date whenEndorsed) {
        this.endorser = new Endorser() {

            @Override
            public String getAction() {
                return action;
            }
        };
        this.item = item;
        this.whenEndorsed = whenEndorsed;
    }

    @Override
    public Endorser getEndorser() {
        return endorser;
    }

    @Override
    public String getItem() {
        return item;
    }

    @Override
    public Date getWhenEndorsed() {
        return whenEndorsed;
    }
}
