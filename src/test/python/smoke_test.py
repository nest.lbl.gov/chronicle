import requests
import xml.dom.minidom
import xml.etree.ElementTree as ET

HEADERS = {'Content-Type': 'application/xml',
           'Accept': 'application/xml'}

def pretty_print(url, s, response = True):
    if None != url:
        if response:
            print('URL : Response : ' + url)
        else:
            print('URL : Request :  ' + url)
        print(xml.dom.minidom.parseString(s).toprettyxml())

session=requests.Session()

usdc_placement_url = 'https://lux-zeplin.lbl.gov/spade/usdc/report/timestamps/placed?max=8&after=2022-03-01T00:00:00&before=2022-03-01T01:00:00&registration=usdc.surf.raw.in'
r = session.get(usdc_placement_url)
digest = ET.fromstring(r.text)
pretty_print(usdc_placement_url, ET.tostring(digest))

subject = digest.find('subject')
subject.text = 'Action "USDC Placement" within the "Data Centers" topic'

load_url = 'http://localhost:8080/chronicle/local/command/load'
r = session.post(load_url, data=ET.tostring(digest), headers=HEADERS)
report = ET.fromstring(r.text)
pretty_print(load_url, r.text, True)

ukdc_placement_url = 'https://lux-zeplin.lbl.gov/spade/ukdc/report/timestamps/placed?max=8&after=2022-03-01T00:00:00&before=2022-03-01T01:00:00&registration=usdc.ukdc.raw.relayed'
r = session.get(ukdc_placement_url)
digest = ET.fromstring(r.text)
pretty_print(ukdc_placement_url, ET.tostring(digest))

subject = digest.find('subject')
subject.text = 'Action "UKDC Placement" within the "Data Centers" topic'

r = session.post(load_url, data=ET.tostring(digest), headers=HEADERS)
report = ET.fromstring(r.text)
pretty_print(load_url, r.text, True)

narrative_url = 'http://localhost:8080/chronicle/local/report/narrative/lz_202202281356_007558_000573_raw'
r = session.get(narrative_url)
report = ET.fromstring(r.text)
pretty_print(narrative_url, r.text, True)

endorsement_url = 'http://localhost:8080/chronicle/local/report/endorsements/USDC%20Placement/Data%20Centers'
r = session.get(endorsement_url)
report = ET.fromstring(r.text)
pretty_print(endorsement_url, r.text, True)
