#!/bin/bash

version=1.0.0.dev5

rm -fr WEB-INF
mkdir -p WEB-INF/classes/META-INF/services
mkdir -p WEB-INF/classes/gov/lbl/nest/chronicle/

./gradlew clean build

cp src/test/resources/META-INF/services/gov.lbl.nest.chronicle.EndorsementLifecycleFactory WEB-INF/classes/META-INF/services/
cp build/classes/java/test/gov/lbl/nest/chronicle/MockEndorsementLifecycleFactory*.class WEB-INF/classes/gov/lbl/nest/chronicle/
jar uf build/libs/chronicle-${version}.war $(find WEB-INF -type f)
cp build/libs/chronicle-${version}.war ~/wildfly/standalone/deployments/chronicle.war

sleep 8

python src/test/python/lifecycle_test.py

rm -fr WEB-INF