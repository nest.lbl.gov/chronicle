--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1
-- Dumped by pg_dump version 15.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: endorsement; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.endorsement (
    endorsementkey integer NOT NULL,
    endorser_endorserkey integer NOT NULL,
    narrative_narrativekey integer NOT NULL,
    whensigned timestamp(6) without time zone
);


--
-- Name: endorsement_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.endorsement_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: endorser; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.endorser (
    endorserkey integer NOT NULL,
    action character varying(64) NOT NULL
);


--
-- Name: endorser_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.endorser_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: item; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.item (
    itemkey integer NOT NULL,
    name character varying(255)
);


--
-- Name: item_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.item_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lastwatched; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lastwatched (
    lastwatchedkey integer NOT NULL,
    lastdatetime timestamp(6) without time zone NOT NULL,
    role character varying(255) NOT NULL
);


--
-- Name: lastwatched_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.lastwatched_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: narrative; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.narrative (
    item_itemkey integer,
    narrativekey integer NOT NULL,
    topic_topickey integer,
    whencreated timestamp(6) without time zone NOT NULL,
    whendiscarded timestamp(6) without time zone
);


--
-- Name: narrative_endorser; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.narrative_endorser (
    narrativeimpl_narrativekey integer NOT NULL,
    endorserimpls_endorserkey integer NOT NULL
);


--
-- Name: narrative_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.narrative_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: poi; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poi (
    guid_poiguidkey integer,
    level smallint,
    pointofinterestkey integer NOT NULL,
    status integer,
    type_poitypekey integer,
    whenoccurred timestamp(6) without time zone,
    CONSTRAINT poi_level_check CHECK (((level >= 0) AND (level <= 8)))
);


--
-- Name: poi_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.poi_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: poiattribute; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poiattribute (
    name_poiattributenamekey integer,
    poiattributekey integer NOT NULL,
    pointofinterest_pointofinterestkey integer,
    value character varying(255)
);


--
-- Name: poiattribute_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.poiattribute_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: poiattributename; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poiattributename (
    poiattributenamekey integer NOT NULL,
    value character varying(255)
);


--
-- Name: poiattributename_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.poiattributename_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: poiguid; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poiguid (
    poiguidkey integer NOT NULL,
    value character varying(255)
);


--
-- Name: poiguid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.poiguid_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: poitype; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poitype (
    poitypekey integer NOT NULL,
    value character varying(255)
);


--
-- Name: poitype_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.poitype_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: seenitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.seenitem (
    lastwatched_lastwatchedkey integer NOT NULL,
    seenitemkey integer NOT NULL,
    item character varying(256) NOT NULL
);


--
-- Name: seenitem_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.seenitem_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: topic; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.topic (
    topickey integer NOT NULL,
    name character varying(255)
);


--
-- Name: topic_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.topic_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: endorsement endorsement_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.endorsement
    ADD CONSTRAINT endorsement_pkey PRIMARY KEY (endorsementkey);


--
-- Name: endorser endorser_action_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.endorser
    ADD CONSTRAINT endorser_action_key UNIQUE (action);


--
-- Name: endorser endorser_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.endorser
    ADD CONSTRAINT endorser_pkey PRIMARY KEY (endorserkey);


--
-- Name: item item_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT item_name_key UNIQUE (name);


--
-- Name: item item_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (itemkey);


--
-- Name: lastwatched lastwatched_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lastwatched
    ADD CONSTRAINT lastwatched_pkey PRIMARY KEY (lastwatchedkey);


--
-- Name: narrative narrative_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.narrative
    ADD CONSTRAINT narrative_pkey PRIMARY KEY (narrativekey);


--
-- Name: poi poi_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poi
    ADD CONSTRAINT poi_pkey PRIMARY KEY (pointofinterestkey);


--
-- Name: poiattribute poiattribute_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattribute
    ADD CONSTRAINT poiattribute_pkey PRIMARY KEY (poiattributekey);


--
-- Name: poiattributename poiattributename_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattributename
    ADD CONSTRAINT poiattributename_pkey PRIMARY KEY (poiattributenamekey);


--
-- Name: poiguid poiguid_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiguid
    ADD CONSTRAINT poiguid_pkey PRIMARY KEY (poiguidkey);


--
-- Name: poitype poitype_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poitype
    ADD CONSTRAINT poitype_pkey PRIMARY KEY (poitypekey);


--
-- Name: seenitem seenitem_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seenitem
    ADD CONSTRAINT seenitem_pkey PRIMARY KEY (seenitemkey);


--
-- Name: topic topic_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.topic
    ADD CONSTRAINT topic_name_key UNIQUE (name);


--
-- Name: topic topic_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.topic
    ADD CONSTRAINT topic_pkey PRIMARY KEY (topickey);


--
-- Name: endorsement fk7ehk7aw6swqqb7kxpfor8dmip; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.endorsement
    ADD CONSTRAINT fk7ehk7aw6swqqb7kxpfor8dmip FOREIGN KEY (narrative_narrativekey) REFERENCES public.narrative(narrativekey);


--
-- Name: narrative_endorser fk9ah1101otp60uv7i45bufsqpo; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.narrative_endorser
    ADD CONSTRAINT fk9ah1101otp60uv7i45bufsqpo FOREIGN KEY (narrativeimpl_narrativekey) REFERENCES public.narrative(narrativekey);


--
-- Name: poi fkf2erw9j5mh3342utu16on4egp; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poi
    ADD CONSTRAINT fkf2erw9j5mh3342utu16on4egp FOREIGN KEY (guid_poiguidkey) REFERENCES public.poiguid(poiguidkey);


--
-- Name: endorsement fkgigu8jrxsf0grygmkl5hed1jp; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.endorsement
    ADD CONSTRAINT fkgigu8jrxsf0grygmkl5hed1jp FOREIGN KEY (endorser_endorserkey) REFERENCES public.endorser(endorserkey);


--
-- Name: poi fkgkqlhivye5d9x3ca1jwr6h08v; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poi
    ADD CONSTRAINT fkgkqlhivye5d9x3ca1jwr6h08v FOREIGN KEY (type_poitypekey) REFERENCES public.poitype(poitypekey);


--
-- Name: poiattribute fkhbmf9108sv87mr5eswkso49br; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattribute
    ADD CONSTRAINT fkhbmf9108sv87mr5eswkso49br FOREIGN KEY (pointofinterest_pointofinterestkey) REFERENCES public.poi(pointofinterestkey);


--
-- Name: seenitem fkle7rsa4d2lrpkjajtuvc6qmyj; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seenitem
    ADD CONSTRAINT fkle7rsa4d2lrpkjajtuvc6qmyj FOREIGN KEY (lastwatched_lastwatchedkey) REFERENCES public.lastwatched(lastwatchedkey);


--
-- Name: narrative fkmf0nnqrpfrftnl6rnlv630ces; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.narrative
    ADD CONSTRAINT fkmf0nnqrpfrftnl6rnlv630ces FOREIGN KEY (topic_topickey) REFERENCES public.topic(topickey);


--
-- Name: narrative fko7dbumwk6u2i6j4fcpbma0x94; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.narrative
    ADD CONSTRAINT fko7dbumwk6u2i6j4fcpbma0x94 FOREIGN KEY (item_itemkey) REFERENCES public.item(itemkey);


--
-- Name: poiattribute fkt89qj0m0jxapdi0mypkw02xgl; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattribute
    ADD CONSTRAINT fkt89qj0m0jxapdi0mypkw02xgl FOREIGN KEY (name_poiattributenamekey) REFERENCES public.poiattributename(poiattributenamekey);


--
-- Name: narrative_endorser fktegxsxf1xxo6rdv41j8dp3l43; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.narrative_endorser
    ADD CONSTRAINT fktegxsxf1xxo6rdv41j8dp3l43 FOREIGN KEY (endorserimpls_endorserkey) REFERENCES public.endorser(endorserkey);


--
-- PostgreSQL database dump complete
--

