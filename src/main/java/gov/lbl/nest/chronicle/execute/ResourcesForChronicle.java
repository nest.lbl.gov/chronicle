package gov.lbl.nest.chronicle.execute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.chronicle.ChronicleDB;
import gov.lbl.nest.chronicle.ImplementationString;
import gov.lbl.nest.common.execution.DeployedFiles;
import gov.lbl.nest.common.execution.DeployedFiles.DeployedFile;
import gov.lbl.nest.jee.monitor.InstrumentationDB;
import gov.lbl.nest.jee.watching.WatcherDB;
import jakarta.ejb.Lock;
import jakarta.ejb.LockType;
import jakarta.ejb.Singleton;
import jakarta.enterprise.inject.Produces;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 * This class provides resources to be injected into the Chronicle application.
 *
 * @author patton
 */
@Singleton
@Lock(value = LockType.READ)
public class ResourcesForChronicle {

    /**
     * Returns the version String containing this project's release.
     * 
     * @return the version String containing this project's release.
     */
    private static synchronized String getDeployedVersion() {
        if (null == version) {
            DeployedFile deployedFile = DeployedFiles.getDeployedWarFile();
            LOG.info("Using version \"" + deployedFile.version
                     + "\" of artifact \""
                     + deployedFile.project
                     + "\"");
            version = deployedFile.version;
        }
        return version;
    }

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ResourcesForChronicle.class);

    /**
     * The version String containing this project's release.
     */
    private static String version;

    /**
     * Expose an entity manager using the resource producer pattern
     */
    @PersistenceContext(unitName = "chronicle")
    private EntityManager entityManager;

    /**
     * Returns the {@link EntityManager} instance used by this application.
     *
     * @return the {@link EntityManager} instance used by this application.
     */
    @Produces
    @InstrumentationDB
    @ChronicleDB
    @WatcherDB
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Returns the implementation version.
     * 
     * @return the implementation version.
     */
    @Produces
    @ImplementationString
    public String getImplementationString() {
        getDeployedVersion();
        return version;
    }

}
