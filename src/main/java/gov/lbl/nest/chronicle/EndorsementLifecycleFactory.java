package gov.lbl.nest.chronicle;

import java.lang.reflect.Constructor;
import java.util.ServiceLoader;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is the basis from which to create
 * {@link EndorsementLifecycleFactory} implementations.
 *
 * @author patton
 */
public abstract class EndorsementLifecycleFactory {

    /**
     * The {@link EndorsementLifecycleFactory} implementation to use if none is
     * explicitly specified.
     */
    public final static EndorsementLifecycleFactory DEFAULT_ENDORSEMENT_LIFECYCLE_FACTORY = new EndorsementLifecycleFactory() {

        @Override
        public EndorsementLifecycle createEndorsementLifecycle() {
            return new EndorsementLifecycle() {

                @Override
                public void failedEndorsement(Chronicle chronicle,
                                              Narrative narrative,
                                              Endorsement endorsement) {
                }

                @Override
                public boolean preEndorsement(Chronicle chronicle,
                                              Narrative narrative,
                                              Endorsement endorsement) {
                    return true;
                }

                @Override
                public void successfulEndorsement(Chronicle chronicle,
                                                  Narrative narrative,
                                                  Endorsement endorsement) {
                }
            };
        }
    };

    /**
     * The {@link ServiceLoader} instance that will load the
     * {@link EndorsementLifecycleFactory} implementation.
     */
    private final static ServiceLoader<EndorsementLifecycleFactory> ENDORSEMENT_LIFECYCLE_FACTORY_LOADER = ServiceLoader.load(EndorsementLifecycleFactory.class,
                                                                                                                              EndorsementLifecycleFactory.class.getClassLoader());
    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(EndorsementLifecycleFactory.class);

    /**
     * true if the implementation class of this Object has already been logged.
     */
    private static AtomicBoolean LOGGING = new AtomicBoolean();

    /**
     * Creates an instance of the {@link EndorsementLifecycleFactory} class.
     *
     * @return the created instance of the {@link EndorsementLifecycleFactory}
     *         class.
     */
    public static EndorsementLifecycleFactory getEndorsementLifecycleFactory() {
        for (EndorsementLifecycleFactory instrumentation : ENDORSEMENT_LIFECYCLE_FACTORY_LOADER) {
            final Class<?> clazz = instrumentation.getClass();
            try {
                final Constructor<?> constructor = clazz.getConstructor();
                EndorsementLifecycleFactory result = (EndorsementLifecycleFactory) constructor.newInstance();
                if (!(LOGGING.get())) {
                    LOG.debug("Using \"" + clazz.getCanonicalName()
                              + "\" as JavaInstanceFactory implementation");
                    LOGGING.set(true);
                }
                return result;
            } catch (Exception e) {
                return null;
            }
        }
        if (!(LOGGING.get())) {
            LOG.info("Using default, " + DEFAULT_ENDORSEMENT_LIFECYCLE_FACTORY.getClass()
                     + " as EndorsementLifecycleFactory implementation");
            LOGGING.set(true);
        }
        return DEFAULT_ENDORSEMENT_LIFECYCLE_FACTORY;
    }

    /**
     * Create an instance of the {@link EndorsementLifecycle} interface.
     *
     * @return the instance of the {@link EndorsementLifecycle} interface.
     */
    abstract public EndorsementLifecycle createEndorsementLifecycle();
}
