package gov.lbl.nest.chronicle.ejb;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

import gov.lbl.nest.chronicle.Endorser;

/**
 * This class is a JPA implementation of the {@link Endorser} interface.
 *
 * @author patton
 */
@Entity
@Table(name = "endorser")
@NamedQueries({ @NamedQuery(name = "getEndorserByAction",
                            query = "SELECT e" + " FROM EndorserImpl e"
                                    + " WHERE e.action = :action"),
                @NamedQuery(name = "getEndorsersByActions",
                            query = "SELECT e" + " FROM EndorserImpl e"
                                    + " WHERE e.action IN (:actions)") })
public class EndorserImpl implements
                          Endorser {

    /**
     * This object's unique identifier within the set of {@link EndorserImpl}
     * instances.
     */
    private int endorserKey;

    /**
     * The action that this object represents.
     */
    private String action;

    /**
     * Create an instance of this class.
     */
    protected EndorserImpl() {
    }

    /**
     * Create an instance of this class.
     *
     * @param action
     *            the action that this object represents.
     */
    protected EndorserImpl(String action) {
        setAction(action);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if ((obj == null) || (getClass() != obj.getClass()))
            return false;
        EndorserImpl other = (EndorserImpl) obj;
        if (getAction() == null) {
            if (other.getAction() != null)
                return false;
        } else if (!getAction().equals(other.getAction()))
            return false;
        return true;
    }

    /**
     * Returns the action that this object represents.
     *
     * @return the action that this object represents.
     */
    @Override
    @Column(length = 64,
            nullable = false,
            unique = true)
    public String getAction() {
        return action;
    }

    /**
     * Returns this object's unique identifier within the set of
     * {@link EndorserImpl} instances.
     *
     * @return this object's unique identifier within the set of
     *         {@link EndorserImpl} instances.
     */
    @Id
    @GeneratedValue
    protected int getEndorserKey() {
        return endorserKey;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getAction() == null) ? 0
                                                         : getAction().hashCode());
        return result;
    }

    /**
     * Sets the action that this object represents.
     *
     * @param action
     *            the action that this object represents.
     */
    protected void setAction(String action) {
        this.action = action;
    }

    /**
     * Sets this object's unique identifier within the set of {@link EndorserImpl}
     * instances.
     *
     * @param key
     *            the unique value
     */
    protected void setEndorserKey(int key) {
        endorserKey = key;
    }
}
