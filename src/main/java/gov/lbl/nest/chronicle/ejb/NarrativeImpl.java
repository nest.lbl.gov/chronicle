package gov.lbl.nest.chronicle.ejb;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.Transient;

import gov.lbl.nest.chronicle.Endorsement;
import gov.lbl.nest.chronicle.Endorser;
import gov.lbl.nest.chronicle.Narrative;

/**
 * This class implements the {@link Narrative} interface using the JPA
 * architecture.
 *
 * @author patton
 */
@Entity
@Table(name = "narrative")
@NamedQueries({ @NamedQuery(name = "getAllNarrativesByItem",
                            query = "SELECT n" + " FROM NarrativeImpl n"
                                    + " WHERE n.whenDiscarded is null"
                                    + " AND n.item = :item"),
                @NamedQuery(name = "getNarrativeByItemAndTopic",
                            query = "SELECT n" + " FROM NarrativeImpl n"
                                    + " WHERE n.whenDiscarded is null"
                                    + " AND n.topic = :topic"
                                    + " AND n.item = :item"),
                @NamedQuery(name = "getNarrativesByItemsAndTopic",
                            query = "SELECT n" + " FROM NarrativeImpl n"
                                    + " WHERE n.whenDiscarded is null"
                                    + " AND n.topic = :topic"
                                    + " AND n.item IN (:items)"),
                @NamedQuery(name = "getNarrativesByItemNamesAndTopic",
                            query = "SELECT n" + " FROM NarrativeImpl n"
                                    + " WHERE n.whenDiscarded is null"
                                    + " AND n.topic = :topic"
                                    + " AND n.item.name in (:names)"),
                @NamedQuery(name = "getNarrativesByActionAndTopic",
                            query = "SELECT e.narrative" + " FROM EndorsementImpl e"
                                    + " WHERE e.narrative.topic = :topic"
                                    + " AND e.endorser = :endorser"),
                @NamedQuery(name = "subsetNarrativesByAction",
                            query = "SELECT e.narrative" + " FROM EndorsementImpl e"
                                    + " WHERE e.narrative IN (:narratives)"
                                    + " AND e.endorser = :endorser") })
public class NarrativeImpl implements
                           Narrative {

    /**
     * The sequence of {@link EndorsementImpl} instances that have been made of far
     * with the most recent first.
     */
    private List<EndorsementImpl> endorsements;

    /**
     * The sequence of {@link EndorserImpl} instances that have been made of far
     * with the most recent first.
     */
    private List<EndorserImpl> endorsers;

    /**
     * The {@link ItemImpl} instance associated with this object.
     */
    private ItemImpl item;

    /**
     * This object's unique identifier within the set of {@link NarrativeImpl}
     * instances.
     */
    private int narrativeKey;

    /**
     * The {@link TopicImpl} instance associated with this object.
     */
    private TopicImpl topic;

    /**
     * The date and time this object was created.
     */
    private Date whenCreated = new Date();

    /**
     * The date and time this object was discarded.
     */
    private Date whenDiscarded;

    /**
     * Create an instance of this class.
     */
    protected NarrativeImpl() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param item
     *            the {@link ItemImpl} instance associated with this object.
     * @param topic
     *            the {@link TopicImpl} instance associated with this object.
     * @param endorsers
     *            the collection of {@link Endorser} instances that still need to
     *            sign off on this Object.
     */
    public NarrativeImpl(ItemImpl item,
                         TopicImpl topic,
                         List<EndorserImpl> endorsers) {
        this.endorsers = endorsers;
        this.item = item;
        this.topic = topic;
    }

    @Transient
    @Override
    public Endorsement getEarliestEndorsement() {
        if (null == endorsements || endorsements.isEmpty()) {
            return null;
        }
        return endorsements.get(endorsements.size() - 1);
    }

    /**
     * Returns the list of {@link EndorserImpl} instances that have already been
     * endorsers.
     *
     * @return the list of {@link EndorserImpl} instances that have already been
     *         endorsers.
     */
    @Transient
    List<EndorserImpl> getEndorsedEndorsers() {
        final List<EndorsementImpl> endorsements = getEndorsementImpls();
        final List<EndorserImpl> result = new ArrayList<>();
        if (null != endorsements) {
            for (EndorsementImpl endorsement : endorsements) {
                final EndorserImpl endorser = endorsement.getEndorser();
                if (!result.contains(endorser)) {
                    result.add(endorser);
                }
            }
        }
        return result;
    }

    /**
     * Returns the sequence of {@link EndorsementImpl} instances that have been made
     * of far with the most recent first.
     *
     * @return the sequence of {@link EndorsementImpl} instances that have been made
     *         of far with the most recent first.
     */
    @OneToMany(mappedBy = "narrative")
    @OrderBy("whenEndorsed DESC")
    List<EndorsementImpl> getEndorsementImpls() {
        return endorsements;
    }

    @Override
    @Transient
    public List<? extends Endorsement> getEndorsements() {
        return endorsements;
    }

    /**
     * Returns the collection of {@link EndorserImpl} instances that are awaiting
     * endorsement.
     *
     * @return the collection of {@link EndorserImpl} instances that are awaiting
     *         endorsement.
     */
    @ManyToMany
    List<EndorserImpl> getEndorserImpls() {
        return endorsers;
    }

    @Override
    @Transient
    public List<? extends Endorser> getEndorsers() {
        return endorsers;
    }

    @Override
    @Transient
    public String getIdentity() {
        return getClass().getName() + ":"
               + item
               + "+"
               + topic;
    }

    @Override
    @ManyToOne
    public ItemImpl getItem() {
        return item;
    }

    /**
     * Returns the sequence of {@link EndorsementImpl} instances that have the same
     * specified {@link EndorserImpl} instance, if any, with the most recent first.
     *
     * @param endorser
     *            the {@link EndorserImpl} instance against which to test.
     *
     * @return the sequence of matching {@link EndorsementImpl} instances, if any,
     *         with the most recent first.
     */
    List<EndorsementImpl> getMatchingEndorsements(EndorserImpl endorser) {
        final List<EndorsementImpl> endorsements = getEndorsementImpls();
        final List<EndorsementImpl> result = new ArrayList<>();
        if (null != endorsements) {
            for (EndorsementImpl endorsement : endorsements) {
                if (endorser.equals(endorsement.getEndorser())) {
                    result.add(endorsement);
                }
            }
        }
        return result;
    }

    /**
     * Returns this object's unique identifier within the set of
     * {@link NarrativeImpl} instances.
     *
     * @return this object's unique identifier within the set of
     *         {@link NarrativeImpl} instances.
     */
    @Id
    @GeneratedValue
    protected int getNarrativeKey() {
        return narrativeKey;
    }

    @Override
    @ManyToOne
    public TopicImpl getTopic() {
        return topic;
    }

    /**
     * Returns the date and time this object was created.
     *
     * @return the date and time this object was created.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false,
            updatable = false)
    protected Date getWhenCreated() {
        return whenCreated;
    }

    /**
     * Returns the date and time this object was discarded.
     *
     * @return the date and time this object was discarded.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    protected Date getWhenDiscarded() {
        return whenDiscarded;
    }

    /**
     * Sets the sequence of {@link EndorsementImpl} instances that have been made so
     * far. The supplied collection must be date/time ordered with the most recent
     * first. The ENDORSEMENT_COMPARATOR of the {@link Narrative} interface can be
     * used to sort the sequence if necessary.
     *
     * @param endorsments
     *            the sequence of {@link EndorsementImpl} instances that have been
     *            made so far.
     */
    protected void setEndorsementImpls(List<EndorsementImpl> endorsments) {
        this.endorsements = endorsments;
    }

    /**
     * Sets the collection of {@link EndorserImpl} instances that are awaiting
     * endorsement.
     *
     * @param endorsers
     *            the collection of {@link EndorserImpl} instances that are awaiting
     *            endorsement.
     */
    protected void setEndorserImpls(List<EndorserImpl> endorsers) {
        this.endorsers = endorsers;
    }

    /**
     * Sets the {@link ItemImpl} instance associated with this object.
     *
     * @param item
     *            the {@link ItemImpl} instance associated with this object.
     */
    protected void setItem(ItemImpl item) {
        this.item = item;
    }

    /**
     * Sets this object's unique identifier within the set of {@link NarrativeImpl}
     * instances.
     *
     * @param narrativeKey
     *            this object's unique identifier within the set of
     *            {@link NarrativeImpl} instances.
     */
    protected void setNarrativeKey(int narrativeKey) {
        this.narrativeKey = narrativeKey;
    }

    /**
     * Sets the {@link TopicImpl} instance associated with this object.
     *
     * @param topic
     *            the {@link TopicImpl} instance associated with this object.
     */
    protected void setTopic(TopicImpl topic) {
        this.topic = topic;
    }

    /**
     * Sets the date and time this object was created.
     *
     * @param dateTime
     *            the date and time this object was created.
     */
    protected void setWhenCreated(Date dateTime) {
        whenCreated = dateTime;
    }

    /**
     * Sets the date and time this object was discarded.
     *
     * @param dateTime
     *            the date and time this object was discarded.
     */
    protected void setWhenDiscarded(Date dateTime) {
        whenDiscarded = dateTime;
    }

}
