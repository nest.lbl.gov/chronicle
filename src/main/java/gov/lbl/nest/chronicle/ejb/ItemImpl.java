package gov.lbl.nest.chronicle.ejb;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

import gov.lbl.nest.chronicle.Item;

/**
 * This class implements the {@link Item} interface using the JPA architecture.
 *
 * @author patton
 */
@Entity
@Table(name = "item")
@NamedQueries({ @NamedQuery(name = "getItemByName",
                            query = "SELECT i" + " FROM ItemImpl i"
                                    + " WHERE i.name = :name"),
                @NamedQuery(name = "getItemsByNames",
                            query = "SELECT i" + " FROM ItemImpl i"
                                    + " WHERE i.name IN (:names)") })
public class ItemImpl implements
                      Item {

    /**
     * This object's unique identifier within the set of {@link ItemImpl} instances.
     */
    private int itemKey;

    /**
     * The name of this object.
     */
    private String name;

    /**
     * Create an instance of this class.
     */
    protected ItemImpl() {
    }

    /**
     * Create an instance of this class.
     *
     * @param name
     *            the name of this object.
     */
    protected ItemImpl(String name) {
        setName(name);
    }

    /**
     * Returns this object's unique identifier within the set of {@link ItemImpl}
     * instances.
     *
     * @return this object's unique identifier within the set of {@link ItemImpl}
     *         instances.
     */
    @Id
    @GeneratedValue
    protected int getItemKey() {
        return itemKey;
    }

    @Override
    @Column(unique = true)
    public String getName() {
        return name;
    }

    /**
     * Sets this object's unique identifier within the set of {@link ItemImpl}
     * instances.
     *
     * @param key
     *            the unique value
     */
    protected void setItemKey(int key) {
        itemKey = key;
    }

    /**
     * Sets the name of this object.
     *
     * @param role
     *            the name of this object.
     */
    protected void setName(String role) {
        this.name = role;
    }
}
