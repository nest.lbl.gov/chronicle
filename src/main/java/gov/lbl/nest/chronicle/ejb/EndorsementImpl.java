package gov.lbl.nest.chronicle.ejb;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.Transient;

import gov.lbl.nest.chronicle.Endorsement;

/**
 * This class is a JPA implementation of the {@link Endorsement} interface.
 *
 * @author patton
 */
@Entity
@Table(name = "Endorsement")
@NamedQueries({ @NamedQuery(name = "getEndorsementsByAction",
                            query = "SELECT e" + " FROM NarrativeImpl n, IN (n.endorsementImpls) e"
                                    + " WHERE n.topic = :topic"
                                    + " AND e.endorser = :endorser"
                                    + " ORDER BY e.whenEndorsed ASC"),
                @NamedQuery(name = "getEndorsementsByActionAfterTime",
                            query = "SELECT e" + " FROM NarrativeImpl n, IN (n.endorsementImpls) e"
                                    + " WHERE n.topic = :topic"
                                    + " AND e.whenEndorsed >= :after"
                                    + " AND e.endorser = :endorser"
                                    + " ORDER BY e.whenEndorsed ASC"),
                @NamedQuery(name = "getEndorsementsByActionBeforeTime",
                            query = "SELECT e" + " FROM NarrativeImpl n, IN (n.endorsementImpls) e"
                                    + " WHERE n.topic = :topic"
                                    + " AND e.whenEndorsed < :before"
                                    + " AND e.endorser = :endorser"
                                    + " ORDER BY e.whenEndorsed ASC"),
                @NamedQuery(name = "getEndorsementsByActionDuringInterval",
                            query = "SELECT e" + " FROM NarrativeImpl n, IN (n.endorsementImpls) e"
                                    + " WHERE n.topic = :topic"
                                    + " AND e.whenEndorsed >= :after"
                                    + " AND e.whenEndorsed < :before"
                                    + " AND e.endorser = :endorser"
                                    + " ORDER BY e.whenEndorsed ASC") })
public class EndorsementImpl implements
                             Endorsement {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link NarrativeImpl} to which this object belongs.
     */
    private NarrativeImpl narrative;

    /**
     * This object's unique identifier within the set of {@link EndorsementImpl}
     * instances.
     */
    private int endorsementKey;

    /**
     * The actor that needs to see or act upon an item with the context of a topic.
     */
    private EndorserImpl endorser;

    /**
     * The date and time the narrative was signed by the actor.
     */
    private Date whenEndorsed;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected EndorsementImpl() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param narrative
     *            the {@link NarrativeImpl} to which this object belongs.
     * @param endorser
     *            the actor that needs to see or act upon an item with the context
     *            of a topic.
     * @param dateTime
     *            the date and time the narrative was signed by the actor.
     */
    protected EndorsementImpl(NarrativeImpl narrative,
                              EndorserImpl endorser,
                              Date dateTime) {
        setNarrative(narrative);
        setEndorser(endorser);
        setWhenEndorsed(dateTime);
    }

    // instance member method (alphabetic)

    /**
     * Returns this object's unique identifier within the set of
     * {@link EndorsementImpl} instances.
     *
     * @return this object's unique identifier within the set of
     *         {@link EndorsementImpl} instances.
     */
    @Id
    @GeneratedValue
    protected int getEndorsementKey() {
        return endorsementKey;
    }

    @Override
    @ManyToOne(optional = false,
               fetch = FetchType.EAGER)
    public EndorserImpl getEndorser() {
        return endorser;
    }

    @Override
    @Transient
    public String getItem() {
        return (getNarrative().getItem()).getName();
    }

    /**
     * The {@link NarrativeImpl} to which this object belongs.
     *
     * @return the {@link NarrativeImpl} to which this object belongs.
     */
    @ManyToOne(optional = false)
    protected NarrativeImpl getNarrative() {
        return narrative;
    }

    /**
     * Returns the date and time the narrative was signed by the actor.
     *
     * @return the date and time the narrative was signed by the actor.
     */
    @Override
    // for backwards compatibility
    @Column(name = "whenSigned")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenEndorsed() {
        return whenEndorsed;
    }

    /**
     * Sets this object's unique identifier within the set of
     * {@link EndorsementImpl} instances.
     *
     * @param key
     *            the unique value
     */
    protected void setEndorsementKey(int key) {
        endorsementKey = key;
    }

    /**
     * Sets actor that needs to see or act upon an item with the context of a topic.
     *
     * @param endorser
     *            actor that needs to see or act upon an item with the context of a
     *            topic.
     */
    protected void setEndorser(EndorserImpl endorser) {
        this.endorser = endorser;
    }

    /**
     * Sets the {@link NarrativeImpl} to which this object belongs.
     *
     * @param narrative
     *            the {@link NarrativeImpl} to which this object belongs.
     */
    protected void setNarrative(NarrativeImpl narrative) {
        this.narrative = narrative;
    }

    /**
     * Sets the date and time the narrative was signed by the actor.
     *
     * @param dateTime
     *            the date and time the narrative was signed by the actor.
     */
    protected void setWhenEndorsed(Date dateTime) {
        this.whenEndorsed = dateTime;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
