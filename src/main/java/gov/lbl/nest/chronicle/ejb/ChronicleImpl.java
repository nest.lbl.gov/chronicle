package gov.lbl.nest.chronicle.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.NonUniqueResultException;
import jakarta.persistence.TypedQuery;
import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.chronicle.Chronicle;
import gov.lbl.nest.chronicle.ChronicleDB;
import gov.lbl.nest.chronicle.Endorsement;
import gov.lbl.nest.chronicle.EndorsementLifecycle;
import gov.lbl.nest.chronicle.EndorsementLifecycleFactory;
import gov.lbl.nest.chronicle.Item;
import gov.lbl.nest.chronicle.Narrative;
import gov.lbl.nest.chronicle.NarrativeExistsException;
import gov.lbl.nest.chronicle.NoSuchActionException;
import gov.lbl.nest.chronicle.NoSuchNarrativeException;
import gov.lbl.nest.chronicle.NoSuchTopicException;
import gov.lbl.nest.common.watching.Digest;

/**
 * This class is a JPA implementation of the {@link Narrative} interface.
 *
 * @author patton
 */
@Stateless
public class ChronicleImpl implements
                           Chronicle {

    /**
     * This class is used to gather the persistent objects for and
     * {@link Endorsement} instance so that the endorsement can be persisted.
     */
    static class LoadingEndorsement {

        /**
         * The persistent {@link EndorserImpl} instance.
         */
        final private EndorserImpl endorserImpl;

        /**
         * The persistent {@link ItemImpl} instance.
         */
        final private ItemImpl itemImpl;

        /**
         * The date and time when this object was endorsed.
         */
        final private Date whenEndorsed;

        /**
         * Creates an instance of this class.
         * 
         * @param itemImpl
         *            the persistent {@link ItemImpl} instance.
         * @param endorserImpl
         *            the persistent {@link EndorserImpl} instance.
         * @param whenEndorsed
         *            the date and time when this object was endorsed.
         */
        private LoadingEndorsement(ItemImpl itemImpl,
                                   EndorserImpl endorserImpl,
                                   Date whenEndorsed) {
            this.endorserImpl = endorserImpl;
            this.itemImpl = itemImpl;
            this.whenEndorsed = whenEndorsed;
        }
    }

    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(ChronicleImpl.class);

    private static final String[] KNOW_AS = new String[] { "a",
                                                           "b",
                                                           "c",
                                                           "d",
                                                           "e",
                                                           "f" };

    /**
     * The object to return when there are no {@link NarrativeImpl} instances to
     * return.
     */
    private static final List<NarrativeImpl> NO_NARRATIVES = Collections.unmodifiableList(new ArrayList<NarrativeImpl>());

    /**
     * The object to return when there are no item names to return.
     */
    private static final List<String> NO_NAMES = Collections.unmodifiableList(new ArrayList<String>());

    /**
     * The name to use for the default topic is none is specified.
     */
    private static final String UNNAMED_DEFAULT_TOPIC_NAME = "__default__";

    /**
     * The {@link ChronicleOptions} used by this object.
     */
    @Inject
    private ChronicleOptions chronicleOptions;

    /**
     * The {@link EndorsementLifecycle} instance used by this object.
     */
    private EndorsementLifecycle endorsementLifecycle = (EndorsementLifecycleFactory.getEndorsementLifecycleFactory()).createEndorsementLifecycle();

    /**
     * The {@link EntityManager} that this object is using.
     */
    @Inject
    @ChronicleDB
    private EntityManager entityManager;

    /**
     * Create an instance of t6his class.
     */
    protected ChronicleImpl() {
    }

    /**
     * Create an instance of this class for testing only.
     *
     * @param entityManager
     *            the {@link EntityManager} that this object is using.
     * @param options
     *            the {@link ChronicleOptions} used by this object.
     */
    ChronicleImpl(EntityManager entityManager,
                  ChronicleOptions options) {
        chronicleOptions = options;
        this.entityManager = entityManager;
    }

    @Override
    public List<? extends Narrative> assignActions(List<String> items,
                                                   String topic,
                                                   List<String> actions) throws IllegalArgumentException,
                                                                         IllegalStateException,
                                                                         NoSuchActionException,
                                                                         NoSuchTopicException {
        final List<NarrativeImpl> narratives = getNarrativeImpls(items,
                                                                 topic);
        final List<EndorserImpl> additionalEndorsers = lookupActions(actions,
                                                                     chronicleOptions.getAutomaticActions());
        final List<NarrativeImpl> result = new ArrayList<>(narratives.size());
        for (NarrativeImpl narrative : narratives) {
            result.add(assignEndorsers(narrative,
                                       additionalEndorsers));
        }
        return result;
    }

    @Override
    public Narrative assignActions(String item,
                                   String topic,
                                   List<String> actions) throws IllegalArgumentException,
                                                         IllegalStateException,
                                                         NoSuchNarrativeException,
                                                         NoSuchActionException,
                                                         NoSuchTopicException {
        final NarrativeImpl narrative = getNarrativeImpl(item,
                                                         topic);
        final List<EndorserImpl> additionalEndorsers = lookupActions(actions,
                                                                     chronicleOptions.getAutomaticActions());
        return assignEndorsers(narrative,
                               additionalEndorsers);
    }

    /**
     * Assigns {@link EndorserImpl} instances to the {@link Narrative} instance.
     * Only those {@link EndorserImpl} instances that are not already assigned the
     * the {@link Narrative} instance will be added, and if they are all already
     * assigned then {@link Narrative} instance will be unchanged.
     *
     * @param narrative
     *            the {@link NarrativeImpl} instance to be updated.
     * @param endorsers
     *            the set of {@link EndorserImpl} instance to add to the supplied
     *            {@link NarrativeImpl} instance.
     *
     * @return the endorsed {@link DocketsImpl} instance.
     */
    private NarrativeImpl assignEndorsers(NarrativeImpl narrative,
                                          List<EndorserImpl> endorsers) {
        final List<EndorserImpl> existingEndorsers = narrative.getEndorserImpls();
        final Iterator<EndorserImpl> iterator = endorsers.iterator();
        while (iterator.hasNext()) {
            if (existingEndorsers.contains(iterator.next())) {
                iterator.remove();
            }
        }
        final List<EndorserImpl> endorsedEndorsers = narrative.getEndorsedEndorsers();
        final Iterator<EndorserImpl> iterator2 = endorsers.iterator();
        while (iterator2.hasNext()) {
            if (endorsedEndorsers.contains(iterator2.next())) {
                iterator2.remove();
            }
        }
        existingEndorsers.addAll(endorsers);
        return narrative;
    }

    /**
     * Attempts to endorse the supplied {@link NarrativeImpl} instance it with the
     * specified {@link EndorserImpl} instance.
     *
     * @param narrative
     *            the {@link NarrativeImpl} instance being endorsed.
     * @param endorser
     *            the {@link EndorserImpl} instance doing the endorsement.
     * @param dateTime
     *            the date and time of the endorsement unless this is
     *            <code>null</code> in which case the current date and time will be
     *            used.
     *
     * @return true if the endorsement was successful.
     */
    private boolean attemptEndorsement(NarrativeImpl narrative,
                                       EndorserImpl endorser,
                                       Date dateTime) {
        final Date dateTimeToUse;
        dateTimeToUse = getDateToUse(dateTime);
        final EndorsementImpl endorsement = new EndorsementImpl(narrative,
                                                                endorser,
                                                                dateTimeToUse);
        if (!endorsementLifecycle.preEndorsement(this,
                                                 narrative,
                                                 endorsement)) {
            endorsementLifecycle.failedEndorsement(this,
                                                   narrative,
                                                   endorsement);
            return false;
        }
        LOG.debug("Created new endorsement for item \"" + endorsement.getItem()
                  + "\" in topic \""
                  + (narrative.getTopic()).getName()
                  + "\"");
        entityManager.persist(endorsement);
        final List<EndorsementImpl> endorsements = narrative.getEndorsementImpls();
        final List<EndorsementImpl> endorsementsToUse;
        final int index;
        if (null == endorsements) {
            endorsementsToUse = new ArrayList<>();
            narrative.setEndorsementImpls(endorsementsToUse);
            index = 0;
        } else {
            endorsementsToUse = endorsements;
            index = Collections.binarySearch(endorsementsToUse,
                                             endorsement,
                                             Narrative.ENDORSEMENT_COMPARATOR);
        }
        if (index >= 0) {
            endorsementsToUse.add(index,
                                  endorsement);
        } else {
            endorsementsToUse.add(-1 - index,
                                  endorsement);
        }
        endorsementLifecycle.successfulEndorsement(this,
                                                   narrative,
                                                   endorsement);
        return true;
    }

    @Override
    public boolean createAction(String action) throws IllegalStateException {
        if (null == action) {
            throw new NullPointerException();
        }
        TypedQuery<EndorserImpl> topicQuery = entityManager.createNamedQuery("getActionByName",
                                                                             EndorserImpl.class);
        topicQuery.setParameter("name",
                                action);
        try {
            topicQuery.getSingleResult();
            return true;
        } catch (NonUniqueResultException e) {
            throw new IllegalStateException("More than one Action matches the name \"" + action
                                            + "\"");
        } catch (NoResultException e1) {
            EndorserImpl result = new EndorserImpl(action);
            LOG.debug("Created new endorser \"" + result.getAction()
                      + "\"");
            entityManager.persist(result);
            return true;
        }
    }

    /**
     * Creates a mapping of {@link ItemImpl} instances, indexed by their Action
     * {@link String}.
     * 
     * @param endorsements
     *            the Collection of {@link Endorsement} instance for which to find
     *            the {@link ItemImpl} instances.
     * 
     * @return the mapping of {@link ItemImpl} instances, indexed by their Action
     *         {@link String}.
     * 
     * @throws NoSuchActionException
     */
    private Map<String, ItemImpl> createKeyedItems(List<? extends Endorsement> endorsements) {
        final List<String> names = new ArrayList<String>();
        for (Endorsement endorsement : endorsements) {
            final String name = endorsement.getItem();
            if (!names.contains(name)) {
                names.add(name);
            }
        }
        final List<ItemImpl> knownItems = lookupItems(names,
                                                      true);
        final Map<String, ItemImpl> result = new HashMap<String, ItemImpl>();
        for (ItemImpl item : knownItems) {
            result.put(item.getName(),
                       item);
        }
        return result;
    }

    /**
     * Creates a mapping of {@link NarrativeImpl} instances, indexed by their Item
     * {@link String}.
     * 
     * @param endorsements
     *            the Collection of {@link LoadingEndorsement} instance for which to
     *            find the {@link NarrativeImpl} instances.
     * @param topic
     *            the {@link TopicImpl} instance for which to find the
     *            {@link NarrativeImpl} instances.
     * 
     * @return the mapping of {@link NarrativeImpl} instances, indexed by their Item
     *         {@link String}.
     * 
     * @throws NoSuchActionException
     */
    private Map<String, NarrativeImpl> createKeyedNarratives(List<LoadingEndorsement> endorsements,
                                                             TopicImpl topic) {
        final List<LoadingEndorsement> loadingEndorsements = new ArrayList<LoadingEndorsement>();
        final List<String> foundItems = new ArrayList<String>();
        for (LoadingEndorsement loadingEndorsement : endorsements) {
            final ItemImpl item = loadingEndorsement.itemImpl;
            final String name = item.getName();
            if (!foundItems.contains(name)) {
                foundItems.add(name);
                loadingEndorsements.add(loadingEndorsement);
            }
        }
        final List<NarrativeImpl> knownNarratives = lookupNarratives(loadingEndorsements,
                                                                     topic);
        final Map<String, NarrativeImpl> result = new HashMap<String, NarrativeImpl>();
        for (NarrativeImpl narrative : knownNarratives) {
            result.put((narrative.getItem()).getName(),
                       narrative);
        }
        return result;
    }

    /**
     * Creates a {@link List} of {@link LoadingEndorsement} instances.
     * 
     * @param endorsements
     *            the {@link Collection} of {@link Endorsement} instance from which
     *            to create the {@link LoadingEndorsement} instances.
     * @param keyedEndorsers
     *            the mapping of {@link EndorserImpl} instances, indexed by their
     *            Action {@link String}.
     * @param keyedItems
     *            the mapping of {@link ItemImpl} instances, indexed by their Action
     *            {@link String}.
     * 
     * @return the {@link List} of {@link LoadingEndorsement} instances.
     */
    protected List<LoadingEndorsement> createLoadingEndorsements(List<? extends Endorsement> endorsements,
                                                                 Map<String, EndorserImpl> keyedEndorsers,
                                                                 Map<String, ItemImpl> keyedItems) {
        final List<LoadingEndorsement> result = new ArrayList<ChronicleImpl.LoadingEndorsement>();
        for (Endorsement endorsement : endorsements) {
            final String name = endorsement.getItem();
            final ItemImpl item = keyedItems.get(name);
            final String action = (endorsement.getEndorser()).getAction();
            final EndorserImpl endorser = keyedEndorsers.get(action);
            result.add(new LoadingEndorsement(item,
                                              endorser,
                                              endorsement.getWhenEndorsed()));
        }
        return result;
    }

    @Override
    public NarrativeImpl createNarrative(String item,
                                         String topic,
                                         List<String> actions) throws NarrativeExistsException,
                                                               IllegalStateException,
                                                               IllegalArgumentException,
                                                               NoSuchActionException,
                                                               NoSuchTopicException {
        if (null == actions || actions.isEmpty()) {
            throw new IllegalArgumentException("At least one action must be specified");
        }

        try {
            final NarrativeImpl narrative = getNarrativeImpl(item,
                                                             topic);
            if (null != narrative) {
                if (null == topic) {
                    throw new IllegalArgumentException("The item, \"" + item
                                                       + "\" is already being managed by the default topic");
                }
                throw new IllegalArgumentException("The item, \"" + item
                                                   + "\" is already being managed in topic \""
                                                   + topic
                                                   + "\"");
            }
        } catch (NoSuchTopicException e) {
            if (!chronicleOptions.getAutomaticTopics()) {
                throw e;
            }
        }
        final List<EndorserImpl> endorsers = lookupActions(actions,
                                                           chronicleOptions.getAutomaticActions());
        if (null == endorsers) {
            throw new NoSuchActionException();
        }
        final NarrativeImpl result = new NarrativeImpl(lookupItem(item,
                                                                  true),
                                                       lookupTopicOrDefault(topic,
                                                                            chronicleOptions.getAutomaticTopics()),
                                                       endorsers);
        LOG.debug("Created new narrative for item\" " + result.getItem()
                  + "\" in topic \""
                  + (result.getTopic()).getName()
                  + "\"");
        entityManager.persist(result);
        return result;
    }

    @Override
    public List<? extends Narrative> createNarratives(List<String> items,
                                                      String topic,
                                                      List<String> actions) throws NarrativeExistsException,
                                                                            IllegalStateException,
                                                                            IllegalArgumentException,
                                                                            NoSuchActionException,
                                                                            NoSuchTopicException {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Creates a mapping of {@link EndorserImpl} instances, indexed by their Action
     * {@link String}.
     * 
     * @param endorsements
     *            the Collection of {@link Endorsement} instance for which to find
     *            the {@link EndorserImpl} instances.
     * 
     * @return the mapping of {@link EndorserImpl} instances, indexed by their
     *         Action {@link String}.
     * 
     * @throws NoSuchActionException
     */
    private Map<String, EndorserImpl> createsKeyedEndorsers(List<? extends Endorsement> endorsements) throws NoSuchActionException {
        final List<String> actions = new ArrayList<String>();
        for (Endorsement endorsement : endorsements) {
            final String action = (endorsement.getEndorser()).getAction();
            if (!actions.contains(action)) {
                actions.add(action);
            }
        }
        final List<EndorserImpl> knownEndorsers = lookupActions(actions,
                                                                chronicleOptions.getAutomaticActions());
        final Map<String, EndorserImpl> result = new HashMap<String, EndorserImpl>();
        for (EndorserImpl endorser : knownEndorsers) {
            result.put(endorser.getAction(),
                       endorser);
        }
        return result;
    }

    @Override
    public boolean createTopic(String topic) throws IllegalStateException {
        if (null == topic) {
            throw new NullPointerException();
        }
        TypedQuery<TopicImpl> topicQuery = entityManager.createNamedQuery("getTopicByName",
                                                                          TopicImpl.class);
        topicQuery.setParameter("name",
                                topic);
        try {
            topicQuery.getSingleResult();
            return true;
        } catch (NonUniqueResultException e) {
            throw new IllegalStateException("More than one Topic matches the name \"" + topic
                                            + "\"");
        } catch (NoResultException e1) {
            TopicImpl result = new TopicImpl(topic);
            LOG.debug("Created new topic \"" + result.getName()
                      + "\"");
            entityManager.persist(result);
            return true;
        }
    }

    @Override
    public List<? extends Narrative> endorse(List<String> items,
                                             String topic,
                                             String action) throws IllegalArgumentException,
                                                            IllegalStateException,
                                                            NoSuchNarrativeException,
                                                            NoSuchActionException,
                                                            NoSuchTopicException {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Attempts to update the supplied {@link NarrativeImpl} instance by endorsing
     * it with the specified action.
     *
     * @param narrative
     *            the {@link NarrativeImpl} instance being endorsed.
     * @param endorser
     *            the {@link EndorserImpl} instance doing the endorsement.
     * @param dateTime
     *            the date and time of the endorsement unless this is
     *            <code>null</code> in which case the current date and time will be
     *            used.
     *
     * @return true if the narrative has been changed.
     *
     * @throws IllegalArgumentException
     *             when the action of the {@link EndorserImpl} instance is not
     *             assigned and can not be assigned automatically.
     */
    private boolean endorse(NarrativeImpl narrative,
                            EndorserImpl endorser,
                            Date dateTime) throws IllegalArgumentException {
        final Collection<EndorserImpl> endorsers = narrative.getEndorserImpls();
        if (endorsers.contains(endorser)) {
            if (attemptEndorsement(narrative,
                                   endorser,
                                   dateTime)) {
                endorsers.remove(endorser);
                return true;
            }
            return false;
        }
        final List<EndorsementImpl> matchingEndorsements = narrative.getMatchingEndorsements(endorser);
        if (matchingEndorsements.isEmpty()) {
            if (!chronicleOptions.getAutomaticEndorsers()) {
                throw new IllegalArgumentException("The action \"" + endorser.getAction()
                                                   + "\" is not assigned to the narrative for \""
                                                   + narrative.getItem()
                                                   + "\" within the context of the \""
                                                   + (narrative.getTopic()).getName()
                                                   + "\" topic");
            }
            return attemptEndorsement(narrative,
                                      endorser,
                                      dateTime);
        }
        final MultipleEndorsements multipleEndorsements = chronicleOptions.getMultipleEndorsements();
        if (MultipleEndorsements.IGNORE == multipleEndorsements) {
            return false;
        }
        if (MultipleEndorsements.FORBIDDEN == multipleEndorsements) {
            throw new IllegalArgumentException("\"" + narrative.getItem()
                                               + "\" has already been endorsed by \""
                                               + endorser.getAction()
                                               + "\" with the context of \""
                                               + (narrative.getTopic()).getName()
                                               + "\", and multiple endorsement are not permited");
        }
        final Date dateTimeToUse;
        dateTimeToUse = getDateToUse(dateTime);
        for (EndorsementImpl endorsement : matchingEndorsements) {
            if (dateTimeToUse.equals(endorsement.getWhenEndorsed())) {
                return false;
            }
        }
        if (MultipleEndorsements.ALLOWED == multipleEndorsements) {
            return attemptEndorsement(narrative,
                                      endorser,
                                      dateTime);
        }
        if (MultipleEndorsements.DUPLICATES == multipleEndorsements) {
            throw new IllegalArgumentException("\"" + narrative.getItem()
                                               + "\" has already been endorsed by \""
                                               + endorser.getAction()
                                               + "\" with the context of \""
                                               + (narrative.getTopic()).getName()
                                               + "\", and multiple endorsement are not permited");
        }
        final List<EndorsementImpl> endorsements = narrative.getEndorsementImpls();
        if (MultipleEndorsements.REPLACE == multipleEndorsements) {
            endorsements.removeAll(matchingEndorsements);
        }
        if (MultipleEndorsements.EARLIEST == multipleEndorsements) {
            final long time = dateTimeToUse.getTime();
            final long earliestTime = ((matchingEndorsements.get(matchingEndorsements.size() - 1)).getWhenEndorsed()).getTime();
            if (earliestTime >= time) {
                endorsements.removeAll(matchingEndorsements);
            } else {
                return false;
            }
        }
        if (MultipleEndorsements.LATEST == multipleEndorsements) {
            final long time = dateTimeToUse.getTime();
            final long latestTime = ((matchingEndorsements.get(matchingEndorsements.size() - 1)).getWhenEndorsed()).getTime();
            if (time >= latestTime) {
                endorsements.removeAll(matchingEndorsements);
            } else {
                return false;
            }
        }
        if (attemptEndorsement(narrative,
                               endorser,
                               dateTime)) {
            return true;
        }
        endorsements.addAll(matchingEndorsements);
        return false;
    }

    @Override
    public Narrative endorse(String item,
                             String topic,
                             String action) throws IllegalArgumentException,
                                            IllegalStateException,
                                            NoSuchNarrativeException,
                                            NoSuchActionException,
                                            NoSuchTopicException {
        return endorse(item,
                       topic,
                       action,
                       null);
    }

    /**
     * Endorses the appropriate narrative to record that the specified action as
     * dealt with the supplied item within the specified topic.
     *
     * @param item
     *            the identity of the item being managed.
     * @param topic
     *            the name of the topic that specifies the context of the item.
     * @param action
     *            the action, i.e. the actor, that has dealt with supplied item
     *            within the specified topic.
     * @param dateTime
     *            the date and time of the endorsement unless this is
     *            <code>null</code> in which case the current date and time will be
     *            used.
     *
     * @return the {@link Digest} instance of the narrative that was endorsed.
     *
     * @throws IllegalArgumentException
     *             if no action is specified.
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchNarrativeException
     *             when the specified narrative does not exist.
     * @throws NoSuchActionException
     *             when the action does not exist and was not created.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist.
     */
    private Narrative endorse(String item,
                              String topic,
                              String action,
                              Date dateTime) throws IllegalArgumentException,
                                             IllegalStateException,
                                             NoSuchNarrativeException,
                                             NoSuchActionException,
                                             NoSuchTopicException {
        final ItemImpl existingItem = lookupItem(item,
                                                 false);
        if (null == existingItem) {
            throw new NoSuchNarrativeException("The item " + item
                                               + " is unknown so there is no Narrative to endorse");
        }
        final TopicImpl existingTopic = lookupTopicOrDefault(topic,
                                                             false);
        final EndorserImpl endorser = lookupAction(action,
                                                   chronicleOptions.getAutomaticActions() && chronicleOptions.getAutomaticEndorsers());
        final NarrativeImpl narrative = getNarrativeImpl(existingItem,
                                                         existingTopic);
        endorse(narrative,
                endorser,
                dateTime);
        return narrative;
    }

    /**
     * Returns the {@link NarrativeImpl} instance, if any, that matches the supplied
     * items and topic. Items whose narratives can not be matched will be ignored.
     *
     * @param items
     *            the identity of the item whose {@link Narrative} instances should
     *            be returned.
     *
     * @return the collection of {@link Narrative} instances, if any, for the
     *         supplied item.
     *
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist.
     */
    private List<NarrativeImpl> getAllNarrativeImpls(String item) {
        if (null == item) {
            return NO_NARRATIVES;
        }
        final ItemImpl existingItem = lookupItem(item,
                                                 false);
        if (null == existingItem) {
            return NO_NARRATIVES;
        }
        TypedQuery<NarrativeImpl> query = entityManager.createNamedQuery("getAllNarrativesByItem",
                                                                         NarrativeImpl.class);
        query.setParameter("item",
                           existingItem);
        final List<NarrativeImpl> result = query.getResultList();
        if (result.isEmpty()) {
            return NO_NARRATIVES;
        }
        return result;

    }

    @Override
    public List<? extends Narrative> getAllNarratives(String item) {
        return getAllNarrativeImpls(item);
    }

    /**
     * Returns the date and time to use for comparing or storing
     * {@link EndorsementImpl} instances.
     *
     * @param dateTime
     *            the raw date and time of the endorsement.
     *
     * @return the date and time to use for comparing or storing
     *         {@link EndorsementImpl} instances.
     */
    private Date getDateToUse(Date dateTime) {
        final Date dateTimeToUse;
        final TimeUnit endorsementAccuracy = chronicleOptions.getEndorsementAccuracy();
        if (null == dateTime) {
            dateTimeToUse = new Date();
            if (!TimeUnit.MILLISECONDS.equals(endorsementAccuracy)) {
                dateTimeToUse.setTime(TimeUnit.MILLISECONDS.convert(endorsementAccuracy.convert(dateTimeToUse.getTime(),
                                                                                                TimeUnit.MILLISECONDS),
                                                                    endorsementAccuracy));
            }
        } else {
            if (TimeUnit.MILLISECONDS.equals(endorsementAccuracy)) {
                dateTimeToUse = dateTime;
            } else {
                dateTimeToUse = new Date();
                dateTimeToUse.setTime(TimeUnit.MILLISECONDS.convert(endorsementAccuracy.convert(dateTime.getTime(),
                                                                                                TimeUnit.MILLISECONDS),
                                                                    endorsementAccuracy));
            }
        }
        return dateTimeToUse;
    }

    /**
     * Returns the sequence of {@link EndorsementImpl} instances that match the
     * supplied action, topic, and filter parameters.
     *
     * @param action
     *            the action, i.e. the actor, whose endorsements should be returned.
     * @param topic
     *            the name of the topic that specifies the context of the
     *            endorsements.
     * @param after
     *            the date and time on or after which the action endorsed an item,
     *            <code>null</code> mean there is no lower limit.
     * @param before
     *            the date and time before which the action endorsed an item,
     *            <code>null</code> mean there is no upper limit.
     * @param max
     *            the maximum number of endorsements to return, 0 or less means
     *            unlimited.
     *
     * @return the list of all items that were signed off by a given action between
     *         the specified date and times.
     *
     * @throws NoSuchActionException
     * @throws IllegalArgumentException
     * @throws NoSuchTopicException
     * @throws IllegalStateException
     */
    private List<EndorsementImpl> getEndorsementImpls(String action,
                                                      String topic,
                                                      Date after,
                                                      Date before,
                                                      int max) throws IllegalArgumentException,
                                                               NoSuchActionException,
                                                               IllegalStateException,
                                                               NoSuchTopicException {
        final EndorserImpl endorser = lookupAction(action,
                                                   false);
        final TopicImpl existingTopic = lookupTopicOrDefault(topic,
                                                             false);
        final TypedQuery<EndorsementImpl> query;
        if (null == before) {
            if (null == after) {
                query = entityManager.createNamedQuery("getEndorsementsByAction",
                                                       EndorsementImpl.class);
            } else {
                query = entityManager.createNamedQuery("getEndorsementsByActionAfterTime",
                                                       EndorsementImpl.class);
                query.setParameter("after",
                                   after);
            }
        } else if (null == after) {
            query = entityManager.createNamedQuery("getEndorsementsByActionBeforeTime",
                                                   EndorsementImpl.class);
            query.setParameter("before",
                               before);
        } else {
            query = entityManager.createNamedQuery("getEndorsementsByActionDuringInterval",
                                                   EndorsementImpl.class);
            query.setParameter("after",
                               after);
            query.setParameter("before",
                               before);
        }
        query.setParameter("topic",
                           existingTopic);
        query.setParameter("endorser",
                           endorser);
        if (max > 0) {
            query.setMaxResults(max);
        }
        return query.getResultList();
    }

    @Override
    public List<? extends Endorsement> getEndorsements(String action,
                                                       String topic,
                                                       Date after,
                                                       Date before,
                                                       int max) throws IllegalArgumentException,
                                                                NoSuchActionException,
                                                                NoSuchTopicException {
        return getEndorsementImpls(action,
                                   topic,
                                   after,
                                   before,
                                   max);
    }

    @Override
    public List<String> getItems(List<String> actions,
                                 String topic) throws IllegalStateException,
                                               NoSuchTopicException,
                                               IllegalArgumentException,
                                               NoSuchActionException {

        LOG.debug("Looking up " + actions.size()
                  + " action(s)");
        final List<EndorserImpl> endorsers = lookupActions(actions,
                                                           false);

        if (null == endorsers || endorsers.isEmpty()) {
            LOG.debug("Found no action(s)");
            return NO_NAMES;
        }

        if (KNOW_AS.length < endorsers.size()) {
            throw new IllegalArgumentException("The actions list is limited to " + KNOW_AS.length
                                               + "actions");
        }

        // Sort EndorserImpl instance into "actions" order.
        LOG.debug("Sorting action(s)into request order)");
        final List<EndorserImpl> endorsersImpl = new ArrayList<>();
        for (String action : actions) {
            EndorserImpl endorserImpl = null;
            final Iterator<EndorserImpl> iterator = endorsers.iterator();
            while (null == endorserImpl && iterator.hasNext()) {
                final EndorserImpl candidate = iterator.next();
                if (action.equals(candidate.getAction())) {
                    endorsersImpl.add(candidate);
                }
            }
        }

        LOG.debug("Looking up topic \"" + topic
                  + "\"");
        final TopicImpl topicImpl = lookupTopic(topic,
                                                false);

        LOG.debug("Building Query");
        final String knownAs0 = KNOW_AS[0];
        final StringBuilder sb = new StringBuilder("SELECT " + knownAs0
                                                   + ".narrative"
                                                   + " FROM EndorsementImpl "
                                                   + knownAs0
                                                   + " WHERE "
                                                   + knownAs0
                                                   + ".narrative.topic = :topic"
                                                   + " AND "
                                                   + knownAs0
                                                   + ".endorser = :endorser0");

        final int finished = endorsersImpl.size();
        for (int index = 1;
             index < finished;
             ++index) {
            final String knownAs = KNOW_AS[index];
            sb.insert(0,
                      "SELECT " + knownAs
                         + ".narrative"
                         + " FROM EndorsementImpl "
                         + knownAs
                         + " WHERE "
                         + knownAs
                         + ".narrative IN (");
            sb.append(") AND " + knownAs
                      + ".endorser = :endorser"
                      + Integer.toString(index));
        }
        LOG.debug("Resulting Query is \"" + sb.toString()
                  + "\"");

        TypedQuery<NarrativeImpl> query = entityManager.createQuery(sb.toString(),
                                                                    NarrativeImpl.class);

        LOG.debug("Setting Query parameters");
        query.setParameter("topic",
                           topicImpl);
        for (int index = 0;
             index < finished;
             ++index) {
            query.setParameter("endorser" + Integer.toString(index),
                               endorsersImpl.get(index));
        }

        LOG.debug("Executing Query");
        final List<NarrativeImpl> narratives = query.getResultList();
//        LOG.debug("Sorting Narratives");
//        Collections.sort(narratives,
//                         Narrative.NARRATIVE_COMPARATOR);

        LOG.debug("Building Result");
        final List<String> result = new ArrayList<>();
        for (NarrativeImpl narrative : narratives) {
            result.add((narrative.getItem()).getName());
        }
        LOG.debug("Returning " + result.size()
                  + " Narrative name(s)");
        return result;
    }

    @Override
    public Narrative getNarrative(String item,
                                  String topic) throws IllegalStateException,
                                                NoSuchTopicException,
                                                NoSuchNarrativeException {
        return getNarrativeImpl(item,
                                topic);
    }

    /**
     * Returns the current {@link NarrativeImpl}, if any, for the specified item
     * with in the context of the supplied topic, <code>null</code> otherwise.
     *
     * @param item
     *            the {@link Item} instance whose {@link NarrativeImpl} should be
     *            returned.
     * @param topic
     *            the {@link TopicImpl} instance from which the
     *            {@link NarrativeImpl} item should be returned.
     *
     * @return the current {@link NarrativeImpl}, if any, for the specified item and
     *         topic, <code>null</code> otherwise.
     *
     * @throws IllegalStateException
     *             when more than one narrative matches the item with the context of
     *             the supplied topic.
     */
    private NarrativeImpl getNarrativeImpl(ItemImpl item,
                                           TopicImpl topic) throws IllegalStateException {
        TypedQuery<NarrativeImpl> narrativeQuery = entityManager.createNamedQuery("getNarrativeByItemAndTopic",
                                                                                  NarrativeImpl.class);
        narrativeQuery.setParameter("item",
                                    item);
        narrativeQuery.setParameter("topic",
                                    topic);
        try {
            return narrativeQuery.getSingleResult();
        } catch (NoResultException e) {
            return null;
        } catch (NonUniqueResultException e) {
            throw new IllegalStateException("More than one narrative exist for item \"" + item
                                            + "\" within the context of topic \""
                                            + topic.getName()
                                            + "\"");
        }
    }

    private NarrativeImpl getNarrativeImpl(String item,
                                           String topic) throws IllegalStateException,
                                                         NoSuchTopicException {
        final ItemImpl existingItem = lookupItem(item,
                                                 false);
        if (null == existingItem) {
            return null;
        }
        final TopicImpl existingTopic = lookupTopicOrDefault(topic,
                                                             chronicleOptions.getAutomaticTopics());
        return getNarrativeImpl(existingItem,
                                existingTopic);
    }

    /**
     * Returns the {@link NarrativeImpl} instance, if any, that matches the supplied
     * items and topic. Items whose narratives can not be matched will be ignored.
     *
     * @param names
     *            the identity of the item whose {@link Digest} should be returned.
     * @param topic
     *            the name of the topic that specifies the context of the item.
     *
     * @return the {@link Digest}, if any, for the supplied item and topic,
     *         <code>null</code> otherwise.
     *
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist.
     */
    private List<NarrativeImpl> getNarrativeImpls(List<String> names,
                                                  String topic) throws IllegalStateException,
                                                                NoSuchTopicException {
        if (null == names || names.isEmpty()) {
            return NO_NARRATIVES;
        }
        final TopicImpl existingTopic = lookupTopicOrDefault(topic,
                                                             chronicleOptions.getAutomaticTopics());
        TypedQuery<NarrativeImpl> query = entityManager.createNamedQuery("getNarrativesByItemNamesAndTopic",
                                                                         NarrativeImpl.class);
        query.setParameter("names",
                           names);
        query.setParameter("topic",
                           existingTopic);
        final List<NarrativeImpl> result = query.getResultList();
        return result;

    }

    @Override
    public List<? extends Narrative> getNarratives(List<String> items,
                                                   String topic) throws IllegalStateException,
                                                                 NoSuchTopicException {
        return getNarrativeImpls(items,
                                 topic);
    }

    @Override
    public Narrative load(String topic,
                          Endorsement input) throws IllegalArgumentException,
                                             IllegalStateException,
                                             NoSuchActionException,
                                             NoSuchTopicException {
        final List<Endorsement> endorsements = new ArrayList<>(1);
        endorsements.add(input);
        final List<? extends Narrative> loaded = load(topic,
                                                      endorsements);
        if (null == loaded) {
            return null;
        }
        return loaded.get(0);
    }

    @Override
    public List<? extends Narrative> load(String topic,
                                          List<? extends Endorsement> endorsements) throws IllegalArgumentException,
                                                                                    IllegalStateException,
                                                                                    NoSuchActionException,
                                                                                    NoSuchTopicException {
        final TopicImpl existingTopic = lookupTopicOrDefault(topic,
                                                             chronicleOptions.getAutomaticTopics());

        // The following is done to minimize the number of access to the DB.

        final Map<String, EndorserImpl> keyedEndorsers = createsKeyedEndorsers(endorsements);
        final Map<String, ItemImpl> keyedItems = createKeyedItems(endorsements);
        final List<LoadingEndorsement> loadingEndorsements = createLoadingEndorsements(endorsements,
                                                                                       keyedEndorsers,
                                                                                       keyedItems);
        final Map<String, NarrativeImpl> keyedNarratives = createKeyedNarratives(loadingEndorsements,
                                                                                 existingTopic);

        // All necessary persistent object should now have been created.
        entityManager.flush();

        final List<NarrativeImpl> narratives = new ArrayList<NarrativeImpl>();
        for (LoadingEndorsement endorsement : loadingEndorsements) {
            String name = (endorsement.itemImpl).getName();
            final NarrativeImpl narrative = keyedNarratives.get(name);
            String action = (endorsement.endorserImpl).getAction();
            final EndorserImpl endorser = keyedEndorsers.get(action);
            if (endorse(narrative,
                        endorser,
                        endorsement.whenEndorsed)
                && !narratives.contains(narrative)) {
                narratives.add(narrative);
            }
        }

        return narratives;
    }

    /**
     * Looks up a action and returns a matching {@link EndorserImpl} instance,
     * creating any that do not already exist if requested.
     *
     * @param action
     *            the action to look up.
     * @param create
     *            true when any unknown the {@link EndorserImpl} instance should be
     *            created.
     *
     * @return the {@link EndorserImpl} instance that matches the supplied action.
     *
     * @throws IllegalArgumentException
     *             if no actions are specified.
     * @throws NoSuchActionException
     *             when the one or more actions does not exist and was not created.
     */
    private EndorserImpl lookupAction(String action,
                                      boolean create) throws IllegalArgumentException,
                                                      NoSuchActionException {
        if (null == action) {
            throw new IllegalArgumentException("Action must be specified");
        }
        TypedQuery<EndorserImpl> query = entityManager.createNamedQuery("getEndorserByAction",
                                                                        EndorserImpl.class);
        query.setParameter("action",
                           action);
        try {
            return query.getSingleResult();
        } catch (NonUniqueResultException e) {
            throw new IllegalStateException("More than one Endorser matches the action \"" + action
                                            + "\"");
        } catch (NoResultException e1) {
            if (create) {
                EndorserImpl result = new EndorserImpl(action);
                LOG.debug("Created new endorser \"" + result.getAction()
                          + "\"");
                entityManager.persist(result);
                return result;
            } else {
                throw new NoSuchActionException("The action \"" + action
                                                + "\" is unknown.");
            }
        }
    }

    /**
     * Looks up the set of action strings and returns a list matching
     * {@link EndorserImpl} instances, creating any that do not already exist if
     * requested.
     *
     * @param actions
     *            the list of action strings to look up.
     * @param create
     *            true when any unknown the {@link EndorserImpl} instances should be
     *            created.
     *
     * @return the list of {@link EndorserImpl} instances that match the supplied
     *         actions, or <code>null</code> when there is one or more actions that
     *         do not exist.
     */
    private List<EndorserImpl> lookupActions(List<String> actions,
                                             boolean create) throws IllegalArgumentException,
                                                             NoSuchActionException {
        if (null == actions || actions.isEmpty()) {
            throw new IllegalArgumentException("At least one action must be specified");
        }
        TypedQuery<EndorserImpl> query = entityManager.createNamedQuery("getEndorsersByActions",
                                                                        EndorserImpl.class);
        query.setParameter("actions",
                           actions);
        final List<EndorserImpl> endorsers = query.getResultList();
        if (actions.size() == endorsers.size()) {
            return endorsers;
        }
        if (create) {
            final List<EndorserImpl> result = new ArrayList<>(endorsers);
            final List<String> knownActions = new ArrayList<>();
            for (EndorserImpl endorser : endorsers) {
                knownActions.add(endorser.getAction());
            }
            for (String action : actions) {
                if (!knownActions.contains(action)) {
                    final EndorserImpl endorser = new EndorserImpl(action);
                    LOG.debug("Created new endorser \"" + endorser.getAction()
                              + "\"");
                    entityManager.persist(endorser);
                    result.add(endorser);
                }
            }
            return result;
        } else {
            return null;
        }
    }

    /**
     * Looks up the item name and returns the matching {@link ItemImpl} instance.
     *
     * @param name
     *            the name of the {@link ItemImpl} to look up.
     * @param create
     *            true when any unknown the {@link ItemImpl} instances should be
     *            created.
     *
     * @return the {@link ItemImpl} instance that matches the supplied name, or
     *         <code>null</code> if it unknown and not created.
     *
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     */
    private ItemImpl lookupItem(String name,
                                boolean create) throws IllegalStateException {
        TypedQuery<ItemImpl> query = entityManager.createNamedQuery("getItemByName",
                                                                    ItemImpl.class);
        query.setParameter("name",
                           name);
        try {
            return query.getSingleResult();
        } catch (NonUniqueResultException e) {
            throw new IllegalStateException("More than one Item matches the name \"" + name
                                            + "\"");
        } catch (NoResultException e1) {
            if (create) {
                final ItemImpl result = new ItemImpl(name);
                LOG.debug("Created new item \"" + result.getName()
                          + "\"");
                entityManager.persist(result);
                return result;
            } else {
                return null;
            }
        }

    }

    /**
     * Looks up the set of name strings and returns a list matching {@link ItemImpl}
     * instances, creating any that do not already exist if requested.
     *
     * @param names
     *            the list of name strings to look up.
     * @param create
     *            true when any unknown the {@link ItemImpl} instances should be
     *            created.
     *
     * @return the list of {@link ItemImpl} instances that match the supplied
     *         actions, or <code>null</code> when there is one or more actions that
     *         do not exist.
     */
    private List<ItemImpl> lookupItems(List<String> names,
                                       boolean create) throws IllegalStateException {
        TypedQuery<ItemImpl> query = entityManager.createNamedQuery("getItemsByNames",
                                                                    ItemImpl.class);
        query.setParameter("names",
                           names);
        final List<ItemImpl> items = query.getResultList();
        if (names.size() == items.size()) {
            return items;
        }
        if (create) {
            final List<ItemImpl> result = new ArrayList<>(items);
            final List<String> knownItems = new ArrayList<>();
            for (ItemImpl item : items) {
                knownItems.add(item.getName());
            }
            for (String name : names) {
                if (!knownItems.contains(name)) {
                    final ItemImpl item = new ItemImpl(name);
                    LOG.debug("Created new item \"" + item.getName()
                              + "\"");
                    entityManager.persist(item);
                    result.add(item);
                }
            }
            return result;
        } else {
            return null;
        }
    }

    private List<NarrativeImpl> lookupNarratives(List<LoadingEndorsement> endorsements,
                                                 TopicImpl topic) {
        TypedQuery<NarrativeImpl> query = entityManager.createNamedQuery("getNarrativesByItemsAndTopic",
                                                                         NarrativeImpl.class);
        query.setParameter("topic",
                           topic);
        final List<ItemImpl> items = new ArrayList<ItemImpl>();
        final List<String> foundItems = new ArrayList<String>();
        for (LoadingEndorsement loadingEndorsement : endorsements) {
            final ItemImpl item = loadingEndorsement.itemImpl;
            final String name = (loadingEndorsement.itemImpl).getName();
            if (!foundItems.contains(name)) {
                foundItems.add(name);
                items.add(item);
            }
        }
        query.setParameter("items",
                           items);
        final List<NarrativeImpl> narratives = query.getResultList();
        if (narratives.size() == endorsements.size()) {
            return narratives;
        }

        final List<NarrativeImpl> result = new ArrayList<>(narratives);
        final List<String> knownNarratives = new ArrayList<>();
        for (NarrativeImpl narrative : narratives) {
            knownNarratives.add((narrative.getItem()).getName());
        }

        for (LoadingEndorsement endorsement : endorsements) {
            final ItemImpl itemImpl = endorsement.itemImpl;
            if (!knownNarratives.contains(itemImpl.getName())) {
                final List<EndorserImpl> endorsers = new ArrayList<EndorserImpl>();
                endorsers.add(endorsement.endorserImpl);
                final NarrativeImpl narrative = new NarrativeImpl(itemImpl,
                                                                  topic,
                                                                  endorsers);
                LOG.debug("Created new narrative for item \"" + itemImpl.getName()
                          + "\" in topic \""
                          + topic.getName()
                          + "\"");
                entityManager.persist(narrative);
                result.add(narrative);
            }
        }
        return result;
    }

    /**
     * Looks up the topic name and returns the matching {@link TopicImpl} instance.
     *
     * @param name
     *            the name of the {@link TopicImpl} to look up.
     * @param create
     *            true when an unknown the {@link TopicImpl} instance should be
     *            created.
     *
     * @return the {@link TopicImpl} instance that matches the supplied name.
     *
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist and was not created or,
     *             if the topic is <code>null</code>, there is no default topic.
     */
    private TopicImpl lookupTopic(String name,
                                  boolean create) throws IllegalStateException,
                                                  NoSuchTopicException {
        TypedQuery<TopicImpl> query = entityManager.createNamedQuery("getTopicByName",
                                                                     TopicImpl.class);
        query.setParameter("name",
                           name);
        try {
            return query.getSingleResult();
        } catch (NonUniqueResultException e) {
            throw new IllegalStateException("More than one Topic matches the name \"" + name
                                            + "\"");
        } catch (NoResultException e1) {
            if (create) {
                TopicImpl topic = new TopicImpl(name);
                LOG.debug("Created new topic \"" + topic.getName()
                          + "\"");
                entityManager.persist(topic);
                return topic;
            } else {
                throw new NoSuchTopicException("The topic \"" + name
                                               + "\" is unknown.");
            }
        }
    }

    /**
     * Looks up the topic name and returns the matching {@link TopicImpl} instance,
     * or the default {@link TopicImpl} instance if no name is specified/
     *
     * @param name
     *            the name of the {@link TopicImpl} to look up. If this
     *            {@link IsindexAction} <code>null</code> the default
     *            {@link TopicImpl} will be returned.
     * @param create
     *            true when an unknown the {@link TopicImpl} instance should be
     *            created.
     *
     * @return the {@link TopicImpl} instance that matches the supplied name.
     *
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist and was not created or,
     *             if the topic is <code>null</code>, there is no default topic.
     */
    private TopicImpl lookupTopicOrDefault(String name,
                                           boolean create) throws IllegalStateException,
                                                           NoSuchTopicException {
        if (null == name) {
            final TypedQuery<TopicImpl> query = entityManager.createNamedQuery("getAllTopics",
                                                                               TopicImpl.class);
            final List<TopicImpl> topics = query.getResultList();
            if (topics.isEmpty()) {
                if (create) {
                    TopicImpl topic = new TopicImpl(UNNAMED_DEFAULT_TOPIC_NAME);
                    LOG.debug("Created new topic \"" + name
                              + "\"");
                    entityManager.persist(topic);
                    return topic;
                } else {
                    throw new NoSuchTopicException("There is no default topic");
                }
            }
            return topics.get(0);
        }
        return lookupTopic(name,
                           create);
    }

    @Override
    public void setAutomaticActions(boolean automatic) {
        chronicleOptions.setAutomaticActions(automatic);
    }

    @Override
    public void setAutomaticEndorsers(boolean automatic) {
        chronicleOptions.setAutomaticEndorsers(automatic);
    }

    @Override
    public void setAutomaticTopics(boolean automatic) {
        chronicleOptions.setAutomaticTopics(automatic);
    }

    @Override
    public void setEndorsementAccuracy(TimeUnit unit) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setMultipleEndorsements(MultipleEndorsements multiple) {
        chronicleOptions.setMultipleEndorsements(multiple);
    }

    @Override
    public List<? extends Narrative> unassignActions(List<String> items,
                                                     String topic,
                                                     List<String> actions) throws IllegalArgumentException,
                                                                           IllegalStateException,
                                                                           NoSuchActionException,
                                                                           NoSuchTopicException {
        final List<NarrativeImpl> narratives = getNarrativeImpls(items,
                                                                 topic);
        final List<EndorserImpl> additionalEndorsers = lookupActions(actions,
                                                                     chronicleOptions.getAutomaticActions());
        final List<NarrativeImpl> result = new ArrayList<>(narratives.size());
        for (NarrativeImpl narrative : narratives) {
            if (unassignActions(narrative,
                                additionalEndorsers)) {
                result.add(narrative);
            }
        }
        return result;
    }

    /**
     * Unassigns {@link EndorserImpl} instances from the {@link Narrative} instance.
     * If an {@link EndorserImpl} instance has already been endorsed for the
     * {@link Narrative} instance then that instance will not be changed, i.e.
     * existing {@link EndorsementImpl} instances will not be deleted. Similarly, if
     * an action is not assigned to the {@link Narrative} instance it will not be
     * changed.
     *
     * @param narrative
     *            the {@link NarrativeImpl} instance to be updated.
     * @param endorsersToAssign
     *            the set of {@link EndorserImpl} instance to be removed from the
     *            supplied {@link NarrativeImpl} instance.
     *
     * @return true is the {@link Narrative} instance has changed.
     */
    private boolean unassignActions(NarrativeImpl narrative,
                                    List<EndorserImpl> endorsers) {
        final List<EndorserImpl> existingEndorsers = narrative.getEndorserImpls();
        return existingEndorsers.removeAll(endorsers);
    }

    @Override
    public Narrative unassignActions(String item,
                                     String topic,
                                     List<String> actions) throws IllegalArgumentException,
                                                           IllegalStateException,
                                                           NoSuchActionException,
                                                           NoSuchTopicException {
        final NarrativeImpl narrative = getNarrativeImpl(item,
                                                         topic);
        final List<EndorserImpl> endorsers = lookupActions(actions,
                                                           false);
        unassignActions(narrative,
                        endorsers);
        return narrative;
    }

}
