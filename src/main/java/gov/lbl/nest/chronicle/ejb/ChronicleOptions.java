package gov.lbl.nest.chronicle.ejb;

import java.util.concurrent.TimeUnit;

import jakarta.ejb.Singleton;

import gov.lbl.nest.chronicle.Chronicle.MultipleEndorsements;
import gov.lbl.nest.chronicle.Narrative;

/**
 * The class is the default implementation for options handling by
 * {@link ChronicleImpl} instances.
 *
 * @author patton
 */
@Singleton
public class ChronicleOptions {

    /**
     * True if this object should automatically create {@link EndorserImpl}
     * instances it does not know about when creating new endorsements.
     */
    private boolean automaticEndorsers = true;

    /**
     * True if this object should automatically create {@link EndorserImpl}
     * instances it does not know about when creating new {@link Narrative}
     * instances.
     */
    private boolean automaticActions = true;

    /**
     * True if this object should automatically create {@link TopicImpl} instances
     * it does not know about.
     */
    private boolean automaticTopics = true;

    /**
     * The accuracy with which endorsements are time stamped.
     */
    private TimeUnit endorsementAccuracy = TimeUnit.MILLISECONDS;

    /**
     * The action to take when multiple endorsements exist that have to the same
     * action and narrative.
     */
    private MultipleEndorsements multipleEndorsements = MultipleEndorsements.ALLOWED;

    /**
     * Returns true if this object should automatically create {@link EndorserImpl}
     * instances it does not know about when creating new {@link Narrative}
     * instances.
     *
     * @return true if this object should automatically create {@link EndorserImpl}
     *         instances it does not know about when creating new {@link Narrative}
     *         instances.
     */
    public boolean getAutomaticActions() {
        return automaticActions;
    }

    /**
     * Returns true if this object should automatically create {@link EndorserImpl}
     * instances it does not know about when creating new endorsements.
     *
     * @return true if this object should automatically create {@link EndorserImpl}
     *         instances it does not know about when creating new endorsements.
     */
    public boolean getAutomaticEndorsers() {
        return automaticEndorsers;
    }

    /**
     * Returns true if this object should automatically create {@link TopicImpl}
     * instances it does not know about.
     *
     * @return true if this object should automatically create {@link TopicImpl}
     *         instances it does not know about.
     */
    public boolean getAutomaticTopics() {
        return automaticTopics;
    }

    /**
     * Returns the accuracy with which endorsements are time stamped.
     *
     * @return the accuracy with which endorsements are time stamped.
     */
    public TimeUnit getEndorsementAccuracy() {
        return endorsementAccuracy;
    }

    /**
     * Returns the action to take when multiple endorsements exist that have to the
     * same action and narrative.
     *
     * @return the action to take when multiple endorsements exist that have to the
     *         same action and narrative.
     */
    public MultipleEndorsements getMultipleEndorsements() {
        return multipleEndorsements;
    }

    /**
     * Sets whether actions should be automatically created when they are unknown
     * when a narrative is created, or not.
     *
     * @param automatic
     *            true when actions should be automatically created.
     */
    public void setAutomaticActions(boolean automatic) {
        automaticActions = automatic;
    }

    /**
     * Sets whether actions should be automatically created when they are unknown
     * when a narrative is endorsed, or not.
     *
     * @param automatic
     *            true when actions should be automatically created.
     */
    public void setAutomaticEndorsers(boolean automatic) {
        automaticEndorsers = automatic;
    }

    /**
     * Sets whether topics should be automatically created when they are unknown, or
     * not.
     *
     * @param automatic
     *            true when topics should be automatically created.
     */
    public void setAutomaticTopics(boolean automatic) {
        automaticTopics = automatic;
    }

    /**
     * Sets the accuracy with which endorsements are time stamped.
     *
     * @param unit
     *            the {@link TimeUnit} constant specifying the accuracy.
     */
    public void setEndorsementAccuracy(TimeUnit unit) {
        endorsementAccuracy = unit;
    }

    /**
     * Sets the action to take when multiple endorsements exist that have to the
     * same action and narrative.
     *
     * @param multiple
     *            the action to take when multiple endorsements exist that have to
     *            the same action and narrative.
     */
    public void setMultipleEndorsements(MultipleEndorsements multiple) {
        multipleEndorsements = multiple;
    }
}
