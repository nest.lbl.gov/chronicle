package gov.lbl.nest.chronicle.ejb;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

import gov.lbl.nest.chronicle.Topic;

/**
 * This class implements the {@link Topic} interface using the JPA architecture.
 *
 * @author patton
 */
@Entity
@Table(name = "topic")
@NamedQueries({ @NamedQuery(name = "getAllTopics",
                            query = "SELECT t" + " FROM TopicImpl t"
                                    + " ORDER BY t.topicKey ASC"),
                @NamedQuery(name = "getCommonTopic",
                            query = "SELECT DISTINCT n.topic" + " FROM NarrativeImpl n"
                                    + " WHERE n.whenDiscarded is null"
                                    + " AND n.item IN (:items)"),
                @NamedQuery(name = "getTopicByName",
                            query = "SELECT t" + " FROM TopicImpl t"
                                    + " WHERE t.name = :name") })
public class TopicImpl implements
                       Topic {

    /**
     * This object's unique identifier within the set of {@link TopicImpl}
     * instances.
     */
    private int topicKey;

    /**
     * The name of this object.
     */
    private String name;

    /**
     * Create an instance of this class.
     */
    protected TopicImpl() {
    }

    /**
     * Create an instance of this class.
     *
     * @param name
     *            the name of this object.
     */
    protected TopicImpl(String name) {
        setName(name);
    }

    @Override
    @Column(unique = true)
    public String getName() {
        return name;
    }

    /**
     * Returns this object's unique identifier within the set of {@link TopicImpl}
     * instances.
     *
     * @return this object's unique identifier within the set of {@link TopicImpl}
     *         instances.
     */
    @Id
    @GeneratedValue
    protected int getTopicKey() {
        return topicKey;
    }

    /**
     * Sets the name of this object.
     *
     * @param role
     *            the name of this object.
     */
    protected void setName(String role) {
        this.name = role;
    }

    /**
     * Sets this object's unique identifier within the set of {@link TopicImpl}
     * instances.
     *
     * @param key
     *            the unique value
     */
    protected void setTopicKey(int key) {
        topicKey = key;
    }
}
