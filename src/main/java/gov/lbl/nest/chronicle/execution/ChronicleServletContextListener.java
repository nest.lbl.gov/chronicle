package gov.lbl.nest.chronicle.execution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

/**
 * 
 */
@WebListener
public class ChronicleServletContextListener implements
                                             ServletContextListener {

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ChronicleServletContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent contextEvent) {
        LOG.info("Chronicle Application starting");
    }

    @Override
    public void contextDestroyed(ServletContextEvent contextEvent) {
        LOG.info("Chronicle Application ending");
    }

}
