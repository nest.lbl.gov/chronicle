package gov.lbl.nest.chronicle.rs;

import java.net.URI;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * The class provides information on resources that can create elements of the
 * application.
 *
 * @author patton
 */
@XmlType(propOrder = { "narratives",
                       "actions",
                       "topic" })
public class Creation {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The URI of the resource that can create narratives.
     */
    private URI narratives;

    /**
     * The URI of the resource that can create actions.
     */
    private URI actions;

    /**
     * The URI of the resource that can create a topic.
     */
    private URI topic;

    // constructors

    /**
     * Creates an instance of this class.
     */
    public Creation() {

    }

    /**
     * Creates an instance of this class.
     *
     * @param narratives
     *            the URI of the resource that can create narratives.
     * @param actions
     *            the URI of the resource that can create actions.
     * @param topic
     *            the URI of the resource that can create a topic.
     */
    Creation(URI narratives,
             URI actions,
             URI topic) {
        setNarratives(narratives);
        setActions(actions);
        setTopic(topic);
    }

    // instance member method (alphabetic)

    /**
     * Returns the URI of the resource that can create actions.
     *
     * @return the URI of the resource that can create actions.
     */
    @XmlElement
    protected URI getActions() {
        return actions;
    }

    /**
     * Returns the URI of the resource that can create narratives.
     *
     * @return the URI of the resource that can create narratives.
     */
    @XmlElement
    protected URI getNarratives() {
        return narratives;
    }

    /**
     * Returns the URI of the resource that can create a topic.
     *
     * @return the URI of the resource that can create a topic.
     */
    @XmlElement
    protected URI getTopic() {
        return topic;
    }

    /**
     * Sets the URI of the resource that can create actions.
     *
     * @param uri
     *            the URI of the resource that can create actions.
     */
    protected void setActions(URI uri) {
        actions = uri;
    }

    /**
     * Sets the URI of the resource that can create narratives.
     *
     * @param uri
     *            the URI of the resource that can create narratives.
     */
    protected void setNarratives(URI uri) {
        narratives = uri;
    }

    /**
     * Sets the URI of the resource that can create a topic.
     *
     * @param uri
     *            the URI of the resource that can create a topic.
     */
    protected void setTopic(URI uri) {
        this.topic = uri;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
