package gov.lbl.nest.chronicle.rs;

import java.net.URI;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class represents a user's entry point to all accessible resources in the
 * application.
 *
 * @author patton
 */
@XmlRootElement(name = "application")
@XmlType(propOrder = { "uri",
                       "creation",
                       "endorse",
                       "assign",
                       "unassign",
                       "narratives",
                       "endorsements",
                       "load",
                       "specification",
                       "implementation" })
public class Application {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The URI to use to assign actions to existing narratives.
     */
    private URI assign;

    /**
     * The information on resources that can create elements of the application.
     */
    private Creation creation;

    /**
     * The URI to use to return to existing narratives.
     */
    private URI narratives;

    /**
     * The URI to use to endorse narratives.
     */
    private URI endorse;

    /**
     * The URI to use to return endorsements.
     */
    private URI endorsements;

    /**
     * The implementation version of this application.
     */
    private String implementation;

    /**
     * The URI to use to load information from and external source.
     */
    private URI load;

    /**
     * The specification version this application implements.
     */
    private String specification;

    /**
     * The URI to use to unassign actions from existing narratives.
     */
    private URI unassign;

    /**
     * The URI of this object.
     */
    private URI uri;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Application() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param uri
     *            the URI of this object.
     * @param creation
     *            the information on resources that can create elements of the
     *            application.
     * @param endorse
     *            the URI to use to endorse narratives.
     * @param assign
     *            the URI to use to assign actions to existing narratives.
     * @param unassign
     *            the URI to use to unassign actions from existing narratives.
     * @param narratives
     *            the URI to use to return to existing narratives.
     * @param endorsements
     *            the URI to use to return endorsements.
     * @param createNarrative
     *            the URI to use to create a new topic.
     * @param specification
     *            the specification version this application implements.
     * @param implementation
     *            the implementation version of this application.
     */
    Application(URI uri,
                Creation creation,
                URI endorse,
                URI assign,
                URI unassign,
                URI narratives,
                URI endorsements,
                URI load,
                String specification,
                String implementation) {
        setAssign(assign);
        setCreation(creation);
        setNarratives(narratives);
        setEndorse(endorse);
        setEndorsements(endorsements);
        setImplementation(implementation);
        setLoad(load);
        setSpecification(specification);
        setUnassign(unassign);
        setUri(uri);
    }

    // instance member method (alphabetic)

    /**
     * Returns the URI to use to assign actions to existing narratives.
     *
     * @return the URI to use to assign actions to existing narratives.
     */
    @XmlElement
    protected URI getAssign() {
        return assign;
    }

    /**
     * Returns the information on resources that can create elements of the
     * application.
     *
     * @return the information on resources that can create elements of the
     *         application.
     */
    @XmlElement(name = "create")
    protected Creation getCreation() {
        return creation;
    }

    /**
     * Returns the URI to use to endorse narratives.
     *
     * @return the URI to use to endorse narratives.
     */
    @XmlElement
    protected URI getEndorse() {
        return endorse;
    }

    /**
     * Returns the URI to use to return endorsements.
     *
     * @return the URI to use to return endorsements.
     */
    @XmlElement
    protected URI getEndorsements() {
        return endorsements;
    }

    /**
     * Returns the implementation version of this application.
     *
     * @return the implementation version of this application.
     */
    @XmlElement
    protected String getImplementation() {
        return implementation;
    }

    /**
     * Returns the URI to use to load information from and external source.
     *
     * @return the URI to use to load information from and external source.
     */
    @XmlElement
    protected URI getLoad() {
        return load;
    }

    /**
     * Returns the URI to use to return to existing narratives.
     *
     * @return the URI to use to return to existing narratives.
     */
    @XmlElement
    protected URI getNarratives() {
        return narratives;
    }

    /**
     * Returns the specification version this application implements.
     *
     * @return the specification version this application implements.
     */
    @XmlElement
    protected String getSpecification() {
        return specification;
    }

    /**
     * Returns the URI to use to unassign actions from existing narratives.
     *
     * @return the URI to use to unassign actions from existing narratives.
     */
    @XmlElement
    protected URI getUnassign() {
        return unassign;
    }

    /**
     * Returns the URI of this object.
     *
     * @return the URI of this object.
     */
    @XmlElement(name = "uri")
    protected URI getUri() {
        return uri;
    }

    /**
     * Sets the URI to use to assign actions to existing narratives.
     *
     * @param uri
     *            the URI to use to assign actions to existing narratives.
     */
    protected void setAssign(URI uri) {
        assign = uri;
    }

    /**
     * Sets the information on resources that can create elements of the
     * application.
     *
     * @param creation
     *            the information on resources that can create elements of the
     *            application.
     */
    protected void setCreation(Creation creation) {
        this.creation = creation;
    }

    /**
     * Sets the URI to use to endorse narratives.
     *
     * @param uri
     *            the URI to use to endorse narratives.
     */
    protected void setEndorse(URI uri) {
        endorse = uri;
    }

    /**
     * Sets the URI to use to return endorsements.
     *
     * @param uri
     *            the URI to use to return endorsements.
     */
    protected void setEndorsements(URI uri) {
        endorsements = uri;
    }

    /**
     * Sets the implementation version of this application.
     *
     * @param implementation
     *            the implementation version of this application.
     */
    protected void setImplementation(String implementation) {
        this.implementation = implementation;
    }

    /**
     * Sets the URI to use to load information from and external source.
     *
     * @param uri
     *            the URI to use to load information from and external source.
     */
    protected void setLoad(URI uri) {
        load = uri;
    }

    /**
     * Sets the URI to use to return to existing narratives.
     *
     * @param uri
     *            the URI to use to return to existing narratives.
     */
    protected void setNarratives(URI uri) {
        narratives = uri;
    }

    /**
     * Sets the specification version this application implements.
     *
     * @param specification
     *            the specification version this application implements.
     */
    protected void setSpecification(String specification) {
        this.specification = specification;
    }

    /**
     * Sets the URI to use to unassign actions from existing narratives.
     *
     * @param uri
     *            the URI to use to unassign actions from existing narratives.
     */
    protected void setUnassign(URI uri) {
        unassign = uri;
    }

    /**
     * Sets the URI of this object.
     *
     * @param uri
     *            the URI of this object.
     */
    protected void setUri(URI uri) {
        this.uri = uri;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
