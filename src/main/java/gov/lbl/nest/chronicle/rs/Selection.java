package gov.lbl.nest.chronicle.rs;

import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * The class selects a set of narratives and endorsers.
 *
 * @author patton
 */
@XmlRootElement(name = "selection")
@XmlType(propOrder = { "items",
                       "topic",
                       "actions" })
public class Selection {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The set of identities of the items being managed within the context of the
     * topic.
     */
    private List<String> items;

    /**
     * The set of actions, i.e. actors, that need to see or act upon the supplied
     * item and topic.
     */
    private List<String> actions;

    /**
     * The name of topic that defines the context for the items.
     */
    private String topic;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the set of actions, i.e. actors, that need to see or act upon the
     * supplied item and topic.
     *
     * @return the set of actions, i.e. actors, that need to see or act upon the
     *         supplied item and topic.
     */
    @XmlElement(name = "action")
    @XmlElementWrapper(name = "actions")
    protected List<String> getActions() {
        return actions;
    }

    /**
     * Returns the set of identities of the items being managed within the context
     * of the topic.
     *
     * @return the set of identities of the items being managed within the context
     *         of the topic.
     */
    @XmlElement(name = "item")
    @XmlElementWrapper(name = "items")
    protected List<String> getItems() {
        return items;
    }

    /**
     * Returns the name of topic that defines the context for the items.
     *
     * @return the name of topic that defines the context for the items.
     */
    @XmlElement
    protected String getTopic() {
        return topic;
    }

    /**
     * Sets the set of actions, i.e. actors, that need to see or act upon the
     * supplied item and topic.
     *
     * @param actions
     *            the set of actions, i.e. actors, that need to see or act upon the
     *            supplied item and topic.
     */
    protected void setActions(List<String> actions) {
        this.actions = actions;
    }

    /**
     * Sets the set of identities of the items being managed within the context of
     * the topic.
     *
     * @param items
     *            the set of identities of the items being managed within the
     *            context of the topic.
     */
    protected void setItems(List<String> items) {
        this.items = items;
    }

    /**
     * Sets the topic that defines the context for the items.
     *
     * @param name
     *            the name of topic that defines the context for the items.
     */
    protected void setTopic(String name) {
        topic = name;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
