package gov.lbl.nest.chronicle.rs;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

/**
 * The RESTful access to the SPADE application.
 *
 * @author patton
 */
@ApplicationPath("local")
public class ChronicleApplication extends
                                  Application {
    // No content
}
