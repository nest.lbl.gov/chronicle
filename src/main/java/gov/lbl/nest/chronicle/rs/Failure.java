package gov.lbl.nest.chronicle.rs;

import java.net.URI;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * The class returns information about a failure.
 *
 * @author patton
 */
@XmlRootElement
@XmlType(propOrder = { "message",
                       "uri" })
public class Failure {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The description of the failure.
     */
    private String message;
    /**
     * The URI that can be used to proceed with the application.
     */
    private URI uri;

    // constructors

    /**
     * Creates an instance of this class.
     */
    public Failure() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the description of the failure.
     * @param uri
     *            the URI that can be used to proceed with the application.
     */
    public Failure(String message,
                   URI uri) {
        setMessage(message);
        setUri(uri);
    }

    // instance member method (alphabetic)

    /**
     * Returns the description of the failure.
     *
     * @return the description of the failure.
     */
    @XmlElement
    public String getMessage() {
        return message;
    }

    /**
     * Returns the URI that can be used to proceed with the application.
     *
     * @return the URI that can be used to proceed with the application.
     */
    @XmlElement
    public URI getUri() {
        return uri;
    }

    /**
     * Sets the description of the failure.
     *
     * @param message
     *            the description of the failure.
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Sets the URI that can be used to proceed with the application.
     *
     * @param uri
     *            the URI that can be used to proceed with the application.
     */
    public void setUri(URI uri) {
        this.uri = uri;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
