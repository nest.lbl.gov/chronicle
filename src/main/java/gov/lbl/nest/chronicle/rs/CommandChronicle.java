package gov.lbl.nest.chronicle.rs;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;

import jakarta.ejb.EJBTransactionRolledbackException;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.core.UriInfo;
import jakarta.xml.bind.JAXBException;

import gov.lbl.nest.chronicle.Chronicle;
import gov.lbl.nest.chronicle.Endorsement;
import gov.lbl.nest.chronicle.Endorser;
import gov.lbl.nest.chronicle.Narrative;
import gov.lbl.nest.chronicle.NarrativeExistsException;
import gov.lbl.nest.chronicle.NoSuchActionException;
import gov.lbl.nest.chronicle.NoSuchTopicException;
import gov.lbl.nest.common.watching.ChangedItem;
import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.common.watching.DigestException;
import gov.lbl.nest.common.watching.Digests;

/**
 * The class provides a RESTful interface to execute Chronicle application
 * commands.
 *
 * @author patton
 *
 * @see ReportChronicle
 */
@Path("command")
@Stateless
public class CommandChronicle {

    private class InputEndorsement implements
                                   Endorsement {

        private final Endorser endorser;
        private final String item;
        private final Date whenEndorsed;

        private InputEndorsement(String item,
                                 String action,
                                 Date whenEndorsed) {
            this.endorser = new Endorser() {

                @Override
                public String getAction() {
                    return action;
                }
            };
            this.item = item;
            this.whenEndorsed = whenEndorsed;
        }

        @Override
        public Endorser getEndorser() {
            return endorser;
        }

        @Override
        public String getItem() {
            return item;
        }

        @Override
        public Date getWhenEndorsed() {
            return whenEndorsed;
        }
    }

    /**
     * The {@link Logger} used by this class.
     */
    // private static final Logger LOG =
    // LoggerFactory.getLogger(CommandNarratives.class);

    /**
     * The directory, if any, in which to store debug Digests.
     */
    private static final File DEBUG_PATH = null;// new File("debug/chronicle");

    /**
     * The {@link Chronicle} instance this instance is using.
     */
    @Inject
    private Chronicle chronicle;

    /**
     * Create an instance of this class.
     */
    protected CommandChronicle() {
    }

    /**
     * Create an instance of this class for testing only.
     *
     * @param chronicle
     *            the {@link Chronicle} instance used by the created object.
     */
    CommandChronicle(Chronicle chronicle) {
        this.chronicle = chronicle;
    }

    /**
     * Creates a new narrative for the supplied item within the specified topic
     * attaching the specified actionss.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the {@link Selection} instance selection the narratives to be
     *            created.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("assign")
    @POST
    @Consumes({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response assignActions(@Context UriInfo uriInfo,
                                  Selection selection) {
        final URI baseUri = uriInfo.getBaseUri();
        final List<String> items = selection.getItems();
        final String topic = selection.getTopic();
        final List<String> actions = selection.getActions();
        ResponseBuilder builder;
        if (null == items || items.isEmpty()) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure("No items specified",
                                       baseUri.resolve("report")));
            return builder.build();
        }
        if (null == actions || actions.isEmpty()) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure("No actions specified",
                                       baseUri.resolve("report")));
            return builder.build();
        }
        List<? extends Narrative> created;
        try {
            try {
                created = chronicle.assignActions(items,
                                                  topic,
                                                  actions);
            } catch (EJBTransactionRolledbackException e) {
                throw e.getCause();
            }
            final List<Digest> digests = new ArrayList<>(created.size());
            for (Narrative narrative : created) {
                digests.add(ReportChronicle.createDigestFromNarrative(narrative));
            }
            if (1 == digests.size()) {
                builder = Response.ok(digests.get(0));
            } else {
                builder = Response.ok(new Digests(digests));
            }
        } catch (IllegalArgumentException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (IllegalStateException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (NoSuchActionException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (NoSuchTopicException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (Throwable e) {
            e.printStackTrace();
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        }
        return builder.build();
    }

    /**
     * Creates a new narrative for the supplied item within the specified topic
     * attaching the specified actions.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the {@link Selection} instance selection the narratives to be
     *            created.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("create/narratives")
    @POST
    @Consumes({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response createNarrative(@Context UriInfo uriInfo,
                                    Selection selection) {
        final URI baseUri = uriInfo.getBaseUri();
        final List<String> items = selection.getItems();
        final String topic = selection.getTopic();
        final List<String> actions = selection.getActions();
        ResponseBuilder builder;
        if (null == items || items.isEmpty()) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure("No items specified",
                                       baseUri.resolve("report")));
            return builder.build();
        }
        if (null == actions || actions.isEmpty()) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure("No actions specified",
                                       baseUri.resolve("report")));
            return builder.build();
        }
        List<? extends Narrative> created;
        try {
            try {
                created = chronicle.createNarratives(items,
                                                     topic,
                                                     actions);
            } catch (EJBTransactionRolledbackException e) {
                throw e.getCause();
            }
            final List<Digest> digests = new ArrayList<>(created.size());
            for (Narrative narrative : created) {
                digests.add(ReportChronicle.createDigestFromNarrative(narrative));
            }
            if (1 == digests.size()) {
                builder = Response.ok(digests.get(0));
            } else {
                builder = Response.ok(new Digests(digests));
            }
        } catch (NarrativeExistsException e) {
            builder = Response.status(Status.CONFLICT);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (IllegalArgumentException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (IllegalStateException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (NoSuchActionException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (NoSuchTopicException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (Throwable e) {
            e.printStackTrace();
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        }
        return builder.build();
    }

    /**
     * Creates a new topic with the supplied name, if it does not already exist.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the {@link Selection} instance selection the narratives to be
     *            created.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("create/topic")
    @POST
    @Consumes({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response createTopic(@Context UriInfo uriInfo,
                                Selection selection) {
        final URI baseUri = uriInfo.getBaseUri();
        final String topic = selection.getTopic();
        ResponseBuilder builder;
        if (null == topic) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure("No topic specified",
                                       baseUri.resolve("report")));
            return builder.build();
        }
        try {
            try {
                if (chronicle.createTopic(topic)) {
                    builder = Response.created(null);
                } else {
                    builder = Response.status(Status.BAD_REQUEST);
                    builder.entity(new Failure("Failed to create Topic and it does not exist",
                                               baseUri.resolve("report")));
                }
            } catch (EJBTransactionRolledbackException e) {
                throw e.getCause();
            }
        } catch (IllegalStateException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (Throwable e) {
            e.printStackTrace();
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        }
        return builder.build();
    }

    /**
     * Endorses the appropriate narratives to record that the specified action as
     * dealt with the supplied items within the specified topic.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the {@link Selection} instance selection the narratives to be
     *            created.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("endorse")
    @POST
    @Consumes({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response endorse(@Context UriInfo uriInfo,
                            Selection selection) {
        final URI baseUri = uriInfo.getBaseUri();
        final List<String> items = selection.getItems();
        final String topic = selection.getTopic();
        final List<String> actions = selection.getActions();
        ResponseBuilder builder;
        if (null == items || items.isEmpty()) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure("No items specified",
                                       baseUri.resolve("report")));
            return builder.build();
        }
        if (null == actions || actions.isEmpty()) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure("No action specified",
                                       baseUri.resolve("report")));
            return builder.build();
        }
        if (1 != actions.size()) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure("Only one action can endorse items within any given request",
                                       baseUri.resolve("report")));
            return builder.build();
        }
        List<? extends Narrative> endorsed;
        try {
            try {
                endorsed = chronicle.endorse(items,
                                             topic,
                                             actions.get(0));
            } catch (EJBTransactionRolledbackException e) {
                throw e.getCause();
            }
            final List<Digest> digests = new ArrayList<>(endorsed.size());
            for (Narrative narrative : endorsed) {
                digests.add(ReportChronicle.createDigestFromNarrative(narrative));
            }
            if (1 == digests.size()) {
                builder = Response.ok(digests.get(0));
            } else {
                builder = Response.ok(new Digests(digests));
            }
        } catch (IllegalArgumentException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (IllegalStateException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (NoSuchActionException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (NoSuchTopicException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (Throwable e) {
            e.printStackTrace();
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        }
        return builder.build();
    }

    /**
     * Receives narrative information from an external source.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param digest
     *            the {@link Digest} instance contain
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("load")
    @POST
    @Consumes({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response load(@Context UriInfo uriInfo,
                         Digest digest) {
        final URI baseUri = uriInfo.getBaseUri();
        if (null != DEBUG_PATH) {
            try {
                DEBUG_PATH.mkdirs();
                ReportChronicle.save(digest,
                                     DEBUG_PATH);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        }
        ResponseBuilder builder;

        // Confirm structure of Digest subject
        final String subject = digest.getSubject();
        final Matcher matcher = ReportChronicle.SUBJECT_PATTERN.matcher(subject);
        if (!matcher.find() || ReportChronicle.SUBJECT_GROUP_COUNT != matcher.groupCount()) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure("The subject of the Digest \'" + subject
                                       + "\' is not a valid for importing into the application",
                                       baseUri.resolve("report")));
            return builder.build();
        }

        // Extract associated topic
        final String topic = matcher.group(ReportChronicle.TOPIC_GROUP);
        final String topicToUse;
        if (null == topic || "".equals(topic)
            || "default".equals(topic)) {
            topicToUse = null;
        } else {
            // Strip quotes are around explicit topic.
            topicToUse = topic.substring(ReportChronicle.QUOTE_LENGTH,
                                         topic.length() - ReportChronicle.QUOTE_LENGTH);
        }

        final List<Endorsement> inputs = new ArrayList<>();
        final List<? extends ChangedItem> issued = digest.getIssued();

        if (ReportChronicle.ACTION_TYPE.equals(matcher.group(ReportChronicle.TYPE_GROUP))) {

            // Handle an "Action" Digest
            final String action = matcher.group(ReportChronicle.NAME_GROUP);
            if (null == action || "".equals(action)) {
                builder = Response.status(Status.BAD_REQUEST);
                builder.entity(new Failure("The supplied Digest '" + subject
                                           + "\' is not a valid summary of a single action",
                                           baseUri.resolve("report")));
                return builder.build();
            }
            if (null != issued && !issued.isEmpty()) {
                for (ChangedItem item : issued) {
                    inputs.add(new InputEndorsement(item.getItem(),
                                                    action,
                                                    item.getWhenItemChanged()));
                }
            }
            final List<? extends String> queued = digest.getQueued();
            if (null != queued && !queued.isEmpty()) {
                for (String item : queued) {
                    inputs.add(new InputEndorsement(item,
                                                    action,
                                                    null));
                }
            }
        } else if (ReportChronicle.ITEM_TYPE.equals(matcher.group(ReportChronicle.TYPE_GROUP))) {

            // Handle an "Item" Digest
            final String item = matcher.group(ReportChronicle.NAME_GROUP);
            if (null == item || "".equals(item)) {
                builder = Response.status(Status.BAD_REQUEST);
                builder.entity(new Failure("The supplied Digest '" + subject
                                           + "\' is not a valid summary of a single item",
                                           baseUri.resolve("report")));
                return builder.build();
            }
            if (null != issued && !issued.isEmpty()) {
                for (ChangedItem action : issued) {
                    inputs.add(new InputEndorsement(item,
                                                    action.getItem(),
                                                    action.getWhenItemChanged()));
                }
            }
            final List<? extends String> queued = digest.getQueued();
            if (null != queued && !queued.isEmpty()) {
                for (String action : queued) {
                    inputs.add(new InputEndorsement(item,
                                                    action,
                                                    null));
                }
            }
        } else {

            // Handle unknown Digest
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure("The supplied Digest '" + subject
                                       + "\' is not a valid summary for this application",
                                       baseUri.resolve("report")));
            return builder.build();
        }

        List<? extends Narrative> result;
        try {
            try {
                result = chronicle.load(topicToUse,
                                        inputs);
            } catch (EJBTransactionRolledbackException e) {
                throw e.getCause();
            }
            final List<Digest> digests = new ArrayList<>(result.size());
            for (Narrative narrative : result) {
                digests.add(ReportChronicle.createDigestFromNarrative(narrative));
            }
            if (1 == digests.size()) {
                builder = Response.ok(digests.get(0));
            } else {
                builder = Response.ok(new Digests(digests));
            }
        } catch (IllegalArgumentException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (IllegalStateException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (DigestException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (Throwable e) {
            e.printStackTrace();
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        }
        return builder.build();
    }

    /**
     * Creates a new narrative for the supplied item within the specified topic
     * attaching the specified actions.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the {@link Selection} instance selection the narratives to be
     *            created.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("unassign")
    @POST
    @Consumes({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response unassignActions(@Context UriInfo uriInfo,
                                    Selection selection) {
        final URI baseUri = uriInfo.getBaseUri();
        final List<String> items = selection.getItems();
        final String topic = selection.getTopic();
        final List<String> actions = selection.getActions();
        ResponseBuilder builder;
        if (null == items || items.isEmpty()) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure("No items specified",
                                       baseUri.resolve("report")));
            return builder.build();
        }
        if (null == actions || actions.isEmpty()) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure("No actions specified",
                                       baseUri.resolve("report")));
            return builder.build();
        }
        List<? extends Narrative> created;
        try {
            try {
                created = chronicle.unassignActions(items,
                                                    topic,
                                                    actions);
            } catch (EJBTransactionRolledbackException e) {
                throw e.getCause();
            }
            final List<Digest> digests = new ArrayList<>(created.size());
            for (Narrative narrative : created) {
                digests.add(ReportChronicle.createDigestFromNarrative(narrative));
            }
            if (1 == digests.size()) {
                builder = Response.ok(digests.get(0));
            } else {
                builder = Response.ok(new Digests(digests));
            }
        } catch (IllegalArgumentException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (IllegalStateException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (NoSuchActionException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (NoSuchTopicException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (Throwable e) {
            e.printStackTrace();
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        }
        return builder.build();
    }
}
