package gov.lbl.nest.chronicle.rs;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import gov.lbl.nest.chronicle.Chronicle;
import gov.lbl.nest.chronicle.ChronicleException;
import gov.lbl.nest.chronicle.Endorsement;
import gov.lbl.nest.chronicle.Endorser;
import gov.lbl.nest.chronicle.ImplementationString;
import gov.lbl.nest.chronicle.Narrative;
import gov.lbl.nest.chronicle.NoSuchTopicException;
import gov.lbl.nest.chronicle.ejb.NarrativeImpl;
import gov.lbl.nest.common.rs.QueryDate;
import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.common.watching.DigestItem;
import gov.lbl.nest.common.watching.Digests;
import jakarta.ejb.EJBTransactionRolledbackException;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.core.UriInfo;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

/**
 * The class provides a RESTful interface to read access Chronicle application.
 *
 * @author patton
 *
 * @see ReportChronicle
 */
@Path("report")
@Stateless
public class ReportChronicle {

    /**
     * The string used to begin the subject line of an item's {@link Digest}
     * instance.
     */
    static final String ITEM_TYPE = "Item";

    /**
     * The string used to begin the subject line of an item's {@link Digest}
     * instance.
     */
    static final String ACTION_TYPE = "Action";

    /**
     * The string used to connect the type of a {@link Digest} instance to the
     * topic.
     */
    static final String SUBJECT_CONNECTOR = "within the";

    /**
     * The string used to end the subject line of an item's {@link Digest} instance.
     */
    static final String SUBJECT_END = "topic";

    /**
     * The regular expression pattern to extract the topic out of a {@link Digest}
     * instance's subject.
     */
    static final Pattern SUBJECT_PATTERN = Pattern.compile("^(" + ReportChronicle.ITEM_TYPE
                                                           + "|"
                                                           + ReportChronicle.ACTION_TYPE
                                                           + ") \"(.*)\" "
                                                           + ReportChronicle.SUBJECT_CONNECTOR
                                                           + " (\".*\"|default) "
                                                           + ReportChronicle.SUBJECT_END
                                                           + "$");

    /**
     * The group in {@link #SUBJECT_PATTERN} that contains the type of the name
     * associated with the {@link Digest} instance.
     */
    static final int TYPE_GROUP = 1;

    /**
     * The group in {@link #SUBJECT_PATTERN} that contains the name associated with
     * the {@link Digest} instance.
     */
    static final int NAME_GROUP = TYPE_GROUP + 1;

    /**
     * The group in {@link #SUBJECT_PATTERN} that contains the topic name associated
     * with the {@link Digest} instance.
     */
    static final int TOPIC_GROUP = NAME_GROUP + 1;

    /**
     * The number of groups in {@link #SUBJECT_PATTERN}.
     */
    static final int SUBJECT_GROUP_COUNT = TOPIC_GROUP;

    /**
     * The length of the quotes around the topic group.
     */
    static final int QUOTE_LENGTH = "\"".length();

    // private static final member data

    /**
     * The prefix used on debug filenames.
     */
    private static final String DEBUG_FILE_PREFIX = "DEBUG_";

    /**
     * The suffix used on debug files.
     */
    private static final String DEBUG_SUFFIX = ".xml";

    /**
     * The {@link Marshaller} property to set formatting.
     */
    private static final String XML_FORMAT_PROPERTY = "jaxb.formatted.output";

    /**
     * Creates a {@link Digest} summarizing the supplied {@link Endorsement}
     * instances.
     *
     * @param action
     *            the action, i.e. the actor, whose endorsements are provided.
     * @param topic
     *            the name of the topic that specifies the context of the
     *            endorsements.
     * @param endorsements
     *            the {@link NarrativeImpl} instances from which the {@link Digest}
     *            should be created.
     *
     * @return the created digest.
     */
    static Digest createDigestForAction(String topic,
                                        String action,
                                        final List<? extends Endorsement> endorsements) {
        final List<DigestChange> issued;
        if (null != endorsements && !endorsements.isEmpty()) {
            issued = new ArrayList<>();
            for (Endorsement endorsement : endorsements) {
                issued.add(new DigestChange(endorsement.getItem(),
                                            endorsement.getWhenEndorsed()));
            }
        } else {
            issued = null;
        }
        final String topicLabel;
        if (null == topic) {
            topicLabel = "default";
        } else {
            topicLabel = "\"" + topic
                         + "\"";
        }
        final Digest result = new Digest(ACTION_TYPE + " \""
                                         + action
                                         + "\" "
                                         + SUBJECT_CONNECTOR
                                         + " "
                                         + topicLabel
                                         + " "
                                         + SUBJECT_END,
                                         issued,
                                         null,
                                         null,
                                         null);
        return result;
    }

    /**
     * Creates a {@link Digest} summarizing the supplied {@link NarrativeImpl}.
     *
     * @param narrative
     *            the {@link NarrativeImpl} instance from which the {@link Digest}
     *            should be created.
     *
     * @return the created digest, or <code>null</code> if the supplied
     *         {@link NarrativeImpl} is null.
     */
    static Digest createDigestFromNarrative(final Narrative narrative) {
        if (null == narrative) {
            return null;
        }
        final List<? extends Endorsement> endorsements = narrative.getEndorsements();
        final List<DigestChange> issued;
        if (null != endorsements && !endorsements.isEmpty()) {
            issued = new ArrayList<>();
            for (Endorsement endorsement : endorsements) {
                issued.add(new DigestChange((endorsement.getEndorser()).getAction(),
                                            endorsement.getWhenEndorsed()));
            }
        } else {
            issued = null;
        }
        final Collection<? extends Endorser> endorsers = narrative.getEndorsers();
        final List<String> pending;
        if (null != endorsers && !endorsers.isEmpty()) {
            pending = new ArrayList<>(endorsers.size());
            for (Endorser endorser : endorsers) {
                pending.add(endorser.getAction());
            }
        } else {
            pending = null;
        }
        final Digest result = new Digest(ITEM_TYPE + " \""
                                         + (narrative.getItem()).getName()
                                         + "\" "
                                         + SUBJECT_CONNECTOR
                                         + " \""
                                         + (narrative.getTopic()).getName()
                                         + "\" "
                                         + SUBJECT_END,
                                         issued,
                                         null,
                                         null,
                                         pending);
        return result;
    }

    /**
     * Saves a {@link Digest} instance into the debug directory if it has been
     * specified.
     *
     * @param digest
     *            the {@link Digest} instance to save
     * @param debugPath
     *            the directory, if any, into which debug information will be saved.
     */
    static void save(Digest digest,
                     File debugPath) throws JAXBException {
        if (null == debugPath) {
            return;
        }

        int count = 0;
        final String dateTime = QueryDate.ZONED_FORMATTER.format(new Date());
        File file = new File(debugPath,
                             DEBUG_FILE_PREFIX + dateTime
                                        + DEBUG_SUFFIX);
        if (file.exists()) {
            ++count;
            file = new File(debugPath,
                            DEBUG_FILE_PREFIX + dateTime
                                       + "_"
                                       + count
                                       + DEBUG_SUFFIX);
        }
        JAXBContext content = JAXBContext.newInstance(digest.getClass());
        Marshaller marshaller = content.createMarshaller();
        marshaller.setProperty(XML_FORMAT_PROPERTY,
                               Boolean.TRUE);
        marshaller.marshal(digest,
                           file);
    }

    /**
     * The {@link Chronicle} instance this instance is using.
     */
    @Inject
    private Chronicle chronicle;

    /**
     * The implementation version.
     */
    @Inject
    @ImplementationString
    private String implementation;

    /**
     * Returns the representation of this application.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return the representation of this application.
     */
    @GET
    @Path("")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Application getApplication(@Context UriInfo uriInfo) {
        final URI baseUri = uriInfo.getBaseUri();
        return new Application(baseUri.resolve("report"),
                               new Creation(baseUri.resolve("command/create/narratives"),
                                            null,
                                            baseUri.resolve("command/create/topic")),
                               baseUri.resolve("command/endorse"),
                               baseUri.resolve("command/assign"),
                               baseUri.resolve("command/unassign"),
                               baseUri.resolve("report/narratives"),
                               baseUri.resolve("report/endorsements"),
                               baseUri.resolve("command/load"),
                               Chronicle.SPECIFICATION,
                               implementation);
    }

    /**
     * Returns the endorsements that match the supplied parameters.
     *
     * @param baseUri
     *            the base URI of the original request.
     * @param topic
     *            the name of the topic that specifies the context of the
     *            endorsements.
     * @param action
     *            the action, i.e. the actor, whose endorsements should be returned.
     * @param after
     *            the date and time on or after which the action endorsed an item,
     *            <code>null</code> mean there is no lower limit.
     * @param before
     *            the date and time before which the action endorsed an item,
     *            <code>null</code> mean there is no upper limit.
     * @param max
     *            the maximum number of endorsements to return, 0 or less means
     *            unlimited.
     *
     * @return the {@link Response} generated for this request.
     */
    private Response getEndorsements(URI baseUri,
                                     String action,
                                     String topic,
                                     String after,
                                     String before,
                                     Integer max) {
        ResponseBuilder builder;
        List<? extends Endorsement> endorsements;
        try {
            final int maxToUse;
            if (null == max) {
                maxToUse = 0;
            } else {
                maxToUse = max.intValue();
            }
            try {
                endorsements = chronicle.getEndorsements(action,
                                                         topic,
                                                         QueryDate.parseQueryDate(after),
                                                         QueryDate.parseQueryDate(before),
                                                         maxToUse);
            } catch (EJBTransactionRolledbackException e) {
                throw e.getCause();
            }
            final Digest digest = createDigestForAction(topic,
                                                        action,
                                                        endorsements);
            if (null != topic) {
                digest.setUri(baseUri.resolve("report/endorsements/" + action.replace(" ",
                                                                                      "%20")
                                              + "/"
                                              + topic.replace(" ",
                                                              "%20")));
            }
            builder = Response.ok(digest);
        } catch (IllegalArgumentException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (ChronicleException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (Throwable e) {
            e.printStackTrace();
            builder = Response.status(Status.INTERNAL_SERVER_ERROR);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        }
        return builder.build();
    }

    /**
     * Returns a {@link Digest} instance containing the endorsements that match the
     * selection and filter parameters.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the {@link Selection} instance selection the narratives to be
     *            created.
     * @param after
     *            the date and time on or after which the action endorsed an item,
     *            <code>null</code> mean there is no lower limit.
     * @param before
     *            the date and time before which the action endorsed an item,
     *            <code>null</code> mean there is no upper limit.
     * @param max
     *            the maximum number of endorsements to return, 0 or less means
     *            unlimited.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("endorsements")
    @POST
    @Consumes({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getEndorsements(@Context UriInfo uriInfo,
                                    Selection selection,
                                    @QueryParam("after") String after,
                                    @QueryParam("before") String before,
                                    @QueryParam("max") Integer max) {
        final URI baseUri = uriInfo.getBaseUri();
        final List<String> actions = selection.getActions();
        ResponseBuilder builder;
        if (null == actions || 1 != actions.size()) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure("One and only one action must be specified",
                                       baseUri.resolve("report")));
            return builder.build();
        }
        return getEndorsements(baseUri,
                               selection.getTopic(),
                               actions.get(0),
                               after,
                               before,
                               max);
    }

    /**
     * Returns a {@link Digest} instance containing the endorsements that match the
     * supplied action and filter parameters for the default topic.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param action
     *            the action, i.e. the actor, whose endorsements should be returned.
     * @param after
     *            the date and time on or after which the action endorsed an item,
     *            <code>null</code> mean there is no lower limit.
     * @param before
     *            the date and time before which the action endorsed an item,
     *            <code>null</code> mean there is no upper limit.
     * @param max
     *            the maximum number of endorsements to return, 0 or less means
     *            unlimited.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("endorsements/{action}")
    @GET
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getEndorsements(@Context UriInfo uriInfo,
                                    @PathParam("action") String action,
                                    @QueryParam("after") String after,
                                    @QueryParam("before") String before,
                                    @QueryParam("max") Integer max) {
        return getEndorsements(uriInfo.getBaseUri(),
                               action,
                               null,
                               after,
                               before,
                               max);
    }

    /**
     * Returns a {@link Digest} instance containing the endorsements that match the
     * supplied action, topic, and filter parameters.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param action
     *            the action, i.e. the actor, whose endorsements should be returned.
     * @param topic
     *            the name of the topic that specifies the context of the
     *            endorsements.
     * @param after
     *            the date and time on or after which the action endorsed an item,
     *            <code>null</code> mean there is no lower limit.
     * @param before
     *            the date and time before which the action endorsed an item,
     *            <code>null</code> mean there is no upper limit.
     * @param max
     *            the maximum number of endorsements to return, 0 or less means
     *            unlimited.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("endorsements/{action}/{topic}")
    @GET
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getEndorsements(@Context UriInfo uriInfo,
                                    @PathParam("action") String action,
                                    @PathParam("topic") String topic,
                                    @QueryParam("after") String after,
                                    @QueryParam("before") String before,
                                    @QueryParam("max") Integer max) {
        return getEndorsements(uriInfo.getBaseUri(),
                               action,
                               topic,
                               after,
                               before,
                               max);
    }

    /**
     * Returns a {@link Digest} instance containing the of items that have
     * endorsements of all the specified actions.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param topic
     *            the name of the topic that specifies the context of the
     *            endorsements.
     * @param actions
     *            the collection of actions, i.e. the actors, whose endorsements any
     *            returned item must have.
     * 
     * @return the {@link Digest} instance containing the of items that have all of
     *         the specified endorsements.
     */
    @Path("topic/{topic}/items")
    @GET
    public Response getItems(@Context UriInfo uriInfo,
                             @PathParam("topic") String topic,
                             @QueryParam("action") List<String> actions) {
        final URI baseUri = uriInfo.getBaseUri();
        ResponseBuilder builder;
        try {
            try {
                List<? extends String> items = chronicle.getItems(actions,
                                                                  topic);
                final List<DigestItem> digestItems = new ArrayList<>();
                for (String item : items) {
                    digestItems.add(new DigestItem(item));
                }
                builder = Response.ok(new Digest((URI) uriInfo.getRequestUri(),
                                                 "Items " + SUBJECT_CONNECTOR
                                                                                + " "
                                                                                + topic
                                                                                + " "
                                                                                + SUBJECT_END
                                                                                + " that have endorsements for all requested Endorsers",
                                                 digestItems));
            } catch (EJBTransactionRolledbackException e) {
                throw e.getCause();
            }
        } catch (IllegalStateException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (NoSuchTopicException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (Throwable e) {
            e.printStackTrace();
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        }
        return builder.build();

    }

    /**
     * Returns a {@link Digest} instance containing the narrative that matches the
     * supplied item and the default topic.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param item
     *            the item, within the context of the topic, whose narrative content
     *            should be returned.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("narrative/{item}")
    @GET
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getNarrative(@Context UriInfo uriInfo,
                                 @PathParam("item") String item) {
        return getNarrative(uriInfo,
                            item,
                            null);
    }

    // static member methods (alphabetic)

    /**
     * Returns a {@link Digest} instance containing the narrative that matches the
     * supplied item and topic.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param item
     *            the item, within the context of the topic, whose narrative content
     *            should be returned.
     * @param topic
     *            the name of the topic that specifies the context of the item.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("narrative/{item}/{topic}")
    @GET
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getNarrative(@Context UriInfo uriInfo,
                                 @PathParam("item") String item,
                                 @PathParam("topic") String topic) {
        final List<String> items = new ArrayList<>(1);
        items.add(item);
        return getNarratives(uriInfo.getBaseUri(),
                             items,
                             topic);
    }

    /**
     * Returns the narratives that match the supplied parameters.
     *
     * @param baseUri
     *            the base URI of the original request.
     * @param items
     *            the items, within the context of the topic, whose narratives
     *            content should be returned.
     * @param topic
     *            the name of the topic that specifies the context of the
     *            endorsements.
     *
     * @return the {@link Response} generated for this request.
     */
    private Response getNarratives(final URI baseUri,
                                   final List<String> items,
                                   final String topic) {
        ResponseBuilder builder;
        List<? extends Narrative> existing;
        try {
            try {
                existing = chronicle.getNarratives(items,
                                                   topic);
            } catch (EJBTransactionRolledbackException e) {
                throw e.getCause();
            }
            final List<Digest> digests = new ArrayList<>(existing.size());
            for (Narrative narrative : existing) {
                digests.add(ReportChronicle.createDigestFromNarrative(narrative));
            }
            if (1 == digests.size()) {
                builder = Response.ok(digests.get(0));
            } else {
                builder = Response.ok(new Digests(digests));
            }
        } catch (IllegalStateException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (NoSuchTopicException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (Throwable e) {
            e.printStackTrace();
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        }
        return builder.build();
    }

    /**
     * Returns a {@link Digest} instance containing the narratives that match the
     * selection.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param selection
     *            the {@link Selection} instance selection the narratives to be
     *            returned.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("narratives")
    @POST
    @Consumes({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getNarratives(@Context UriInfo uriInfo,
                                  Selection selection) {
        final URI baseUri = uriInfo.getBaseUri();
        final List<String> items = selection.getItems();
        ResponseBuilder builder;
        if (null == items || items.isEmpty()) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure("No items specified",
                                       baseUri.resolve("report")));
            return builder.build();
        }
        final String topic = selection.getTopic();
        return getNarratives(baseUri,
                             items,
                             topic);
    }

    /**
     * Returns a {@link Digest} instance containing the all of the narratives that
     * match the supplied item.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param item
     *            the item, within the context of the topic, whose narrative content
     *            should be returned.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("narratives/{item}")
    @GET
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getNarratives(@Context UriInfo uriInfo,
                                  @PathParam("item") String item) {
        final URI baseUri = uriInfo.getBaseUri();
        ResponseBuilder builder;
        List<? extends Narrative> existing;
        try {
            try {
                existing = chronicle.getAllNarratives(item);
            } catch (EJBTransactionRolledbackException e) {
                throw e.getCause();
            }
            final List<Digest> digests = new ArrayList<>(existing.size());
            for (Narrative narrative : existing) {
                digests.add(ReportChronicle.createDigestFromNarrative(narrative));
            }
            if (1 == digests.size()) {
                builder = Response.ok(digests.get(0));
            } else {
                builder = Response.ok(new Digests(digests));
            }
        } catch (IllegalStateException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (NoSuchTopicException e) {
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        } catch (Throwable e) {
            e.printStackTrace();
            builder = Response.status(Status.BAD_REQUEST);
            builder.entity(new Failure(e.getMessage(),
                                       baseUri.resolve("report")));
        }
        return builder.build();
    }
}
