package gov.lbl.nest.chronicle;

/**
 * This class represents an item that can have one of more associated
 * {@link Narrative} instances.
 *
 * @author patton
 */
public interface Item {

    /**
     * Returns the name of this object.
     *
     * @return the name of this object.
     */
    String getName();

}