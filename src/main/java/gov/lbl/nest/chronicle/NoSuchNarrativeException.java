package gov.lbl.nest.chronicle;

/**
 * This exception reports that a requested {@link Narrative} instance does not
 * exist and was not created.
 *
 * @author patton
 */
public class NoSuchNarrativeException extends
                                      ChronicleException {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by the Serialized mechanism.
     */
    private static final long serialVersionUID = -1L;

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    public NoSuchNarrativeException() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the detailed message.
     */
    public NoSuchNarrativeException(String message) {
        super(message);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
