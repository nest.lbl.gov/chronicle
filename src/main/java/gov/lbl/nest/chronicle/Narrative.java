package gov.lbl.nest.chronicle;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * This class connection endorsers and endorsements of a specified item in a
 * given topic.
 *
 * @author patton
 */
public interface Narrative {

    /**
     * The {@link Comparator} instance to keep {@link Endorsement} instances in the
     * correct order. The correct order being most recent first.
     */
    static final Comparator<Endorsement> ENDORSEMENT_COMPARATOR = new Comparator<>() {

        @Override
        public int compare(Endorsement arg0,
                           Endorsement arg1) {
            final Date lhs = arg0.getWhenEndorsed();
            final Date rhs = arg1.getWhenEndorsed();
            if (lhs.before(rhs)) {
                return 1;
            }
            if (lhs.after(rhs)) {
                return -1;
            }
            return 0;
        }
    };

    /**
     * The {@link Comparator} instance to keep {@link Narrative} instances in the
     * correct order. The correct order being most recent last.
     */
    static final Comparator<Narrative> NARRATIVE_COMPARATOR = new Comparator<>() {

        @Override
        public int compare(Narrative arg0,
                           Narrative arg1) {
            // Note parameters "swapped" as this order is the opposite of
            // ENDORSEMENT_COMPARATOR
            return ENDORSEMENT_COMPARATOR.compare(arg1.getEarliestEndorsement(),
                                                  arg0.getEarliestEndorsement());

        }
    };

    /**
     * Returns the earliest {@link Endorsement} instance, if any, that has been
     * made.
     *
     * @return the earliest {@link Endorsement} instances, if any, that has been
     *         made.
     */
    Endorsement getEarliestEndorsement();

    /**
     * Returns the sequence of {@link Endorsement} instances that have been made of
     * far with the most recent first.
     *
     * @return the sequence of {@link Endorsement} instances that have been made of
     *         far with the most recent first.
     */
    List<? extends Endorsement> getEndorsements();

    /**
     * Returns the collection of {@link Endorser} instances that are awaiting
     * endorsement.
     *
     * @return the collection of {@link Endorser} instances that are awaiting
     *         endorsement.
     */
    public List<? extends Endorser> getEndorsers();

    /**
     * Returns the identity of this Object.
     *
     * @return the identity of this Object.
     */
    String getIdentity();

    /**
     * returns the {@link Item} instance associated with this Object.
     *
     * @return the {@link Item} instance associated with this Object.
     */
    Item getItem();

    /**
     * Returns the {@link Topic} instance associated with this Object.
     *
     * @return the {@link Topic} instance associated with this Object.
     */
    Topic getTopic();
}