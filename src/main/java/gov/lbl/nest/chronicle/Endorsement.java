package gov.lbl.nest.chronicle;

import java.util.Date;

/**
 * This class captures the endorsement of an item, within a topic, by an action.
 *
 * @author patton
 */
public interface Endorsement {

    /**
     * Returns the {@link Endorser} instance that did this endorsement.
     *
     * @return the {@link Endorser} instance that did this endorsement.
     */
    Endorser getEndorser();

    /**
     * Returns the endorsed item.
     *
     * @return the endorsed item.
     */
    String getItem();

    /**
     * Returns the date and time when this object was endorsed.
     *
     * @return the date and time when this object was endorsed.
     */
    Date getWhenEndorsed();
}