package gov.lbl.nest.chronicle;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.common.watching.Digests;
import gov.lbl.nest.common.xml.item.Items;

/**
 * This is the interface through which interactions to the {@link Chronicle}
 * application are made.
 *
 * @author patton
 */
public interface Chronicle {

    /**
     * The enumeration of possible actions to take when there is an attempt to
     * create an {@link Endorsement} that already exists for a given action and
     * narrative.
     *
     * @author patton
     */
    public enum MultipleEndorsements {

                                      /**
                                       * Ignore any new endorsements whose action matches an existing one.
                                       */
                                      IGNORE,

                                      /**
                                       * More than one endorsement with matching actions is allowed.
                                       */
                                      ALLOWED,

                                      /**
                                       * Any new endorsements whose action matches an existing one will cause an
                                       * Exception to be thrown.
                                       */
                                      FORBIDDEN,

                                      /**
                                       * Any new endorsements whose action matches an existing one will cause an
                                       * Exception to be thrown unless it is a duplicate, within the endorsement
                                       * accuracy, of the existing one in which case it is ignored.
                                       */
                                      DUPLICATES,

                                      /**
                                       * Replace an existing endorsement whose action matches a new one with the new
                                       * endorsement. Note: The the case where behavior has been changed from
                                       * {@link #ALLOWED} to this, ALL existing endorsements with the matching action
                                       * will be replaced by the new one.
                                       */
                                      REPLACE,

                                      /**
                                       * Replace an existing endorsement whose action matches a new one with the new
                                       * endorsement if the new one's endorsed data and time is earlier then the
                                       * existing one's. Note: The the case where behavior has been changed from
                                       * {@link #ALLOWED} to this, ALL existing endorsements with the matching action
                                       * will be replaced if the new one is earlier than or equal to all of the
                                       * existing ones.
                                       */
                                      EARLIEST,

                                      /**
                                       * Replace an existing endorsement whose action matches a new one with the new
                                       * endorsement if the new one's endorsed data and time is later then the
                                       * existing one's. Note: The the case where behavior has been changed from
                                       * {@link #ALLOWED} to this, ALL existing endorsements with the matching action
                                       * will be replaced if the new one is later than or equal to all of the existing
                                       * ones.
                                       */
                                      LATEST;

    }

    /**
     * The version of the specification that this library implements.
     */
    static final String SPECIFICATION = "2.0";

    /**
     * Assigns actions to {@link Narrative} instances, if they exist, for the
     * supplied set of items within the context of specified topic. Only those
     * actions that does are not already assigned the a {@link Narrative} instance
     * will be added, and if they are all already assigned then the
     * {@link Narrative} instance will be unchanged. If there is no
     * {@link Narrative} instance for an item then it will be ignored.
     *
     * @param items
     *            the identities of the items to whose narratives the actions should
     *            be assigned
     * @param topic
     *            the name of the topic that specifies the context of the items.
     * @param actions
     *            the set of actions, i.e. actors that need to see or act upon the
     *            supplied item and topic, to be assigned to the existing
     *            narratives.
     *
     * @return the set of changed {@link Narrative} instances.
     *
     * @throws IllegalArgumentException
     *             if no actions are specified.
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchActionException
     *             when the one or more actions does not exist and was not created.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist and was not created or,
     *             if the topic is <code>null</code>, there is no default topic.
     */
    List<? extends Narrative> assignActions(List<String> items,
                                            String topic,
                                            List<String> actions) throws IllegalArgumentException,
                                                                  IllegalStateException,
                                                                  NoSuchActionException,
                                                                  NoSuchTopicException;

    /**
     * Assigns actions to the {@link Narrative} instance, if it exists, for the
     * supplied item within the context of specified topic. Only those actions that
     * does are not already assigned the the {@link Narrative} instance will be
     * added, and if they are all already assigned then {@link Narrative} instance
     * will be unchanged. If there is no {@link Narrative} instance for the item
     * then <code>null</code> will be returned.
     *
     * @param item
     *            the identity of the item to whose narrative the actions should be
     *            assigned
     * @param topic
     *            the name of the topic that specifies the context of the item.
     * @param actions
     *            the set of actions, i.e. actors that need to see or act upon the
     *            supplied item and topic, to be assigned to the existing narrative.
     *
     * @return the updated {@link Narrative} instances.
     *
     * @throws IllegalArgumentException
     *             if no actions are specified.
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchNarrativeException
     *             when the specified narrative does not exist.
     * @throws NoSuchActionException
     *             when the one or more actions does not exist and was not created.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist and was not created or,
     *             if the topic is <code>null</code>, there is no default topic.
     */
    Narrative assignActions(String item,
                            String topic,
                            List<String> actions) throws IllegalArgumentException,
                                                  IllegalStateException,
                                                  NoSuchNarrativeException,
                                                  NoSuchActionException,
                                                  NoSuchTopicException;

    /**
     * Creates a new action.
     *
     * @param action
     *            the name of the action to be created.
     *
     * @return true if the action was created or already existed.
     *
     * @throws IllegalStateException
     *             when there is more than one action with the supplied name.
     */
    boolean createAction(String action) throws IllegalStateException;

    /**
     * Creates a new narrative for the supplied item within the context of specified
     * topic attaching the specified actions.
     *
     * @param item
     *            the identity the item with which to create the new narratives.
     * @param topic
     *            the name of the topic that specifies the context of the item.
     * @param actions
     *            the set of actions, i.e. actors that need to see or act upon the
     *            supplied item and topic, to be assigned to the created narrative.
     *
     * @return the {@link Narrative} instance created.
     *
     * @throws NarrativeExistsException
     *             when the requested narrative already exists.
     * @throws IllegalArgumentException
     *             if no actions are specified.
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchActionException
     *             when the one or more actions does not exist and was not created.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist and was not created or,
     *             if the topic is <code>null</code>, there is no default topic.
     */
    Narrative createNarrative(String item,
                              String topic,
                              List<String> actions) throws NarrativeExistsException,
                                                    IllegalStateException,
                                                    IllegalArgumentException,
                                                    NoSuchActionException,
                                                    NoSuchTopicException;

    /**
     * Creates a new set of narrative for the supplied items within the context of
     * specified topic attaching the specified actions.
     *
     * @param items
     *            the identities of the items with which to create the new
     *            narratives.
     * @param topic
     *            the name of the topic that specifies the context of the items.
     * @param actions
     *            the set of actions, i.e. actors that need to see or act upon the
     *            supplied item and topic, to be assigned to the created narrative.
     *
     * @return the set of {@link Narrative} instances created.
     *
     * @throws NarrativeExistsException
     *             when the requested narrative already exists.
     * @throws IllegalArgumentException
     *             if no actions are specified.
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchActionException
     *             when the one or more actions does not exist and was not created.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist and was not created or,
     *             if the topic is <code>null</code>, there is no default topic.
     */
    List<? extends Narrative> createNarratives(List<String> items,
                                               String topic,
                                               List<String> actions) throws NarrativeExistsException,
                                                                     IllegalStateException,
                                                                     IllegalArgumentException,
                                                                     NoSuchActionException,
                                                                     NoSuchTopicException;

    /**
     * Creates a new topic.
     *
     * @param topic
     *            the name of the topic to be created.
     *
     * @return true if the topic was created or already existed.
     *
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     */
    boolean createTopic(String topic) throws IllegalStateException;

    /**
     * Endorses the appropriate narratives to record that the specified action as
     * dealt with the supplied items within the specified topic.
     *
     * @param items
     *            the set of identities of the items being managed within the
     *            context of the topic.
     * @param topic
     *            the name of the topic that specifies the context of the items.
     * @param action
     *            the action, i.e. the actor, that has dealt with supplied item
     *            within the specified topic.
     *
     * @return the {@link Digests} instance of all of the narratives that were
     *         endorsed.
     *
     * @throws IllegalArgumentException
     *             if no action is specified.
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchNarrativeException
     *             when at least one or more specified narrative do not exist.
     * @throws NoSuchActionException
     *             when the action does not exist and was not created.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist.
     */
    List<? extends Narrative> endorse(List<String> items,
                                      String topic,
                                      String action) throws IllegalArgumentException,
                                                     IllegalStateException,
                                                     NoSuchNarrativeException,
                                                     NoSuchActionException,
                                                     NoSuchTopicException;

    /**
     * Endorses the appropriate narrative to record that the specified action as
     * dealt with the supplied item within the specified topic.
     *
     * @param item
     *            the identity of the item being managed.
     * @param topic
     *            the name of the topic that specifies the context of the item.
     * @param action
     *            the action, i.e. the actor, that has dealt with supplied item
     *            within the specified topic.
     *
     * @return the {@link Digest} instance of the narrative that was endorsed.
     *
     * @throws IllegalArgumentException
     *             if no action is specified.
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchNarrativeException
     *             when the specified narrative does not exist.
     * @throws NoSuchActionException
     *             when the action does not exist and was not created.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist.
     */
    Narrative endorse(String item,
                      String topic,
                      String action) throws IllegalArgumentException,
                                     IllegalStateException,
                                     NoSuchNarrativeException,
                                     NoSuchActionException,
                                     NoSuchTopicException;

    /**
     * Returns all of the {@link Narrative} instances for the specified item.
     * 
     * @param item
     *            the name of the item whose {@link Narrative} instances should be
     *            returned.
     * 
     * @return all of the {@link Narrative} instances for the specified item.
     */
    List<? extends Narrative> getAllNarratives(String item);

    /**
     * Returns the sequence of {@link Endorsement} instances that match the supplied
     * action, topic, and filter parameters.
     *
     * @param action
     *            the action, i.e. the actor, whose endorsements should be returned.
     * @param topic
     *            the name of the topic that specifies the context of the
     *            endorsements.
     * @param after
     *            the date and time on or after which the action endorsed an item,
     *            <code>null</code> mean there is no lower limit.
     * @param before
     *            the date and time before which the action endorsed an item,
     *            <code>null</code> mean there is no upper limit.
     * @param max
     *            the maximum number of endorsements to return, 0 or less means
     *            unlimited.
     *
     * @return the list of all items that were signed off by a given action between
     *         the specified date and times.
     *
     * @throws IllegalArgumentException
     *             if no action is specified.
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchActionException
     *             when the action does not exist and was not created.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist.
     */
    List<? extends Endorsement> getEndorsements(String action,
                                                String topic,
                                                Date after,
                                                Date before,
                                                int max) throws IllegalArgumentException,
                                                         NoSuchActionException,
                                                         NoSuchTopicException;

    /**
     * Returns the collection of names of {@link Item} instances that have
     * endorsements of all the specified actions.
     * 
     * @param actions
     *            the actions, i.e. the actors, all of whom who must be endorsed for
     *            an {@link Items} instances to be returned in the list.
     * @param topic
     *            the name of the topic that specifies the context of the
     *            endorsements.
     * 
     * @return the collection of names of {@link Item} instances that have
     *         endorsements of all the specified actions.
     * 
     * @throws IllegalArgumentException
     *             if no action is specified.
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchActionException
     *             when the action does not exist and was not created.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist.
     */
    List<String> getItems(List<String> actions,
                          String topic) throws IllegalStateException,
                                        NoSuchTopicException,
                                        IllegalArgumentException,
                                        NoSuchActionException;

    /**
     * Returns a {@link Digest} instance, if any, for the supplied item and topic,
     * <code>null</code> otherwise.
     *
     * @param item
     *            the identity of the item whose {@link Digest} should be returned.
     * @param topic
     *            the name of the topic that specifies the context of the item.
     *
     * @return the {@link Digest}, if any, for the supplied item and topic,
     *         <code>null</code> otherwise.
     *
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchNarrativeException
     *             when the specified narrative does not exist.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist.
     */
    Narrative getNarrative(String item,
                           String topic) throws IllegalStateException,
                                         NoSuchTopicException,
                                         NoSuchNarrativeException;

    /**
     * Returns the {@link Narrative} instance, if any, that matches the supplied
     * items and topic. Items whose narratives can not be matched will be ignored.
     *
     * @param items
     *            the identity of the item whose {@link Digest} should be returned.
     * @param topic
     *            the name of the topic that specifies the context of the item.
     *
     * @return the {@link Digest}, if any, for the supplied item and topic,
     *         <code>null</code> otherwise.
     *
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist.
     */
    List<? extends Narrative> getNarratives(List<String> items,
                                            String topic) throws IllegalStateException,
                                                          NoSuchTopicException;

    /**
     * Loads a narrative with information from an external source. The information
     * is received in terms of an {@link Endorsement} instance. If endorsement does
     * not contain a time it is interpreted as action assignments. An unknown
     * narrative is created.
     *
     * @param topic
     *            the name of the topic that specifies the context of the inputs.
     * @param input
     *            the input received from the external source.
     *
     * @return the updated and created {@link Narrative} instance that result from
     *         this change.
     *
     * @throws IllegalArgumentException
     *             if no action is specified.
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name or it is
     *             not possible to create a required narrative.
     * @throws NoSuchActionException
     *             when the action does not exist and was not created.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist.
     */
    Narrative load(String topic,
                   Endorsement input) throws IllegalArgumentException,
                                      IllegalStateException,
                                      NoSuchActionException,
                                      NoSuchTopicException;

    /**
     * Loads narratives with information from an external source. The information is
     * received in terms of a set of {@link Endorsement} instances. Those
     * endorsements that do not contain a time are interpreted as action
     * assignments. Any unknown narrative is created.
     *
     * @param topic
     *            the name of the topic that specifies the context of the load.
     * @param endorsements
     *            the collection of {@link Endorsement} instances to load.
     *
     * @return the set of updated and created {@link Narrative} instances that
     *         results from these changes.
     *
     * @throws IllegalArgumentException
     *             if no action is specified.
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name or it is
     *             not possible to create a required narrative.
     * @throws NoSuchActionException
     *             when the action does not exist and was not created.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist.
     */
    List<? extends Narrative> load(String topic,
                                   List<? extends Endorsement> endorsements) throws IllegalArgumentException,
                                                                             IllegalStateException,
                                                                             NoSuchActionException,
                                                                             NoSuchTopicException;

    /**
     * Sets whether actions should be automatically created when they are unknown
     * when a narrative is created, or not.
     *
     * @param automatic
     *            true when actions should be automatically created.
     */
    void setAutomaticActions(boolean automatic);

    /**
     * Sets whether actions should be automatically created when they are unknown
     * when a narrative is endorsed, or not.
     *
     * @param automatic
     *            true when actions should be automatically created.
     */
    void setAutomaticEndorsers(boolean automatic);

    /**
     * Sets whether topics should be automatically created when they are unknown, or
     * not.
     *
     * @param automatic
     *            true when topics should be automatically created.
     */
    void setAutomaticTopics(boolean automatic);

    /**
     * Sets the accuracy with which endorsements are time stamped.
     *
     * @param unit
     *            the {@link TimeUnit} constant specifying the accuracy.
     */
    void setEndorsementAccuracy(TimeUnit unit);

    /**
     * Sets the action to take when multiple endorsements exist that have to the
     * same action and narrative.
     *
     * @param multiple
     *            the action to take when multiple endorsements exist that have to
     *            the same action and narrative.
     */
    void setMultipleEndorsements(MultipleEndorsements multiple);

    /**
     * Unassigns actions from {@link Narrative} instances, if they exist, for the
     * supplied set of items within the context of specified topic. If an action has
     * already been endorsed for a {@link Narrative} instance then the instance will
     * not be changed, i.e. existing endorsements will not be deleted. Similarly, if
     * an action is not assigned to a {@link Narrative} instance it is not changed.
     * If there is no {@link Narrative} instance the item then it will be ignored.
     *
     * @param items
     *            the identities of the items to whose narrative the actions should
     *            be unassigned
     * @param topic
     *            the name of the topic that specifies the context of the item.
     * @param actions
     *            the set of actions, i.e. actors that need to see or act upon the
     *            supplied item and topic, to be unassigned to the existing
     *            narrative.
     *
     * @return the set of changed {@link Narrative} instances.
     *
     * @throws IllegalArgumentException
     *             if no actions are specified.
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchActionException
     *             when the one or more actions does not exist and was not created.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist and was not created or,
     *             if the topic is <code>null</code>, there is no default topic.
     */
    List<? extends Narrative> unassignActions(List<String> items,
                                              String topic,
                                              List<String> actions) throws IllegalArgumentException,
                                                                    IllegalStateException,
                                                                    NoSuchActionException,
                                                                    NoSuchTopicException;

    /**
     * Unassigns actions from the {@link Narrative} instance, if it exist, for the
     * supplied item within the context of specified topic. If an action has already
     * been endorsed for the {@link Narrative} instance then that instance will not
     * be changed, i.e. existing endorsements will not be deleted. Similarly, if an
     * action is not assigned to the {@link Narrative} instance it is not changed.
     * If there is no {@link Narrative} instance then <code>null</code> is returned.
     *
     * @param item
     *            the identity of the item to whose narrative the actions should be
     *            unassigned
     * @param topic
     *            the name of the topic that specifies the context of the item.
     * @param actions
     *            the set of actions, i.e. actors that need to see or act upon the
     *            supplied item and topic, to be unassigned to the existing
     *            narrative.
     *
     * @return the updated {@link Narrative} instance, or <code>null</code> if there
     *         is no {@link Narrative} instance for the specified item and topic..
     *
     * @throws IllegalArgumentException
     *             if no actions are specified.
     * @throws IllegalStateException
     *             when there is more than one topic with the supplied name.
     * @throws NoSuchActionException
     *             when the one or more actions does not exist and was not created.
     * @throws NoSuchTopicException
     *             when the specified topic does not exist and was not created or,
     *             if the topic is <code>null</code>, there is no default topic.
     */
    Narrative unassignActions(String item,
                              String topic,
                              List<String> actions) throws IllegalArgumentException,
                                                    IllegalStateException,
                                                    NoSuchActionException,
                                                    NoSuchTopicException;
}
