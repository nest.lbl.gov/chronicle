package gov.lbl.nest.chronicle;

import jakarta.ejb.ApplicationException;

/**
 * This exception is base class for all Exceptions thrown by the
 * {@link Chronicle} application.
 *
 * @author patton
 */
@ApplicationException(rollback = true)
public class ChronicleException extends
                                Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by the Serialized mechanism.
     */
    private static final long serialVersionUID = -1L;

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    public ChronicleException() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the detailed message.
     */
    public ChronicleException(String message) {
        super(message);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
