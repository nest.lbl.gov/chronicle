package gov.lbl.nest.chronicle;

/**
 * This class represents a topic that provides a context with in which an item
 * can have a {@link Narrative}.
 *
 * @author patton
 */
public interface Topic {

    /**
     * Returns the name of this object.
     *
     * @return the name of this object.
     */
    String getName();

}