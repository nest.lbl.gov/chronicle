package gov.lbl.nest.chronicle;

import java.util.Date;

/**
 * This interface is used to implements guard conditions and post-endorsement
 * activities.
 *
 * @author patton
 */
public interface EndorsementLifecycle {

    /**
     * This class is used to create internal endorsements generated during any of
     * the methods of the {@link EndorsementLifecycle} class.
     * 
     * @author patton
     */
    public static class InputEndorsement implements
                                         Endorsement {

        private final Endorser endorser;
        private final String item;
        private final Date whenEndorsed;

        /**
         * Creates an instance of this class.
         *
         * @param item
         *            the name of the item being endorsed.
         * @param action
         *            the name of the action of the endorser doing the endorsement. of a
         *            topic.
         * @param whenEndorsed
         *            the date and time the endorsement was name.
         */
        public InputEndorsement(String item,
                                String action,
                                Date whenEndorsed) {
            this.endorser = new Endorser() {

                @Override
                public String getAction() {
                    return action;
                }
            };
            this.item = item;
            this.whenEndorsed = whenEndorsed;
        }

        @Override
        public Endorser getEndorser() {
            return endorser;
        }

        @Override
        public String getItem() {
            return item;
        }

        @Override
        public Date getWhenEndorsed() {
            return whenEndorsed;
        }
    }

    /**
     * Called after an {@link Endorsement} instance has failed to be added to a
     * {@link Narrative} instance.
     *
     * @param chronicle
     *            the {@link Chronicle} instance containing the {@link Endorsement}
     *            instance.
     * @param narrative
     *            the {@link Narrative} instance that failed to contain the
     *            {@link Endorsement} instance.
     * @param endorsement
     *            the {@link Endorsement} instance.
     */
    void failedEndorsement(Chronicle chronicle,
                           Narrative narrative,
                           Endorsement endorsement);

    /**
     * Called before an {@link Endorsement} instance is added to a {@link Narrative}
     * instance.
     *
     * @param chronicle
     *            the {@link Chronicle} instance containing the {@link Endorsement}
     *            instance.
     * @param narrative
     *            the {@link Narrative} instance to which the {@link Endorsement}
     *            instance sh9uld be added.
     * @param endorsement
     *            the {@link Endorsement} instance.
     *
     * @return <code>true</code> if the {@link Endorsement} instance can be added to
     *         the {@link Narrative} instance.
     */
    boolean preEndorsement(Chronicle chronicle,
                           Narrative narrative,
                           Endorsement endorsement);

    /**
     * Called after an {@link Endorsement} instance has been successfully added to a
     * {@link Narrative} instance.
     *
     * @param chronicle
     *            the {@link Chronicle} instance containing the {@link Endorsement}
     *            instance.
     * @param narrative
     *            the {@link Narrative} instance now containing the
     *            {@link Endorsement} instance.
     * @param endorsement
     *            the {@link Endorsement} instance.
     */
    void successfulEndorsement(Chronicle chronicle,
                               Narrative narrative,
                               Endorsement endorsement);

}
