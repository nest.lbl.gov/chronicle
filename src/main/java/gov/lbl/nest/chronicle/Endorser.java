package gov.lbl.nest.chronicle;

/**
 * This class represents an action that has been or should be performed on an
 * item.
 *
 * @author patton
 */
public interface Endorser {

    /**
     * Returns the action that this object represents.
     *
     * @return the action that this object represents.
     */
    String getAction();
}