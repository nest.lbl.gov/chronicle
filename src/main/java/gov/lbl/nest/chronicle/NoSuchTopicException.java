package gov.lbl.nest.chronicle;

/**
 * This exception reports that a requested {@link Topic} does not exist and was
 * not created.
 *
 * @author patton
 */
public class NoSuchTopicException extends
                                  ChronicleException {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by the Serialized mechanism.
     */
    private static final long serialVersionUID = -1L;

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    public NoSuchTopicException() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the detailed message.
     */
    public NoSuchTopicException(String message) {
        super(message);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
