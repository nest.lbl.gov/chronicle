# The Chronicle Application #

> Definition of chronicle ([Merriam Webster](https://www.merriam-webster.com/dictionary/chronicle))
>
> * a historical account of events arranged in order of time usually without analysis or interpretation


The purpose of the Chronicle application is to record when an action has been performed on an item. For example, the item may be a file and the action is its delivery to a data center. Once the action has been preformed on the item, an endorsement (the combination of the action and the date & time it was preformed) is associated with the item. The Chronicle application can then be queried for the ordered list of endorsements associated with a specific item, with the resulting list, know as a narrative, being returned.

As well and associating endorsements with an item, the Chronicle application can associate actions that should be, but have not yet been, performed on an item. These are known as endorsers and, obviously, they become endorsements once they have been performed. One mode of operation of the Chronicle application using this feature is to consider a narrative that contains one or more endorsers as being incomplete, and those with no endorsers, i.e. thise that contain only, endorsements as complete.

The Chronicle application also supports guard conditions that test whether an endorsement is allowed or should be rejected, and post-endorsement hooks that allow for, among other things, the addition of new, and removal of existing, endorsers to or from a narrative based on the endorsement being made. This enables a incomplete narrative to be manipulated as endorsements are added.

With all of the above features, it is clear that the Chronicle application may need to support multiple narratives for a single item, which it does by narratives with the same purpose but different items into topics. Within each topic an item can only have one narrative but that item can belong to as many different topics as necessary.
